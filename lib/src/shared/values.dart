import 'package:flutter/material.dart';

class Values {
  //Colors
  static final Color mediumGreyText = Colors.grey[400];
  static const BLACK_TEXT = Colors.black;
  static const double AVATAR_SCALE = 100;
  static const String PRINT_DEVBOX_PREFIX = "--------------> DEVBOX says:";



  //Dimensions
  static const double BUTTON_HEIGHT = 48;
  static const double CARD_BORDER_RADIUS = 10;
  static const double TEXT_FIELD_BORDER_RADIUS = 24;


}