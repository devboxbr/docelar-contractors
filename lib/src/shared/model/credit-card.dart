import 'package:json_annotation/json_annotation.dart';
part 'credit-card.g.dart';

@JsonSerializable(explicitToJson: true)
class CreditCard {
  String customerName;
  String number;
  String validThrough;
  String cvv;

  CreditCard(this.customerName, this.number, this.validThrough, this.cvv);

  @override
  String toString() {
    return 'CreditCard{customerName: $customerName, number: $number, validThrough: $validThrough}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is CreditCard &&
              runtimeType == other.runtimeType &&
              customerName == other.customerName &&
              number == other.number &&
              validThrough == other.validThrough;

  @override
  int get hashCode =>
      customerName.hashCode ^
      number.hashCode ^
      validThrough.hashCode;

  factory CreditCard.fromJson(Map<String, dynamic> json) => _$CreditCardFromJson(json);
  Map<String, dynamic> toJson() => _$CreditCardToJson(this);

}