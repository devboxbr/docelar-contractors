// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rating.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Rating _$RatingFromJson(Map json) {
  return Rating(
    json['contractorId'] as String,
    (json['rate'] as num)?.toDouble(),
    json['title'] as String,
    json['comment'] as String,
    json['serviceDate'] == null
        ? null
        : DateTime.parse(json['serviceDate'] as String),
    json['ratingDate'] == null
        ? null
        : DateTime.parse(json['ratingDate'] as String),
  );
}

Map<String, dynamic> _$RatingToJson(Rating instance) => <String, dynamic>{
      'contractorId': instance.contractorId,
      'rate': instance.rate,
      'title': instance.title,
      'comment': instance.comment,
      'serviceDate': instance.serviceDate?.toIso8601String(),
      'ratingDate': instance.ratingDate?.toIso8601String(),
    };
