import 'package:json_annotation/json_annotation.dart';
part 'stamp.g.dart';

@JsonSerializable(explicitToJson: true)
class Stamp {
  String id;
  //String name;
  DateTime dateTime;
  double durationInMinutes;
  String observations;

  Stamp(this.dateTime, this.durationInMinutes, this.observations);

  factory Stamp.fromJson(Map<String, dynamic> json) => _$StampFromJson(json);
  Map<String, dynamic> toJson() => _$StampToJson(this);


}
