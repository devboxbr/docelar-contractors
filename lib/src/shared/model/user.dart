import 'package:docelar/src/shared/model/address.dart';
import 'package:docelar/src/shared/model/appointment.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable(explicitToJson: true)
class User {
  String id;
  String name;
  String lastName;
  String cpf;
  String phone;
  String email;
  String pictureUrl;
  double rating;
  Address mainAddress;
  List<Address> allAddresses;
  double wallet;
  List<Appointment> scheduledAppointments;
  List<Appointment> archivedAppointments;
  List<String> recentSearches;
  DateTime registerDateTime;

  User(this.name, this.lastName, this.cpf, this.phone, this.email);

  ///Returns user's name and lastName with both first letters capitalized, being protected against nullable
  String getFullName() {
    //If there is no first name
    if (name.isEmpty || name == null) {
      //And there is no last name too
      if (lastName.isEmpty || lastName == null) {
        return "Erro";
      } else {
        //If there still a last name
        return lastName[0].toUpperCase() + lastName?.substring(1);
      }
      //If there is a name
    } else {
      //And if there is no last name
      if (lastName.isEmpty || lastName == null) {
        return name[0].toUpperCase() + name?.substring(1);
        //If there is still a last name
      } else {
        return name[0].toUpperCase() +
            name?.substring(1) +
            " " +
            lastName[0].toUpperCase() +
            lastName?.substring(1);
      }
    }
  }

  String getFormattedCpf() {
    return "${cpf.substring(0, 3)}.${cpf.substring(3, 6)}.${cpf.substring(6, 9)}-${cpf.substring(9, 11)}";
  }

  String getFormattedPhone() {
    switch(phone.length){
      case 10:
        return "(${phone.substring(0,2)}) ${phone.substring(2,6)}-${phone.substring(6,10)}";
      case 11:
        return "(${phone.substring(0,2)}) ${phone.substring(2,7)}-${phone.substring(7,11)}";
      case 12:
        return "(${phone.substring(0,3)}) ${phone.substring(3,8)}-${phone.substring(8,12)}";
    }
  }

  @override
  bool operator ==(other) {
    if (other is User) {
      if (identical(this, other) ||
          this.cpf == other.cpf ||
          this.email == other.email) return true;
    }
    return false;
  }

  @override
  int get hashCode => (cpf.hashCode + email.hashCode);

  @override
  String toString() {
    return "User {name: $name $lastName, email: $email, phone: $phone}";
  }

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
