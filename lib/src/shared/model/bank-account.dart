import 'package:json_annotation/json_annotation.dart';
part 'bank-account.g.dart';

@JsonSerializable(explicitToJson: true)
class BankAccount {
  String bankName;
  String agency;
  String account;
  String type;

  BankAccount(this.bankName, this.agency, this.account, this.type);

  factory BankAccount.fromJson(Map<String, dynamic> json) => _$BankAccountFromJson(json);
  Map<String, dynamic> toJson() => _$BankAccountToJson(this);
}