// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Appointment _$AppointmentFromJson(Map json) {
  return Appointment(
    json['contractorId'] as String,
    json['workerId'] as String,
    json['dateTime'] == null
        ? null
        : DateTime.parse(json['dateTime'] as String),
    json['address'] == null
        ? null
        : Address.fromJson((json['address'] as Map)?.map(
            (k, e) => MapEntry(k as String, e),
          )),
    (json['value'] as num)?.toDouble(),
  )
    ..id = json['id'] as String
    ..locationCoordinates = json['locationCoordinates'] == null
        ? null
        : LocationCoordinates.fromJson(
            (json['locationCoordinates'] as Map)?.map(
            (k, e) => MapEntry(k as String, e),
          ));
}

Map<String, dynamic> _$AppointmentToJson(Appointment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'contractorId': instance.contractorId,
      'workerId': instance.workerId,
      'dateTime': instance.dateTime?.toIso8601String(),
      'address': instance.address?.toJson(),
      'value': instance.value,
      'locationCoordinates': instance.locationCoordinates?.toJson(),
    };
