import 'package:docelar/src/shared/model/credit-card.dart';
import 'package:docelar/src/shared/model/user.dart';
import 'package:docelar/src/shared/model/worker.dart';
import 'package:json_annotation/json_annotation.dart';
import 'address.dart';
import 'appointment.dart';

part 'contractor.g.dart';

@JsonSerializable(explicitToJson: true)
class Contractor extends User {
  CreditCard creditCard;
  List<Worker> favoriteWorkers;

  Contractor(String name, String lastName, String cpf, String phone, String email)
      : super(name, lastName, cpf, phone, email);

  factory Contractor.fromJson(Map<String, dynamic> json) => _$ContractorFromJson(json);
  Map<String, dynamic> toJson() => _$ContractorToJson(this);


}

