import 'package:docelar/src/shared/model/bank-account.dart';
import 'package:docelar/src/shared/model/rating.dart';
import 'package:docelar/src/shared/model/stamp.dart';
import 'package:docelar/src/shared/model/user.dart';
import 'package:json_annotation/json_annotation.dart';
import 'address.dart';
import 'appointment.dart';
part 'worker.g.dart';

@JsonSerializable(explicitToJson: true)
class Worker extends User {
  BankAccount bankAccount;
  List<Stamp> stamps;
  Map<String, dynamic> activities;
  List<Rating> ratings;

  Worker(String name, String lastName, String cpf, String phone, String email) : super(name, lastName, cpf, phone, email);

  factory Worker.fromJson(Map<String, dynamic> json) => _$WorkerFromJson(json);
  Map<String, dynamic> toJson() => _$WorkerToJson(this);
}