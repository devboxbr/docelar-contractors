// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bank-account.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BankAccount _$BankAccountFromJson(Map json) {
  return BankAccount(
    json['bankName'] as String,
    json['agency'] as String,
    json['account'] as String,
    json['type'] as String,
  );
}

Map<String, dynamic> _$BankAccountToJson(BankAccount instance) =>
    <String, dynamic>{
      'bankName': instance.bankName,
      'agency': instance.agency,
      'account': instance.account,
      'type': instance.type,
    };
