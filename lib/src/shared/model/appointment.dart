import 'package:docelar/src/shared/model/address.dart';
import 'package:docelar/src/shared/model/locationcoordinates.dart';
import 'package:json_annotation/json_annotation.dart';
part 'appointment.g.dart';

@JsonSerializable(explicitToJson: true)
class Appointment {
  String id;
  String contractorId;
  String workerId;
  DateTime dateTime;
  Address address;
  double value;
  LocationCoordinates locationCoordinates;

  Appointment(this.contractorId, this.workerId, this.dateTime, this.address,
      this.value);

  @override
  bool operator ==(other) {
    if(other is Appointment) {
      if(identical(this, other) || this.id == other.id) return true;
    }
    return false;
  }

  @override
  int get hashCode {
    return id.hashCode;
  }

  @override
  String toString() {
    return 'Appointment{id: $id, employerId: $contractorId, employeeId: $workerId, dateTime: $dateTime, address: $address, value: $value}';
  }

  factory Appointment.fromJson(Map<String, dynamic> json) => _$AppointmentFromJson(json);
  Map<String, dynamic> toJson() => _$AppointmentToJson(this);


}