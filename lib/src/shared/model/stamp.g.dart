// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stamp.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Stamp _$StampFromJson(Map json) {
  return Stamp(
    json['dateTime'] == null
        ? null
        : DateTime.parse(json['dateTime'] as String),
    (json['durationInMinutes'] as num)?.toDouble(),
    json['observations'] as String,
  )..id = json['id'] as String;
}

Map<String, dynamic> _$StampToJson(Stamp instance) => <String, dynamic>{
      'id': instance.id,
      'dateTime': instance.dateTime?.toIso8601String(),
      'durationInMinutes': instance.durationInMinutes,
      'observations': instance.observations,
    };
