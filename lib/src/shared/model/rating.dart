import 'package:json_annotation/json_annotation.dart';
part 'rating.g.dart';

@JsonSerializable(explicitToJson: true)
class Rating {
  String contractorId;
  double rate;
  String title;
  String comment;
  DateTime serviceDate;
  DateTime ratingDate;

  Rating(this.contractorId, this.rate, this.title, this.comment,
      this.serviceDate, this.ratingDate);

  factory Rating.fromJson(Map<String, dynamic> json) => _$RatingFromJson(json);
  Map<String, dynamic> toJson() => _$RatingToJson(this);

  @override
  String toString() {
    return "Rating {Title: $title, Rate: $rate, Comment: $comment}";
  }


}