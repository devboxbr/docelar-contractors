// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'locationcoordinates.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LocationCoordinates _$LocationCoordinatesFromJson(Map json) {
  return LocationCoordinates(
    json['latitude'] as String,
    json['longitude'] as String,
  );
}

Map<String, dynamic> _$LocationCoordinatesToJson(
        LocationCoordinates instance) =>
    <String, dynamic>{
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };
