// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contractor.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Contractor _$ContractorFromJson(Map json) {
  return Contractor(
    json['name'] as String,
    json['lastName'] as String,
    json['cpf'] as String,
    json['phone'] as String,
    json['email'] as String,
  )
    ..id = json['id'] as String
    ..pictureUrl = json['pictureUrl'] as String
    ..rating = (json['rating'] as num)?.toDouble()
    ..mainAddress = json['mainAddress'] == null
        ? null
        : Address.fromJson((json['mainAddress'] as Map)?.map(
            (k, e) => MapEntry(k as String, e),
          ))
    ..allAddresses = (json['allAddresses'] as List)
        ?.map((e) => e == null
            ? null
            : Address.fromJson((e as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )))
        ?.toList()
    ..wallet = (json['wallet'] as num)?.toDouble()
    ..scheduledAppointments = (json['scheduledAppointments'] as List)
        ?.map((e) => e == null
            ? null
            : Appointment.fromJson((e as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )))
        ?.toList()
    ..archivedAppointments = (json['archivedAppointments'] as List)
        ?.map((e) => e == null
            ? null
            : Appointment.fromJson((e as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )))
        ?.toList()
    ..recentSearches =
        (json['recentSearches'] as List)?.map((e) => e as String)?.toList()
    ..registerDateTime = json['registerDateTime'] == null
        ? null
        : DateTime.parse(json['registerDateTime'] as String)
    ..creditCard = json['creditCard'] == null
        ? null
        : CreditCard.fromJson((json['creditCard'] as Map)?.map(
            (k, e) => MapEntry(k as String, e),
          ))
    ..favoriteWorkers = (json['favoriteWorkers'] as List)
        ?.map((e) => e == null
            ? null
            : Worker.fromJson((e as Map)?.map(
                (k, e) => MapEntry(k as String, e),
              )))
        ?.toList();
}

Map<String, dynamic> _$ContractorToJson(Contractor instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'lastName': instance.lastName,
      'cpf': instance.cpf,
      'phone': instance.phone,
      'email': instance.email,
      'pictureUrl': instance.pictureUrl,
      'rating': instance.rating,
      'mainAddress': instance.mainAddress?.toJson(),
      'allAddresses': instance.allAddresses?.map((e) => e?.toJson())?.toList(),
      'wallet': instance.wallet,
      'scheduledAppointments':
          instance.scheduledAppointments?.map((e) => e?.toJson())?.toList(),
      'archivedAppointments':
          instance.archivedAppointments?.map((e) => e?.toJson())?.toList(),
      'recentSearches': instance.recentSearches,
      'registerDateTime': instance.registerDateTime?.toIso8601String(),
      'creditCard': instance.creditCard?.toJson(),
      'favoriteWorkers':
          instance.favoriteWorkers?.map((e) => e?.toJson())?.toList(),
    };
