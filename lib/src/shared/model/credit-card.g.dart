// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credit-card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreditCard _$CreditCardFromJson(Map json) {
  return CreditCard(
    json['customerName'] as String,
    json['number'] as String,
    json['validThrough'] as String,
    json['cvv'] as String,
  );
}

Map<String, dynamic> _$CreditCardToJson(CreditCard instance) =>
    <String, dynamic>{
      'customerName': instance.customerName,
      'number': instance.number,
      'validThrough': instance.validThrough,
      'cvv': instance.cvv,
    };
