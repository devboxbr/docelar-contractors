// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Address _$AddressFromJson(Map json) {
  return Address(
    json['label'] as String,
    json['street'] as String,
    json['number'] as String,
    json['neighborhood'] as String,
    json['postalCode'] as String,
    json['city'] as String,
    json['state'] as String,
    json['complementInfo'] as String,
    json['referencePoint'] as String,
  )..locationCoordinates = json['locationCoordinates'] == null
      ? null
      : LocationCoordinates.fromJson((json['locationCoordinates'] as Map)?.map(
          (k, e) => MapEntry(k as String, e),
        ));
}

Map<String, dynamic> _$AddressToJson(Address instance) => <String, dynamic>{
      'label': instance.label,
      'street': instance.street,
      'number': instance.number,
      'neighborhood': instance.neighborhood,
      'postalCode': instance.postalCode,
      'city': instance.city,
      'state': instance.state,
      'complementInfo': instance.complementInfo,
      'referencePoint': instance.referencePoint,
      'locationCoordinates': instance.locationCoordinates?.toJson(),
    };
