import 'package:json_annotation/json_annotation.dart';
part 'locationcoordinates.g.dart';

@JsonSerializable(explicitToJson: true)
class LocationCoordinates {
  String latitude;
  String longitude;

  LocationCoordinates(this.latitude, this.longitude);

  factory LocationCoordinates.fromJson(Map<String, dynamic> json) => _$LocationCoordinatesFromJson(json);
  Map<String, dynamic> toJson() => _$LocationCoordinatesToJson(this);

}