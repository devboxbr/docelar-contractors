/// JSON GUIDE:
/// https://flutter.dev/docs/development/data-and-backend/json#serializing-json-using-code-generation-libraries

import 'package:docelar/src/shared/model/locationcoordinates.dart';
import 'package:json_annotation/json_annotation.dart';

part 'address.g.dart';

@JsonSerializable(explicitToJson: true)
class Address {
  String label;
  String street;
  String number;
  String neighborhood;
  String postalCode;
  String city;
  String state;
  String complementInfo;
  String referencePoint;
  LocationCoordinates locationCoordinates;

  Address(this.label, this.street, this.number, this.neighborhood, this.postalCode,
      this.city, this.state, this.complementInfo, this.referencePoint);

  String getFullAddress(){
    return "$street, $number, $neighborhood, $complementInfo, $referencePoint, $postalCode, $city, $state";
  }

  String getFormattedPostalCode(){
    return "${postalCode.substring(0, 5)}-${postalCode.substring(5, 8)}";
  }

  factory Address.fromJson(Map<String, dynamic> json) => _$AddressFromJson(json);

  Map<String, dynamic> toJson() => _$AddressToJson(this);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Address &&
          runtimeType == other.runtimeType &&
          street == other.street &&
          number == other.number &&
          neighborhood == other.neighborhood &&
          postalCode == other.postalCode &&
          city == other.city &&
          state == other.state;

  @override
  int get hashCode =>
      street.hashCode ^
      number.hashCode ^
      neighborhood.hashCode ^
      postalCode.hashCode ^
      city.hashCode ^
      state.hashCode ^
      locationCoordinates.hashCode;

  @override
  String toString() {
    return 'Address{street: $street, number: $number, neighborhood: $neighborhood, postalCode: $postalCode, city: $city, state: $state, complementInfo: $complementInfo, referencePoint: $referencePoint}';
  }
}
