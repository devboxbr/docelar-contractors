import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:docelar/src/shared/model/address.dart';
import 'package:docelar/src/shared/model/worker.dart';
import 'package:docelar/src/shared/model/contractor.dart';
import 'package:docelar/src/shared/model/user.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';

class FirebaseService {
  static FirebaseAuth _auth = FirebaseAuth.instance;
  static Firestore firestore = Firestore.instance;
  static FirebaseStorage storage = FirebaseStorage.instance;
  static FirebaseUser currentUser;

  //static var googleSignIn = GoogleSignIn();

  //CONSTANTS
  static const String CONTRACTORS_COLLECTION = "contractors";
  static const String WORKERS_COLLECTION = "workers";
  static const String USERS_COLLECTION = "users";
  static const String USERS_DATA_STORAGE_FOLDER = "users-data";
  static const String SYSTEM_FOLDER = "system";
  static const String STAMPS_DOCUMENT = "stamps";
  static const String ACTIVITIES_DOCUMENT = "activities";
  static const String WORKER_CATEGORIES_DOCUMENT = "worker-categories";
  static const String WORKER_CATEGORIES_LIST = "categories";
  static const String ICONS_FOLDER = "icons";
  static const String SCHEDULED_APPOINTMENTS_COLLECTION =
      "scheduled-appointments";
  static const String ARCHIVED_APPOINTMENTS_COLLECTION =
      "archived-appointments";
  static const String USER_PROFILE_PICTURE = "profile-picture_url";

  //TODO: Decidir se vai implementar uma collection na root do Firestore apenas para mailing list
  static const String MAILING_LIST = "mailing-list";

  //
  //FIREBASE AUTH
  //

  ///Creates a user on Firebase
  static Future<FirebaseUser> signUserUp(String email, String password) async {
    await _auth.createUserWithEmailAndPassword(
        email: email, password: password);

    FirebaseUser user = await _auth.currentUser();

    if (user != null) {
      //print("user null");
      return _auth.currentUser();
    } else {
      return null;
    }
  }

/*
  static Future<String> updateAuthUser(String name, String pictureUrl) async {
    return await _auth.currentUser().then((FirebaseUser user) async {
      //TODO: Terminar o update do perfil no auth
      UserUpdateInfo updateUser = UserUpdateInfo();
      updateUser.displayName = name;
      print("${Values.PRINT_DEVBOX_PREFIX} PictureUrl: $pictureUrl");
      updateUser.photoUrl = pictureUrl;
      String errorCaught;

      await user.updateProfile(updateUser).catchError((error) {
        errorCaught = "Ops! Aconteceu algo errado!";
        //TODO: Verificar o formato da URL usada aqui (URI?)
        print("------------------------------");
        print("${Values.PRINT_DEVBOX_PREFIX} error.toString(): ${error.toString()}");
        print(
            "FirebaseService.updateAuthUserDisplayName(): Não foi possível atualizar o perfil do usuário do FirebaseAuth");
        print("------------------------------");
      });

      if (errorCaught == null) {
        //If there was NO error
        await user.reload();
        return errorCaught;
      } else {
        //If there was an error
        return errorCaught;
      }
    }).catchError((error) {
      if (error.toString().contains("ERROR_USER_DISABLED"))
        return "Usuário desabilitado!";
      else if (error.toString().contains("ERROR_USER_NOT_FOUND"))
        return "Usuário não encontrado!";
      return null;
    });
  }
*/

  ///Tries to reset the password and returns an [errorCaught]
  ///containing a message to the user about the problem
  static Future<String> resetPassword(String email) async {
    String errorCaught;
    //TODO: set language here when we internationalize the app
    await _auth.sendPasswordResetEmail(email: email).catchError((e) {
      if (e.toString().contains("ERROR_USER_NOT_FOUND")) {
        //print("${Values.PRINT_DEVBOX_PREFIX} FirebaseService.resetPassword() ERROR:");
        //print("${Values.PRINT_DEVBOX_PREFIX} e.toString(): ${e.toString()}");
        errorCaught = "Usuário não encontrado";
      }
    });

    return errorCaught;
  }

  ///Signs the user in
  static Future<FirebaseUser> signUserIn(String email, String password) async {
    await FirebaseService._auth
        .signInWithEmailAndPassword(email: email, password: password);

    FirebaseUser user = await _auth.currentUser();

    if (user != null) {
      //print("user null");
      return _auth.currentUser();
    }

    return null;

    //TODO: check if there is any release of exception treatment by google (following this: https://github.com/flutter/plugins/pull/775)
  }

  ///Checks if the user is already logged in
  static Future<bool> isUserLoggedIn() async {
    FirebaseUser user = await _auth.currentUser();
    if (user != null) return true;
    return false;
  }

/*
  ///Signs the user in using a Facebook account
  static Future<FirebaseUser> signInWithFacebook() async {
    var facebookLogin = FacebookLogin();
    var facebookLoginResult =
        await facebookLogin.logInWithReadPermissions(['email']);

    print(facebookLoginResult.status.toString());

    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        print("Error on Facebook login!");
        return null;
      case FacebookLoginStatus.cancelledByUser:
        print("Facebook login cancelled by user!");
        return null;
      case FacebookLoginStatus.loggedIn:
        print("Facebook login done!");

        final token = facebookLoginResult.accessToken.token;
        final graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,picture,name&access_token=$token');
        final profile = json.decode(graphResponse.body);
        //print(profile);
        AuthCredential credential = FacebookAuthProvider.getCredential();
        /*
        FirebaseUser user = await _auth.signInWithCredential(credential);

        print(user == null ? "user null" : "user not null");
        if (user != null) {
          currentUser = user;
          return user;
        }
        */

        //final token = facebookLoginResult.accessToken.token;
        break;
    }
    return null;
  }

  ///Signs the user in using a Google account
  static Future<FirebaseUser> signUserInWithGoogle() async {
    GoogleSignInAccount account = googleSignIn.currentUser;

    //Try to log the user without bothering him
    if (account == null) {
      account = await googleSignIn.signInSilently();
    }
    //If the silent login wasn't successful, then ask the user to sign in (showing a window to sign in)
    if (account == null) {
      account = await googleSignIn.signIn();
    }

    return await signInWithGoogleCredentials();
  }

  ///Signs the user up with Google credentials
  static Future<FirebaseUser> signInWithGoogleCredentials() async {
    GoogleSignInAuthentication credentials =
        await googleSignIn.currentUser.authentication;
    /*
    FirebaseUser user = await _auth.signInWithCredential(
        GoogleAuthProvider.getCredential(
            idToken: credentials.idToken,
            accessToken: credentials.accessToken));

    if (user != null) {
      currentUser = user;
      return user;
    }
    */
    return null;
  }
*/

  ///Signs the user out
  static void signOut() {
    _auth.signOut();
    currentUser = null;
  }

  //
  // FIRESTORE
  //

  ///Save user to Firestore
  static Future<bool> saveUserToFirestore(User user) async {
    String userCollection;
    bool success;
    if (user is Contractor)
      userCollection = CONTRACTORS_COLLECTION;
    else if (user is Worker) userCollection = WORKERS_COLLECTION;

    if (user != null) {
      await firestore
          .collection(userCollection)
          .document(user.id)
          .setData(user.toJson())
          .catchError((error) {
        //print("${Values.PRINT_DEVBOX_PREFIX} error to save at firestore: $error");
        success = false;
      }).then((value) {
        success = true;
      }).whenComplete(() {
        //print("${Values.PRINT_DEVBOX_PREFIX} FirebaseService.saveUserToFirestore() - Returning success = $success");
        return success;
      });
    }

    return success ?? false;
  }

  ///Update user on Firestore
  static Future<bool> updateUserOnFirestore(User userUpdated) async {
    String userCollection;
    bool success = false;
    if (userUpdated is Contractor)
      userCollection = CONTRACTORS_COLLECTION;
    else if (userUpdated is Worker)
      userCollection = WORKERS_COLLECTION;

    if (userUpdated != null) {
      await firestore
          .collection(userCollection)
          .document(userUpdated.id)
          .updateData(userUpdated.toJson())
          .catchError((error) {
      }).then((value) {
        success = true;
      });
    }
    return success;
  }

  ///Updates user email at firebase authentication
  static Future<bool> updateUserEmailOnAuth(String newEmail) async {
    print("${Values.PRINT_DEVBOX_PREFIX} 11");
    FirebaseUser firebaseUser = await _auth.currentUser();
    if(firebaseUser != null) {
      print("${Values.PRINT_DEVBOX_PREFIX} 12");
      return await firebaseUser.updateEmail(newEmail).then((v) {
        print("${Values.PRINT_DEVBOX_PREFIX} 13");
        return true;
      })/*.catchError((onError) {
        print("${Values.PRINT_DEVBOX_PREFIX} 14");
        print("${Values.PRINT_DEVBOX_PREFIX} ${onError.toString()}");
        return false;
      })*/;
    }
    print("${Values.PRINT_DEVBOX_PREFIX} 15");
    return false;
  }

  ///Saves a new address to the contractor's database
  static Future<Contractor> saveContractorAddressToFirestore(
      String uid, Address address) async {
    //Check if the address object passed is valid
    if (address != null) {
      //Tries to get the user document
      return await firestore
          .collection(CONTRACTORS_COLLECTION)
          .document(uid)
          .get()
          .then((DocumentSnapshot docSnap) async {
        //If the received snapshot is valid
        if (docSnap != null) {
          //Saves the snapshot as an User object
          Contractor contractor = Contractor.fromJson(docSnap.data);
          //If the received user is valid
          if (contractor != null) {
            //If the user hasn't a main address registered
            if(contractor.mainAddress == null) {
              //Saves the passed address to this user
              contractor.mainAddress = address;
            }

            //If the list of addresses is not null
            if (contractor.allAddresses != null) {
              //And if the list doesn't contain this address
              if (!contractor.allAddresses.contains(address)) {
                //Adds the new address
                contractor.allAddresses.add(address);
                //Saves the user to Firestore
                return await firestore
                    .collection(CONTRACTORS_COLLECTION)
                    .document(uid)
                    .updateData(contractor.toJson())
                    .then((onValue) {
                  return contractor;
                });
              }
              //If the user's object hasn't a valid list of addresses
            } else {
              //Creates a new instance of the list
              contractor.allAddresses = List<Address>();
              //Adds the address to the list
              contractor.allAddresses.add(address);
              //Saves the user to Firestore
              //print("${Values.PRINT_DEVBOX_PREFIX} contractor.toJson(): ${contractor.toJson()}");
              return await firestore
                  .collection(CONTRACTORS_COLLECTION)
                  .document(uid)
                  .updateData(contractor.toJson())
                  .then((onValue) {
                return contractor;
              });
            }
          }
        }
        return null;
      });
    }
    return null;
  }

  ///Saves a new email to the worker's database
  static Future<Worker> saveWorkerAddressToFirestore(
      String uid, Address address) async {
    //Check if the object passed is valid
    if (address != null) {
      //Tries to get the user document
      await firestore
          .collection(WORKERS_COLLECTION)
          .document(uid)
          .get()
          .then((DocumentSnapshot docSnap) async {
        //If the received snapshot is valid
        if (docSnap != null) {
          //Saves the snapshot as an User object
          Worker worker = Worker.fromJson(docSnap.data);
          //If the received user is valid
          if (worker != null) {
            //If the user hasn't a main address registered
            if(worker.mainAddress == null) {
              //Saves the passed address to this user
              worker.mainAddress = address;
            }

            //If the list of addresses is not null
            if (worker.allAddresses != null) {
              //And if the list doesn't contain this address
              if (!worker.allAddresses.contains(address)) {
                //Adds the new address
                worker.allAddresses.add(address);
                //Saves the user to Firestore
                await firestore
                    .collection(WORKERS_COLLECTION)
                    .document(uid)
                    .updateData(worker.toJson());
                return worker;
              }
              //If the user's object hasn't a valid list of addresses
            } else {
              //Creates a new instance of the list
              worker.allAddresses = List<Address>();
              //Adds the address to the list
              worker.allAddresses.add(address);
              //Saves the user to Firestore
              //print("${Values.PRINT_DEVBOX_PREFIX} worker.toJson(): ${worker.toJson()}");
              await firestore
                  .collection(WORKERS_COLLECTION)
                  .document(uid)
                  .updateData(worker.toJson());

              return worker;
            }
          }
        }
        return null;
      });
    }
    return null;
  }

  ///Returns a Contractor object retrieved at Firestore database
  static Future<Contractor> getContractorInFirestore() async {
    return await getCurrentUserId().then((String uid) async {
      return await firestore
          .collection(CONTRACTORS_COLLECTION)
          .document(uid)
          .get()
          .then((DocumentSnapshot documentSnapshot) {
        if (documentSnapshot != null) {
          //Saves the snapshot as an User object
          Map<String, dynamic> contractorMap =
              new Map<String, dynamic>.from(documentSnapshot.data);
          Contractor contractor = Contractor.fromJson(contractorMap);

          if (contractor != null) {
            //print("${Values.PRINT_DEVBOX_PREFIX} Contractor found: NAME{${contractor.name}}");
            return contractor;
          }
        }
        return null;
      });
    });
  }

  ///Returns a Worker object retrieved at Firestore database
  static Future<Worker> getWorkerInFirestore(String workerId) async {
    return await firestore
        .collection(WORKERS_COLLECTION)
        .document(workerId)
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot != null) {
        //Saves the snapshot as an User object
        Map<String, dynamic> workerMap =
            new Map<String, dynamic>.from(documentSnapshot.data);
        Worker worker = Worker.fromJson(workerMap);

        if (worker != null) {
          //print("${Values.PRINT_DEVBOX_PREFIX} Worker found: NAME{${worker.name}}");
          return worker;
        }
      }
      return null;
    });
  }

  ///Gets the link of the icon passed as the parameter [iconName]
  static Future<String> getIconLinkFromFirestore(String iconName) async {
    return await firestore
        .collection(SYSTEM_FOLDER)
        .document(ICONS_FOLDER)
        .get()
        .then((snapshot) {
      //If the snapshot is not null
      if (snapshot != null) {
        //Gets the link of the icon passed as parameter
        return snapshot[iconName];
      }
      return null;
    });
  }

  ///Gets the name of the stamp passed as [stampId]
  static Future<String> getStampNameById(String stampId) async {
    return await firestore
        .collection(SYSTEM_FOLDER)
        .document(STAMPS_DOCUMENT)
        .get()
        .then((DocumentSnapshot stampsSnapshot) {
      //If the snapshot is not null
      if (stampsSnapshot.data != null) {
        //The snapshot is a map(stampName - stampData), in which the values are another map containing the stamp data
        //Then iterate through each stampData and checks if its id is the same of the one passed in the method
        //If the id is the one searched, returns the value contained in the 'name' key
        String stampName;
        stampsSnapshot.data.values.forEach((map) {
          if (map['id'] == stampId) stampName = map['name'];
        });
        return stampName;
      }
      //print("${Values.PRINT_DEVBOX_PREFIX} FirebaseService.getStampNameById(): returning null");
      return null;
    });
  }

  static Future<String> savePictureToFirebase(File pictureFile) async {
    return await _auth.currentUser().then((FirebaseUser user) async {
      if (user != null) {
        if (pictureFile != null) {
          StorageUploadTask task = storage
              .ref()
              .child(USERS_DATA_STORAGE_FOLDER)
              .child(user.uid)
              .child(USER_PROFILE_PICTURE)
              .putFile(pictureFile);
          StorageTaskSnapshot taskSnapshot = await task.onComplete;
          String url = await taskSnapshot.ref.getDownloadURL();
          return url;
        }
      }
      return null;
    });
  }

  ///Gets the picture of the user currently logged in
  static Future<String> getUserProfilePictureUrl(String uid) async {
    return await getContractorInFirestore().then((contractor) {
      return contractor.pictureUrl;
    });
  }

  ///Gets the ID of the user currently logged in
  static Future<String> getCurrentUserId() async {
    return await _auth.currentUser().then((FirebaseUser user) {
      return user.uid;
    });
  }

  ///Gets a sub-activity by its [activityKey] and [subActivityId], from the system/activities document at Firestore
  static Future<String> findSubActivityByIdOnFirestore(
      String activityKey, String subActivityId) async {
    return await firestore
        .collection(SYSTEM_FOLDER)
        .document(ACTIVITIES_DOCUMENT)
        .get()
        .then((DocumentSnapshot docSnapshot) {
      if (docSnapshot != null) {
        //print("${Values.PRINT_DEVBOX_PREFIX} docSnapshot: ${docSnapshot.data}");
        return docSnapshot[activityKey][subActivityId];
      }
      return null;
    });
  }

  static Future<double> getWalletInfo() async {
    return await getContractorInFirestore().then((Contractor contractor) {
      if (contractor != null) return contractor.wallet;
      return null;
    });
  }
}
