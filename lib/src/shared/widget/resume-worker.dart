import 'package:docelar/src/shared/widget/user-picture-avatar.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class WorkerResume extends StatelessWidget {

  //Card height
  static const double HEIGHT = 132;
  //Amount of stars to be shown
  static const int STARS_MAX_COUNT = 5;
  //Stars size
  static const double STAR_SIZE = 20;
  //Text to be shown when there is no rating
  static const String NO_RATING_TEXT = "Sem avaliação";

  //Icon properties
  final String pictureUrl;

  //Title properties
  final String name;

  //Worker rating in the system
  final double rating;


  WorkerResume(this.pictureUrl, this.name, this.rating);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      //Spacings inside the card

      height: HEIGHT,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ///LEADING - AVATAR
          UserPictureAvatar(pictureUrl),

          const SizedBox(width: 16),

          ///BODY
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                ///TITLE
                Text(name,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 18)),

                ///STARS
                SmoothStarRating(
                  //Whether to use whole number for rating(1.0  or 0.5)
                  allowHalfRating: false,
                  //Rating changed callback
                  onRatingChanged: (value) {},
                  //The maximum amount of stars
                  starCount: STARS_MAX_COUNT,
                  //The current value of rating (double)
                  rating: rating ?? 0,
                  //The size of a single star
                  size: STAR_SIZE,
                  //The body color of star
                  color: Theme.of(context).accentColor,
                  //The border color of star
                  borderColor: Theme.of(context).accentColor,
                  //Spacing between stars(default is 0.0)
                  spacing: 0,
                ),

                //SizedBox(height: 8),

                ///RATING
                Text(
                    rating == null
                        ? NO_RATING_TEXT
                        : rating.toString(),
                    style: const TextStyle(fontSize: 16)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
