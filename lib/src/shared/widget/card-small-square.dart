import 'package:docelar/src/shared/helpers/custom-icons.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:flutter/material.dart';

class CardSmallSquare extends StatelessWidget {
  //Card properties
  final double _borderRadius = 10;
  final double _borderSize = 0;
  final double size;
  final double width;
  final double height;
  static const double defaultSize = 82;

  //Icon properties
  final CustomIcon customIcon;
  final String customIconCode;
  final IconData iconData;
  final String iconUrl;
  static const double iconSize = 32;

  //Title properties
  final String title;
  final double _titleSize = 10;

  CardSmallSquare(
      {this.iconUrl,
      this.customIconCode,
      this.customIcon,
      this.iconData,
      this.size,
      this.width,
      this.height,
      @required this.title});

  ///Chooses the icon to use according to this widget's construction set-up (depends on the parameter passed)
  Widget chooseIcon(BuildContext context) {
    if (iconUrl != null) {
      return Image.network(
        iconUrl,
        height: iconSize,
      );
    } else {
      if (iconData != null)
        return Icon(iconData,
            size: iconSize, color: Theme.of(context).primaryColor);
      else {
        if (customIcon != null)
          return customIcon;
        else
          return CustomIcon(iconCode: customIconCode, size: iconSize);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size ?? height,
      width: size ?? width,
      margin: EdgeInsets.only(right: 8),
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(_borderRadius)),
          border: Border.all(color: Values.mediumGreyText, width: _borderSize)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ///ICON
          chooseIcon(context),

          const SizedBox(height: 8),

          ///TITLE
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(color: Values.BLACK_TEXT, fontSize: _titleSize),
          )
        ],
      ),
    );
  }
}
