import 'package:docelar/src/shared/values.dart';
import 'package:flutter/material.dart';

class SectionText extends StatelessWidget {

  final String _text;

  SectionText(this._text);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(left: 16),
      child: Text(
        _text,
        style: TextStyle(
            color: Values.mediumGreyText, fontSize: 16),
      ),
    );
  }
}
