import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/shared/helpers/custom-icons.dart';
import 'package:docelar/src/shared/services/firebase-service.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/drawer/main-drawer-bloc.dart';
import 'package:docelar/src/shared/widget/drawer/drawer-profile-picture.dart';
import 'package:docelar/src/wallet/wallet-bloc.dart';
import 'package:flutter/material.dart';

class MainDrawer extends StatefulWidget {
  @override
  _MainDrawerState createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {
  final AppBloc appBloc = BlocProvider.getBloc<AppBloc>();
  final WalletBloc walletBloc = BlocProvider.getBloc<WalletBloc>();
  final MainDrawerBloc mainDrawerBloc = BlocProvider.getBloc<MainDrawerBloc>();

  @override
  Widget build(BuildContext context) {
    setState(() {
      mainDrawerBloc.getUserData();
    });

    return Drawer(
      child: ListView(
        padding: EdgeInsets.all(32.0),
        children: <Widget>[
          //User picture, name and email
          _buildUserData(context),

          SizedBox(height: 16),

          //MENU
          MainDrawerTile(
            iconCode: CustomIcon.profile,
            titleText: "Perfil",
            trail: null,
            onTap: () {
              mainDrawerBloc.loadProfileScreen(context);
            },
          ),

          MainDrawerTile(
            iconCode: CustomIcon.wallet,
            titleWidget: Row(
              children: <Widget>[
                Text("Carteira",
                    style: TextStyle(fontSize: 16, color: Values.BLACK_TEXT)),
                SizedBox(width: 30),

                //WALLET PREVIEW
                Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.all(
                            Radius.circular(Values.CARD_BORDER_RADIUS))),
                    padding: EdgeInsets.all(8),
                    child: FutureBuilder(
                      future: walletBloc.getWalletInfo(),
                      builder: (context, snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.none:
                          case ConnectionState.waiting:
                            //Shows a circular progress indicator when there is no data loaded
                            return Container(
                                alignment: Alignment.centerRight,
                                height: 30,
                                width: 30,
                                child: CircularProgressIndicator());
                          default:
                            return Text(
                              snapshot.data,
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16),
                            );
                        }
                      },
                    ))
              ],
            ),
            //titleText: "Carteira",
            trail: null,
            onTap: () {
              mainDrawerBloc.loadWalletScreen(context);
            },
          ),
          MainDrawerTile(
            iconCode: CustomIcon.historic,
            titleText: "Histórico",
            trail: null,
            onTap: () {},
          ),
          MainDrawerTile(
            iconCode: CustomIcon.schedule,
            titleText: "Agenda",
            trail: null,
            onTap: () {},
          ),
          MainDrawerTile(
            iconCode: CustomIcon.gift,
            titleText: "Resgatar presente",
            trail: null,
            onTap: () {},
          ),
          MainDrawerTile(
            iconCode: CustomIcon.config,
            titleText: "Configurações",
            trail: null,
            onTap: () {},
          ),
        ],
      ),
    );
  }

  Widget _buildUserData(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 16),
      height: 170,
      child: Column(
        children: <Widget>[
          Container(
            child: FutureBuilder(
              future: FirebaseService.isUserLoggedIn(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                  case ConnectionState.none:
                    return CircularProgressIndicator();
                  default:
                    if (snapshot.hasData) {
                      return DrawerProfilePicture(
                          mainDrawerBloc.outProfilePictureUrl);
                    } else {
                      return Container();
                    }
                }
              },
            ), //USER PICTURE --------------------
            //Gradient
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(colors: [
                  Color.fromARGB(255, 255, 199, 58),
                  Color.fromARGB(255, 250, 128, 114)
                ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
          ),

          SizedBox(height: 4),

          //USER NAME
          StreamBuilder(
              stream: mainDrawerBloc.outUserName,
              builder: (context, snapshot) {
                //print(snapshot.data);
                if (snapshot.hasData) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("${snapshot.data}",
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Text("(", style: TextStyle(fontWeight: FontWeight.bold)),
                      GestureDetector(
                        child: Text("Sair",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blueAccent)),
                        onTap: () {
                          mainDrawerBloc.signOut(context, appBloc);
                        },
                      ),
                      Text(")", style: TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  );
                } else {
                  return Container();
                }
              }),

          //USER EMAIL
          StreamBuilder<String>(
              stream: mainDrawerBloc.outUserEmail,
              builder: (context, snapshot) {
                return Text("${snapshot.data}");
              })
        ],
      ),
    );
  }
}

///
/// TILE
///
class MainDrawerTile extends StatelessWidget {
  final String iconCode;
  final String titleText;
  final Widget titleWidget;
  final Widget trail;
  final Function onTap;

  ///////////////////////////
  //PARAMETERS OF THE TILE///
  ///////////////////////////

  // Tile/row height
  double tileHeight = 50;

  // Icon properties
  Color iconColor = Values.BLACK_TEXT;
  double iconSize = 32;

  // Spacing between the icon and the title
  double iconAndTitleSpacing = 32;

  // Title properties
  Color titleColor = Values.BLACK_TEXT;
  double titleSize = 16;

  MainDrawerTile(
      {@required this.iconCode,
      this.titleText,
      this.titleWidget,
      this.trail,
      @required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: onTap,
        child: Container(
          height: tileHeight,
          child: Row(
            children: <Widget>[
              //ICON
              CustomIcon(iconCode: iconCode, size: 32),
              //SPACING
              SizedBox(width: iconAndTitleSpacing),
              //TITLE
              titleWidget == null
                  ? Text(titleText,
                      style: TextStyle(fontSize: titleSize, color: titleColor))
                  : titleWidget,
              //TRAIL
              trail ?? Container()
              //If not null, uses the widget passed as trail, otherwise uses an empty Container
            ],
          ),
        ),
      ),
    );
  }
}
