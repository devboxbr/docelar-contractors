import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/user-empty-avatar.dart';
import 'package:docelar/src/shared/widget/user-picture-avatar.dart';
import  'package:flutter/material.dart';

class DrawerProfilePicture extends StatelessWidget {
  final Stream<String> pictureUrlStream;

  DrawerProfilePicture([this.pictureUrlStream]);

  Widget setAvatar() {
    if (pictureUrlStream == null) {
      //If we haven't passed a stream with the url string-----------------------
      return UserEmptyAvatar();
    } else {
      //If we have passed a stream with the url string--------------------------
      return StreamBuilder<String>(
          stream: pictureUrlStream,
          builder: (context, snapshot) {
            //Getting no URL
            if(!snapshot.hasData || snapshot.data.isEmpty || snapshot.data == null) {
              return UserEmptyAvatar();
            } else {
              //Getting a correct URL
              return UserPictureAvatar(snapshot.data);
            }
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return setAvatar();
  }
}
