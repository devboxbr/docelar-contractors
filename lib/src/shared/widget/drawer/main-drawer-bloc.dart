import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/profile/profile-view.dart';
import 'package:docelar/src/shared/helpers/custom-navigation.dart';
import 'package:docelar/src/shared/model/contractor.dart';
import 'package:docelar/src/shared/services/firebase-service.dart';
import 'package:docelar/src/signin/signin-view.dart';
import 'package:docelar/src/wallet/wallet-view.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class MainDrawerBloc extends BlocBase {
  var _userName = BehaviorSubject<String>.seeded("Nome não localizado");
  var _userEmail = BehaviorSubject<String>.seeded("E-mail não localizado");
  var _profilePictureUrl = BehaviorSubject<String>.seeded(null);

  Stream<String> get outUserName => _userName.stream;

  Sink<String> get inUserName => _userName.sink;

  Stream<String> get outUserEmail => _userEmail.stream;

  Sink<String> get inUserEmail => _userEmail.sink;

  Stream<String> get outProfilePictureUrl => _profilePictureUrl.stream;

  Sink<String> get inProfilePictureUrl => _profilePictureUrl.sink;

  Future<bool> getUserData() async {
    if (await FirebaseService.isUserLoggedIn()) {
      await FirebaseService.getContractorInFirestore().then((contractor) {
        if (contractor != null) {
          String fullName = contractor.getFullName();
          if (fullName.isNotEmpty || fullName != null) {
            inUserName.add(fullName);
            inUserEmail.add(contractor.email);
            inProfilePictureUrl.add(contractor.pictureUrl);
          }
          return true;
        }
        return false;
      });
    }
    return false;
  }

  void signOut(BuildContext context, AppBloc appBloc) {
    FirebaseService.signOut();
    appBloc.inContractorIn.add(null);
    CustomNavigation.loadSignInScreen(context);
  }

  @override
  void dispose() {
    super.dispose();
    _userName.close();
    _userEmail.close();
    _profilePictureUrl.close();
  }

  void loadWalletScreen(BuildContext context) {
    CustomNavigation.loadWalletScreen(context);
  }

  void loadProfileScreen(BuildContext context) {
    CustomNavigation.loadProfileScreen(context);
  }
}
