import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/home/home-bloc.dart';
import 'package:docelar/src/home/search/home-search.dart';
import 'package:docelar/src/home/widget/address-quick-pick.dart';
import 'package:docelar/src/shared/widget/button-navigation.dart';
import 'package:flutter/material.dart';

class MainAppBar extends StatelessWidget {
  final Widget child;
  final bool isShowSearch;
  final AppBloc appBloc;

  MainAppBar({@required this.child, @required this.isShowSearch, this.appBloc});

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
        floating: true,
        pinned: true,
        elevation: 0,
        backgroundColor: Colors.grey[50],
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: NavigationButton(),
        ),
        actions: <Widget>[
          //SEARCH ICON BUTTON
          isShowSearch
              ? IconButton(
                  color: Colors.yellow,
                  icon: const Icon(Icons.search, color: Colors.grey),
                  onPressed: () {
                    showSearch(context: context, delegate: HomeSearch(appBloc: appBloc));
                  },
                )
              : null
        ],
        title: child);
  }
}
