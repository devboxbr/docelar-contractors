import 'package:docelar/src/shared/values.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

///Widget with the symbol of an empty avatar to be
///implemented when the user hasn't a profile picture.
class UserEmptyAvatar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(4),

      ///ICON
      child: Icon(FontAwesomeIcons.user, color: Colors.grey[400]),

      width: Values.AVATAR_SCALE,
      height: Values.AVATAR_SCALE,

      ///BACKGROUND CIRCLE
      decoration: BoxDecoration(
          border: Border.all(width: 3, color: Colors.grey[300]),
          shape: BoxShape.circle,
          color: Colors.white),
    );
  }
}
