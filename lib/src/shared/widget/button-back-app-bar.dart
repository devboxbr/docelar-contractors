import 'package:flutter/material.dart';

class BackButtonAppBar extends StatelessWidget {

  final Function callback;
  final Color color;

  BackButtonAppBar({@required this.callback, this.color});

  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: color ?? Colors.black /*Theme.of(context).primaryColor*/,
        ),
        onPressed: callback);
  }
}
