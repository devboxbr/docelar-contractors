import 'package:docelar/src/shared/values.dart';
import 'package:flutter/material.dart';

class UserPictureAvatar extends StatelessWidget {

  final String pictureUrl;
  static const double radius = Values.AVATAR_SCALE/2;

  UserPictureAvatar(this.pictureUrl);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
        radius: radius,
        backgroundColor: Colors.grey[200],
        backgroundImage: NetworkImage(pictureUrl));
  }
}
