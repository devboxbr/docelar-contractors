import 'package:flutter/material.dart';

class NavigationButton extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 4,
      borderRadius: BorderRadius.circular(35),
      color: Theme.of(context).primaryColor,      
      child: Container(
        //padding: EdgeInsets.all(),
//        width: size,
//        height: size,
        child: IconButton(
            icon: Icon(Icons.menu, color: Colors.white),

          onPressed: () {
              Scaffold.of(context).openDrawer();
          },
        )
        
      ),
    );
  }
}
