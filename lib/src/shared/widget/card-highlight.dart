import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/drawer/drawer-profile-picture.dart';
import 'package:flutter/material.dart';

class CardHighlight extends StatelessWidget {
  final double _borderRadius = 10;
  final double _borderSize = 1;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Material(
        elevation: 4,
        borderRadius: BorderRadius.all(Radius.circular(_borderRadius)),
        child: Expanded(
          child: Container(
            height: 200,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(_borderRadius)),
                border:
                    Border.all(color: Values.mediumGreyText, width: _borderSize)),
            child: Row(
              children: <Widget>[

                Padding(
                  padding: EdgeInsets.all(4),
                  child: DrawerProfilePicture(),
                ),
                Column(
                  children: <Widget>[

                    Text("Marcela"),
                    Row(
                      children: <Widget>[
                        Container(
                          width: 4,
                          height: 4,
                          color: Colors.yellow,
                        ),
                        Container(
                          width: 4,
                          height: 4,
                          color: Colors.yellow,
                        ),
                        Container(
                          width: 4,
                          height: 4,
                          color: Colors.yellow,
                        ),
                        Container(
                          width: 4,
                          height: 4,
                          color: Colors.yellow,
                        ),
                        Container(
                          width: 4,
                          height: 4,
                          color: Colors.yellow,
                        )
                      ],
                    ),
                    Text("5,0")

                  ],
                )

              ],
            ),
          ),
        ),
      ),
    );
  }
}
