import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:docelar/src/shared/services/firebase-service.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/card-small-square.dart';
import 'package:flutter/material.dart';

///Widget containing a cards list for a quick access to the categories
class CardListCategories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseService.firestore
          .collection(FirebaseService.SYSTEM_FOLDER)
          .document(FirebaseService.WORKER_CATEGORIES_DOCUMENT)
          .get()
          .asStream(),

      builder: (BuildContext context,
          AsyncSnapshot<DocumentSnapshot> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
          //Shows a circular progress indicator when there is no data loaded
            return Center(
                child: Container(
                    width: 50,
                    height: 50,
                    child: CircularProgressIndicator()
                ));
          default:
          //print("${Values.PRINT_DEVBOX_PREFIX} ${snapshot.data.data.keys}");
            Map<String, String> categories = Map.from(snapshot.data.data);
            //print(categories);

            return Container(
              height: CardSmallSquare.defaultSize,
              padding: EdgeInsets.only(left: 16),
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: categories.keys.map((key) {
                  return CardSmallSquare(
                    title: key.toString(),
                    size: CardSmallSquare.defaultSize,
                    iconUrl: categories[key],
                  );
                }).toList(),
              ),
            );

        /*print("${Values.PRINT_DEVBOX_PREFIX} $categories");
            //categoriesMap.map((categoryName) => print(categoryName)).toList();
            return Container();
            */
        }
      },
    );
  }
}
