import 'package:docelar/src/shared/model/address.dart';
import 'package:docelar/src/shared/model/rating.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:flutter/material.dart';

class CardAddress extends StatelessWidget {
  final int cardIndex;
  final Address address;
  final bool isMainAddress;
  static const double CARD_HEIGHT = 100;
  static const double EXTERNAL_MARGIN = 16;
  static const int MAX_LINES_FULL_ADDRESS = 3;

  CardAddress({this.cardIndex, @required this.address, this.isMainAddress});

  @override
  Widget build(BuildContext context) {
    //The screen width - Content Paddings and Margins - the icon's container width - an error margin
    double commentsMaxWidth = MediaQuery.of(context).size.width -
        (EXTERNAL_MARGIN * 3) -
        CARD_HEIGHT -
        16;

    return Container(
        //height: CARD_HEIGHT,
        //Spacing outside the card
        margin: const EdgeInsets.symmetric(
            horizontal: EXTERNAL_MARGIN, vertical: 4),

        //decoration with bg color and SHADOW
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.grey[400], blurRadius: 2, offset: Offset(0, 2))
          ],
          borderRadius:
              BorderRadius.all(Radius.circular(Values.CARD_BORDER_RADIUS)),
          //border: Border.all(color: Values.mediumGreyText, width: _borderSize)
        ),
        child: Row(
          children: <Widget>[
            ///LEADING
            Stack(
              children: <Widget>[
                Container(
                  height: CARD_HEIGHT,
                  width: CARD_HEIGHT,
                  padding: const EdgeInsets.all(16),
                  child: CircleAvatar(
                    backgroundColor: Colors.grey[50],
                    child: Text(
                      (cardIndex + 1).toString(),
                      style: TextStyle(
                          fontSize: 40, color: Theme.of(context).accentColor),
                    ),
                  ),
                ),

                ///Main address mark
                isMainAddress
                    ? Positioned(
                        top: 65,
                        left: -35,
                        child: Transform(
                          child: Container(
                            alignment: Alignment.center,
                            width: 150,
                            height: 20,
                            color: Theme.of(context).accentColor,
                            child: Text(
                              "PRINCIPAL",
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                          transform: Matrix4.rotationZ(-0.7),
                        ),
                      )
                    : Container()
              ],
            ),

            ///SEPARATOR LINE
            Container(
              width: 1,
              height: CARD_HEIGHT - 32,
              color: Colors.grey[200],
            ),

            ///CONTENT
            Container(
              padding: const EdgeInsets.symmetric(vertical: 8),
              margin: const EdgeInsets.only(left: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ///CARD TITLE - ADDRESS LABEL
                  Text(
                    address.label,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),

                  SizedBox(height: 4),

                  ///ADDRESS DATA
                  Container(
                    width: commentsMaxWidth,
                    child: Text(
                      address.getFullAddress(),
                      maxLines: MAX_LINES_FULL_ADDRESS,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.grey),
                    ),
                  )
                  /*rating.comment != null
                      ? rating.comment.isNotEmpty
                          ? Container(
                              width: commentsMaxWidth,
                              child: Text(
                                rating.comment,
                                maxLines: COMMENT_MAX_LINES,
                                overflow: TextOverflow.ellipsis,
                              ))
                          : Container()
                      : Container()*/
                ],
              ),
            ),
          ],
        ));
  }
}
