import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/shared/model/user.dart';
import 'package:flutter/material.dart';

class Dialogs {
  ///Shows a dialog with an "OK" button, with the [title] and [contentText] passed
  static void showDialogWithOkButton(
      BuildContext context, String title, String contentText) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Theme.of(context).primaryColor),
            ),
            content: Text(contentText),
            actions: <Widget>[
              FlatButton(
                  child: Text("OK"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  })
            ],
          );
        });
  }

  ///Shows a dialog containing a "YES" and a "NO" buttons, showing the
  ///[title] and the [contentText] passed, as well as calling one of the
  ///callback functions passed, according to the button clicked by the user
  static void showConfirmationYesNoDialog(
      {@required BuildContext context,
      @required String title,
      @required String contentText,
      @required Function yesCallback,
      @required Function noCallback}) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Theme.of(context).primaryColor),
            ),
            content: Text(contentText),
            actions: <Widget>[
              FlatButton(
                child: Text("SIM"),
                onPressed: () {
                  Navigator.of(context).pop();
                  yesCallback();
                },
              ),
              FlatButton(
                child: Text("NÃO"),
                onPressed: () {
                  Navigator.of(context).pop();
                  noCallback();
                },
              )
            ],
          );
        });
  }

  ///Shows a dialog with a CircularProgressBar while something is loading
  ///Dismiss it using Navigator.of(context).pop() anytime and anywhere, after showing the dialog
  static void showLoadingDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return Center(
            child: Container(
                padding: EdgeInsets.all(24),
                decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                height: 100,
                width: 100,
                child: CircularProgressIndicator()),
          );
        });
  }

  ///Shows a dialog with an "OK" button, with the [title] and [contentWidget] passed
  static void showDialogWithItemsList({
      @required BuildContext context, @required List<Widget> contentWidgets,
      Widget title}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: title,
            content: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: contentWidgets,
              ),
            ),
          );
        });
  }

  ///Shows a dialog with a form to edit user address
  static void showConfirmationSaveCancelDialog(
      BuildContext context, Widget contentWidget, Function saveCallback) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            content: contentWidget,
            actions: <Widget>[
              FlatButton(
                child: Text("CANCELAR"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text("SALVAR"),
                onPressed: () {
                  saveCallback();
                },
              )
            ],
          );
        });
  }
}
