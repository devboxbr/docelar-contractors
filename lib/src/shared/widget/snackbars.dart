import 'package:docelar/src/shared/helpers/custom-navigation.dart';
import 'package:flutter/material.dart';

class SnackBars {
  ///Shows a red default snackbar with no action
  static void showErrorSnackBar(String content, BuildContext context,
      {GlobalKey<ScaffoldState> scaffoldKey}) {
    Widget snackBar = SnackBar(
        content: Text(content,
            overflow: TextOverflow.ellipsis,
            maxLines: 3,
            style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.red[900]);
    if (scaffoldKey == null)
      Scaffold.of(context).showSnackBar(snackBar);
    else
      scaffoldKey.currentState.showSnackBar(snackBar);
  }

  ///Shows a default snackbar with no action
  static void showMessageSnackBar(String content, BuildContext context,
      {GlobalKey<ScaffoldState> scaffoldKey}) {
    Widget snackBar = SnackBar(
        content: Text(content, style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.black54);
    if (scaffoldKey == null)
      Scaffold.of(context).showSnackBar(snackBar);
    else
      scaffoldKey.currentState.showSnackBar(snackBar);
  }

  static void showMessageSnackBarWithAction(String content,
      BuildContext context, String textAction, Function onPressed,
      {GlobalKey<ScaffoldState> scaffoldKey}) {
    Widget snackBar = SnackBar(
      content: Text(content, style: TextStyle(color: Colors.white)),
      backgroundColor: Colors.black54,
      action: SnackBarAction(label: textAction, onPressed: onPressed),
    );
    if (scaffoldKey == null)
      Scaffold.of(context).showSnackBar(snackBar);
    else
      scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
