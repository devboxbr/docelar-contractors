import 'package:flutter/material.dart';

ThemeData appTheme() {
  return ThemeData(
      primaryColor: Color.fromARGB(255, 33, 70, 130),
      accentColor: Color.fromARGB(255, 252, 217, 75)
  );
}

