import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class CustomTextInputFormatters {
  static final phoneFormatter =  MaskTextInputFormatter(mask: '(##) #####-####', filter: { "#": RegExp(r'[0-9]') });
  static final cpfFormatter =  MaskTextInputFormatter(mask: '###.###.###-##', filter: { "#": RegExp(r'[0-9]') });
  static final cepFormatter =  MaskTextInputFormatter(mask: '#####-###', filter: { "#": RegExp(r'[0-9]') });
  //static final realFormatter =  MaskTextInputFormatter(mask: '##,##', filter: { "#": RegExp(r'[0-9]') }); //Not working yet
}