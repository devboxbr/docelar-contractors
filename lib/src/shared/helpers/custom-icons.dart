import 'package:flutter/material.dart';

///Creates an icon widget with any image created by Devbox
class CustomIcon extends StatelessWidget {
  /// In order to set a new icon, just put the new png file in [path]'s folder
  /// And create a new static const String with its name, just like the others
  //Drawer
  static const String profile = "profile";
  static const String wallet = "wallet";
  static const String historic = "historic";
  static const String schedule = "schedule";
  static const String gift = "gift";
  static const String config = "config";
  static const String logout = "logout";

  //Categories
  static const String petwalker = "petwalker";
  static const String gardener = "gardener";
  static const String babysitter = "babysitter";
  static const String elder = "elder";

  //Forms
  static const String contact = "contact";
  static const String password = "password";

  //Wallet
  static const String finance_history = "finance_history";
  static const String help = "help";
  static const String money_back = "money_back";
  static const String transfer_money = "transfer_money";
  static const String charge_to_friend = "charge_to_friend";

  //Essential
  static const String path = "resources/icons/";
  static const String imageFormat = ".png";

  //Patterns
  static const double defaultSize = 32;

  //Parameters
  final String iconCode;
  final double size;
  final Color color;

  CustomIcon({@required this.iconCode, this.size, this.color});

  @override
  Widget build(BuildContext context) {
    return Image.asset("$path$iconCode$imageFormat",
        height: size ?? defaultSize, width: size ?? defaultSize, color: color ?? null);
  }
}
