import 'package:docelar/src/home/home-view.dart';
import 'package:docelar/src/profile/edit-address-view.dart';
import 'package:docelar/src/profile/profile-view.dart';
import 'package:docelar/src/shared/model/address.dart';
import 'package:docelar/src/signin/signin-view.dart';
import 'package:docelar/src/signup/sign-up-address-view.dart';
import 'package:docelar/src/signup/signup-personal-view.dart';
import 'package:docelar/src/wallet/wallet-view.dart';
import 'package:flutter/material.dart';

class CustomNavigation {

  static final walletView = WalletView();
  static final signUpView = SignUpView();
  static final signInView = SignInView();
  static final homeView = HomeView();
  static final signUpAddressView = SignUpAddressView();
  static final profileView = ProfileView();

  static const PUSH_REPLACEMENT = "pushReplacement";
  static const PUSH_ONLY = "push";


  ///Load address screen
  static void loadScreen(BuildContext context, String typeOfPush, Widget screen) {
    switch(typeOfPush) {
      case PUSH_ONLY:
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => signUpAddressView));
        break;
      case PUSH_REPLACEMENT:
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => signUpAddressView));
        break;
    }
  }

  ///Load address screen
  static void loadAddressSignUpScreen(BuildContext context) {
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => signUpAddressView));
  }

  ///Load home screen
  static void loadHomeScreen(BuildContext context) {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => homeView));
  }

  ///Load sign-in screen
  static void loadSignInScreen(BuildContext context) {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => signInView));
  }

  ///Load sign-up screen
  static void loadSignUpScreen(BuildContext context) {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => signUpView));
  }

  ///Load wallet screen
  static void loadWalletScreen(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => walletView));
  }

  ///Load profile screen
  static void loadProfileScreen(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => profileView));
  }

  ///Load edit address from profile screen and loads [address] information to the form
  static void loadEditAddressScreen(BuildContext context, Address address, int index) {
    final editAddressView = EditOrCreateAddressView(address, index);

    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => editAddressView));
  }

  ///Load edit address from profile screen and loads [address] information to the form
  static void loadCreateAddressScreen(BuildContext context) {
    final editAddressView = EditOrCreateAddressView(null, null);

    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => editAddressView));
  }
}