import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/signin/widget/text-field-login.dart';
import 'package:docelar/src/signin/signin-bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SignInView extends StatefulWidget {
  @override
  _SignInViewState createState() => _SignInViewState();
}

class _SignInViewState extends State<SignInView> {
  final _formKey = GlobalKey<FormState>(debugLabel: "signin-key");

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool hideText = false;

  final SignInBloc signInBloc = BlocProvider.getBloc<SignInBloc>();
  final AppBloc appBloc = BlocProvider.getBloc<AppBloc>();


  @override
  void dispose() {
    super.dispose();
    emailController.dispose();
    passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.transparent));

    return Scaffold(
        resizeToAvoidBottomPadding: true,
        body: Builder(builder: (BuildContext scaffoldContext) {
          return Stack(children: <Widget>[
            //BACKGROUND
            Image.asset(
              "resources/images/bg_recife_antigo.png",
              fit: BoxFit.cover,
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.center,
            ),

            //FORM
            Form(
              key: _formKey,
              child: ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                shrinkWrap: true,
                //crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  const SizedBox(height: 100),

                  //LOGO DOCE LAR
                  Image.asset(
                    "resources/images/logo_docelar.png",
                    width: 200,
                    height: 50,
                    alignment: Alignment.center,
                  ),

                  const SizedBox(height: 100),

                  //EMAIL FIELD
                  TextFieldLogin("E-mail", Icons.email, (value) {
                    return signInBloc.validateEmail(value);
                  }, emailController, false, TextInputType.emailAddress,
                      signInBloc),

                  const SizedBox(height: 16),

                  //PASSWORD FIELD
                  TextFieldLogin("Senha", Icons.lock, (value) {
                    return signInBloc.validatePassword(value);
                  }, passwordController, true, TextInputType.text, signInBloc),

                  const SizedBox(height: 16),

                  //SHOW-HIDE PASSWORD AND FORGOT PASSWORD
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      /*
                      //CHECKBOX
                      StreamBuilder<bool>(
                        stream: signInBloc.outObscurePasswordText,
                        builder: (context, snapshot) {
                          return Checkbox(
                            onChanged: (value){
                              signInBloc.inObscurePasswordText.add(value);
                            }, value: snapshot.data ?? true,
                          );
                        }
                      ),

                      //TEXT HIDE PASSWORD0
                      Text("Esconder senha", style: TextStyle(color: Colors.white)),

                      */

                      //SEPARATOR
                      const Expanded(child: const SizedBox(height: 8)),

                      //FORGOT PASSWORD
                      GestureDetector(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 16),
                          child: Text(
                            "Esqueci minha senha",
                            textAlign: TextAlign.right,
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                        onTap: (){
                          String email = emailController.text;

                          signInBloc.resetPassword(email, scaffoldContext);
                        },
                      ),
                    ],
                  ),

                  const SizedBox(height: 32),

                  //BUTTON SIGN-IN
                  SizedBox(
                    height: Values.BUTTON_HEIGHT,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(35)),
                      onPressed: () async {
                        // Validate returns true if the form is valid, otherwise false.
                        if (_formKey.currentState.validate()) {
                          signInBloc.inEmail.add(emailController.text);
                          signInBloc.inPassword.add(passwordController.text);

                          signInBloc.signUserIn(scaffoldContext, appBloc);
                        }
                      },
                      color: Theme.of(context).accentColor,
                      child: const Text(
                        "ENTRAR",
                        style: const TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),

                  const SizedBox(height: 16),

                  //SIGN-UP BUTTON
                  SizedBox(
                    height: 32,
                    child: GestureDetector(
                      onTap: () {
                        signInBloc.loadSignUpScreen(context);
                      },
                      child: Text(
                        "Criar conta",
                        textAlign: TextAlign.center,
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                  ),

/*

                  SizedBox(height: 32),
                  Text("OU", style: TextStyle(color: Colors.white, fontSize: 18), textAlign: TextAlign.center,),
                  SizedBox(height: 32),

                  //FACEBOOK LOGIN BUTTON
                  Container(
                    height: 58,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(

                          borderRadius: BorderRadius.circular(35)),
                      onPressed: () {
                        //signInBloc.signUserInWithFacebook(scaffoldContext);

                        */
/*showDialog(context: scaffoldContext, builder: (context) {
                          return OkAlertDialog("Entrar com Facebook", "Indisponível");
                        });*/ /*

                        Scaffold.of(scaffoldContext).showSnackBar(SnackBar(content: Text("Indisponível!")));

                      },
                      color: Colors.blue[900],
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(FontAwesomeIcons.facebookSquare,
                              color: Colors.white),
                          SizedBox(width: 16),
                          Text(
                            "Entrar com Facebook",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ),

                  SizedBox(height: 16),

                  //GMAIL LOGIN BUTTON
                  Container(
                    height: 58,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(35)),
                      onPressed: () {
                        signInBloc.signUserInWithGoogle(scaffoldContext);
                        */
/*showDialog(context: scaffoldContext, builder: (context) {
                          return OkAlertDialog("Entrar com Gmail", "Indisponível");
                        });*/ /*

                      },
                      color: Colors.red,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(FontAwesomeIcons.google, color: Colors.white),
                          SizedBox(width: 16),
                          Text(
                            "Entrar com Gmail",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ),
*/

                  const SizedBox(height: 32),

                  //TEXT "POWERED BY"
                  Image.asset(
                    "resources/images/powered_by_text.png",
                    width: 73,
                    height: 13,
                    alignment: Alignment.center,
                  ),

                  //LOGO DEVBOX
                  Image.asset(
                    "resources/images/logo_devbox.png",
                    width: 119,
                    height: 31,
                    alignment: Alignment.center,
                  ),

                  const SizedBox(height: 48),
                ],
              ),
            ),
          ]);
        }));
  }
}
