import 'dart:ui';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/shared/helpers/custom-navigation.dart';
import 'package:docelar/src/shared/model/contractor.dart';
import 'package:docelar/src/shared/services/firebase-service.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/dialogs.dart';
import 'package:docelar/src/shared/widget/snackbars.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class SignInBloc extends BlocBase {
  FirebaseUser currentUser;

  var _email = BehaviorSubject<String>.seeded("");
  var _password = BehaviorSubject<String>.seeded("");
  var _obscurePasswordText = BehaviorSubject<bool>.seeded(true);

  Stream<String> get outEmail => _email.stream;

  Sink<String> get inEmail => _email.sink;

  Stream<String> get outPassword => _password.stream;

  Sink<String> get inPassword => _password.sink;

  Stream<bool> get outObscurePasswordText => _obscurePasswordText.stream;

  Sink<bool> get inObscurePasswordText => _obscurePasswordText.sink;

  //Method to validate the email's text field of the login form
  String validateEmail(String value) {
    if (value.isEmpty) {
      //If the field is empty
      return "Informe seu e-mail";
    } else if (!value.contains("@")) {
      //If the text hasn't an @ symbol, consider as an invalid email
      return "Informe um e-mail";
    } else if (value.contains(" ")) {
      //If the text has som blank space
      return "E-mails não contêm espaços em branco";
    } else if (!value.contains(new RegExp(".+@.+\..+"))) {
      //Checks if the email has an email formatting
      return "Informe um e-mail válido";
    } else {
      return null;
    }
  }

  //Method to validate the password's text field of the login form
  String validatePassword(String value) {
    if (value.isEmpty) {
      //If the password text is empty
      return "Informe sua senha";
    } else if (value.length < 6) {
      //If the password has less than 6 characters
      return "Obrigatório ter mais de 6 caracteres";
    } else {
      return null;
    }
  }
  /// Errors:
  ///   ERROR_INVALID_EMAIL - If the email address is malformed.
  ///   ERROR_WRONG_PASSWORD - If the password is wrong.
  ///   ERROR_USER_NOT_FOUND - If there is no user corresponding to the given [email] address, or if the user has been deleted.
  ///   ERROR_USER_DISABLED - If the user has been disabled (for example, in the Firebase console)
  ///   ERROR_TOO_MANY_REQUESTS - If there was too many attempts to sign in as this user.
  ///   ERROR_OPERATION_NOT_ALLOWED - Indicates that Email & Password accounts are not enabled.
  void signUserIn(BuildContext context, AppBloc appBloc) async {
    Dialogs.showLoadingDialog(context);

    currentUser =
        await FirebaseService.signUserIn(_email.value, _password.value)
            .catchError((onError) {
      if (onError.toString().contains("ERROR_INVALID_EMAIL")) {
        SnackBars.showErrorSnackBar("E-mail inválido!", context);
      } else if (onError.toString().contains("ERROR_USER_NOT_FOUND")) {
        SnackBars.showErrorSnackBar("Usuário não encontrado!", context);
      } else if (onError.toString().contains("ERROR_WRONG_PASSWORD")) {
        SnackBars.showErrorSnackBar("Senha incorreta!", context);
      } else if (onError.toString().contains("ERROR_USER_DISABLED")) {
        SnackBars.showErrorSnackBar("Usuário desativado!", context);
      } else if (onError.toString().contains("ERROR_TOO_MANY_REQUESTS")) {
        SnackBars.showErrorSnackBar("Muitas tentativas sucessivas, aguarde um pouco.", context);
      } else if (onError.toString().contains("ERROR_OPERATION_NOT_ALLOWED")) {
        SnackBars.showErrorSnackBar("Este método de login não está ativo.", context);
      }
    });

    if (currentUser != null) {
      Contractor contractor = await FirebaseService.getContractorInFirestore();
      if(contractor != null)
        //appBloc.inContractorIn.add(contractor);
      _loadHomeScreen(context);
      //TODO: Save this user to shared preferences aiming an automatic sign in next time
    } else {
      print("${Values.PRINT_DEVBOX_PREFIX} Error to log in with firebase!");
      //Popping the CircularProgressIndicator() dialog
      Navigator.of(context).pop();
      //Feedback to the user
      SnackBars.showErrorSnackBar("Não foi possível entrar. Verifique sua conexão com a internet!", context);
    }
  }
/*
  void signUserInWithFacebook(BuildContext context) async {
    FirebaseService.signOut();
    String error;
    currentUser = await FirebaseService.signInWithFacebook();

    if (currentUser != null) {
      _loadHomeScreen(context);
    } else {
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text("Erro: $error")));
    }
  }

  void signUserInWithGoogle(BuildContext context) async {
    FirebaseUser user = await FirebaseService.signInWithGoogleCredentials();

    if (user != null) {
      _loadHomeScreen(context);
    }
  }
*/
  void _loadHomeScreen(BuildContext context) {
    CustomNavigation.loadHomeScreen(context);
  }

  void loadSignUpScreen(BuildContext context) {
    CustomNavigation.loadSignUpScreen(context);
  }

  ///Calls the FirebaseService class to reset the password and returns an [errorCaught]
  ///containing a message to the user about the problem
  Future resetPassword(String email, BuildContext context) async {
    //If the email field is empty
    if(email.isEmpty)
      //Shows a snackbar about the error to the user
      SnackBars.showErrorSnackBar("Insira o e-mail da sua conta!", context);
    else if(email.isNotEmpty) {
      //If the email field is NOT empty, tries to reset the password
      String errorCaught = await FirebaseService.resetPassword(email);
      //And if the reset method returns a NOT NULL string
      if(errorCaught != null) {
        //An error was caught and passed to this string
        //And the user will see a snackbar about the error
        SnackBars.showErrorSnackBar(errorCaught, context);
      } else {
        //If the reset method returns a NULL, no error was caught
        //And the user will be informed to check his email
        SnackBars.showMessageSnackBar("Confira seu e-mail!", context);
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
    _email.close();
    _password.close();
    _obscurePasswordText.close();
  }

  @override
  void notifyListeners() {
    super.notifyListeners();
  }

  @override
  void removeListener(VoidCallback listener) {
    super.removeListener(listener);
  }

  @override
  void addListener(VoidCallback listener) {
    super.addListener(listener);
  }

  @override
  bool get hasListeners {
    return null;
  }
}
