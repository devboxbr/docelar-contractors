import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/signin/signin-bloc.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TextFieldLogin extends StatelessWidget {
  final String _hintText;
  final IconData _icon;
  final Function(String) _validator;
  final TextEditingController _fieldController;
  final bool _obscureText;
  final TextInputType _textInputType;
  final SignInBloc _signInBloc;
  final List inputFormatters;

  TextFieldLogin(
      this._hintText,
      this._icon,
      this._validator,
      this._fieldController,
      this._obscureText,
      this._textInputType,
      this._signInBloc,
      {this.inputFormatters});

  @override
  Widget build(BuildContext context) {
    //_signInBloc.inObscurePasswordText.add(this._obscureText);

    return StreamBuilder(
        stream: _signInBloc.outObscurePasswordText,
        builder: (context, snapshot) {
          return TextFormField(
            inputFormatters: inputFormatters,
            obscureText: this._obscureText ? (snapshot.data ?? false) : false,
            style: TextStyle(color: Colors.white),
            keyboardType: _textInputType,
            decoration: InputDecoration(
                suffixIcon: this._obscureText
                    ? IconButton(
                        icon: Icon(
                            snapshot.data != null
                                ? (snapshot.data
                                    ? FontAwesomeIcons.eyeSlash
                                    : FontAwesomeIcons.eye)
                                : null,
                            color: Colors.white),
                        onPressed: () {
                          _signInBloc.inObscurePasswordText.add(!snapshot.data);
                        })
                    : null,
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                filled: true,
                fillColor: Color.fromARGB(80, 255, 255, 255),
                border: OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Colors.transparent),
                    borderRadius:
                        const BorderRadius.all(const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS))),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Colors.transparent),
                    borderRadius:
                        const BorderRadius.all(const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS))),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context).accentColor, width: 1),
                    borderRadius:
                        const BorderRadius.all(const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS))),
                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red, width: 1),
                    borderRadius:
                        const BorderRadius.all(const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS))),
                focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red, width: 1),
                    borderRadius:
                        const BorderRadius.all(const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS))),
                errorStyle: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: Colors.red[200]),
                hintText: _hintText,
                hintStyle: TextStyle(color: Colors.white),
                prefixIcon: Icon(_icon, color: Colors.white)),

            validator: _validator,
            //onSaved: ,
            controller: _fieldController,
          );
        });
  }
}
