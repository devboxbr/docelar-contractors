import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/shared/services/firebase-service.dart';

class WorkerDetailsBloc extends BlocBase {

  Future<String> getIconLink(String iconName) async {
    return await FirebaseService.getIconLinkFromFirestore(iconName);
  }

  Future<String> findStampNameById(String stampId) async {
    return await FirebaseService.getStampNameById(stampId);
  }

  Future<String> findSubActivityByIdOnFirestore(String activityKey, String subActivityId) async {
    return await FirebaseService.findSubActivityByIdOnFirestore(activityKey, subActivityId);
  }

  @override
  void dispose() {
    super.dispose();
  }
}