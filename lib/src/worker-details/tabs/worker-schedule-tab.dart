import 'package:docelar/src/shared/model/worker.dart';
import 'package:docelar/src/worker-details/worker-details-bloc.dart';
import 'package:flutter/material.dart';

class WorkerScheduleTab extends StatelessWidget {
  final String _tabName = "schedule";
  final Worker _worker;
  final WorkerDetailsBloc workerDetailsBloc;

  WorkerScheduleTab(this._worker, this.workerDetailsBloc);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Builder(
        builder: (BuildContext context) {
          return CustomScrollView(
            key: PageStorageKey<String>(_tabName),
            slivers: <Widget>[
              SliverOverlapInjector(
                // This is the flip side of the SliverOverlapAbsorber above.
                handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              ),
              SliverPadding(
                padding: const EdgeInsets.all(8.0),
                sliver: SliverFixedExtentList(
                  itemExtent: 48.0,
                  delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                      return ListTile(
                        title: Text('Item $index'),
                      );
                    },
                    childCount: 30,
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}