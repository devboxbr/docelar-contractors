import 'package:docelar/src/shared/model/worker.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/section-text.dart';
import 'package:docelar/src/worker-details/widget/rating-card.dart';
import 'package:docelar/src/worker-details/worker-details-bloc.dart';
import 'package:flutter/material.dart';

class WorkerRatingsTab extends StatelessWidget {
  final String _tabName = "ratings";
  final Worker _worker;
  final WorkerDetailsBloc workerDetailsBloc;

  WorkerRatingsTab(this._worker, this.workerDetailsBloc);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: false,
      child: Builder(
        builder: (BuildContext context) {
          return CustomScrollView(
            key: PageStorageKey<String>(_tabName),
            slivers: <Widget>[
              SliverOverlapInjector(
                handle:
                    NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              ),
              _buildRatingsList(context)
            ],
          );
        },
      ),
    );
  }

  Widget _buildRatingsList(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate([
        SizedBox(
          height: 16,
        ),
        SectionText("Minhas avaliações"),
        SizedBox(
          height: 8,
        ),
        ListView.builder(itemBuilder: (context, index) {
          if (_worker.ratings != null && _worker.ratings.length > 0) {
            //print("${Values.PRINT_DEVBOX_PREFIX} _worker.ratings[index]: ${_worker.ratings[index]}");
            return RatingCard(rating: _worker.ratings[index], index: index,);
          } else {
            return Container(
                padding: EdgeInsets.only(left: 32),
                height: 50,
                child: Text("Sem avaliações ainda."));
          }
        },
          itemCount: _worker.ratings != null ? _worker.ratings.length : 1,
          shrinkWrap: true,
        )
      ]),
    );
  }
}
