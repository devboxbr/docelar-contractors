import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/shared/model/stamp.dart';
import 'package:docelar/src/shared/model/worker.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/card-small-square.dart';
import 'package:docelar/src/shared/widget/section-text.dart';
import 'package:docelar/src/shared/widget/user-picture-avatar.dart';
import 'package:docelar/src/shared/widget/resume-worker.dart';
import 'package:docelar/src/worker-details/worker-details-bloc.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class WorkerAboutTab extends StatelessWidget {
  final String _tabName = "about";
  final Worker _worker;
  final WorkerDetailsBloc _workerDetailsBloc;

  WorkerAboutTab(this._worker, this._workerDetailsBloc);

  /*void createStamps() {
    Stamp stamp1 = Stamp("Deficiente", DateTime.now(), 60, "Nada");
    stamp1.id = "deficient";
    Stamp stamp2 = Stamp("Especialista em pisos", DateTime.now(), 60, "Nada");
    stamp2.id = "floor";
    Stamp stamp3 =
        Stamp("Verificado(a) pessoalmente", DateTime.now(), 60, "Nada");
    stamp3.id = "verified";
    Stamp stamp4 = Stamp("Bons modos", DateTime.now(), 60, "Nada");
    stamp4.id = "manners";

    _worker.stamps = [stamp1, stamp2, stamp3, stamp4];
  }*/

  @override
  Widget build(BuildContext context) {
    //createStamps();

    //print("${Values.PRINT_DEVBOX_PREFIX} _worker.stamps.length: ${_worker.stamps != null ? _worker.stamps.length : "no stamps"}");

    return SafeArea(
      top: true,
      bottom: false,
      child: Builder(
        builder: (BuildContext context) {
          return CustomScrollView(
            key: PageStorageKey<String>(_tabName),
            slivers: <Widget>[
              SliverOverlapInjector(
                handle:
                    NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              ),
              SliverList(
                delegate: SliverChildListDelegate([
                  //Picture, name and rating
                  _buildPersonalData(context),

                  //Section title
                  SectionText("Selos e certificados"),
                  const SizedBox(height: 16),

                  //_buildStamps(context),
                ]),
              ),
              _buildStamps(context),
              const SliverToBoxAdapter(
                child: const SizedBox(height: 16),
              ),
              SliverToBoxAdapter(
                  child: SectionText("Atividades que desempenho")),
              const SliverToBoxAdapter(
                child: const SizedBox(height: 8),
              ),
              _buildActivities(context),
            ],
          );
        },
      ),
    );
  }

  ///Finds and organizes the worker's personal data (only the parte available to the contractor)
  Widget _buildPersonalData(BuildContext context) {
    return Container(
      //Spacing outside the card
      child: WorkerResume(_worker.pictureUrl, _worker.name, _worker.rating),
    );
  }

  ///Finds and organizes all the stamps at the worker's account
  Widget _buildStamps(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        padding: const EdgeInsets.only(left: 16),
        height: CardSmallSquare.defaultSize,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: _worker.stamps != null ? _worker.stamps.length : 1,
          itemBuilder: (context, index) {
            if (_worker.stamps == null) {
              //print("_worker.stamps = null");
              return CardSmallSquare(
                  iconData: Icons.not_interested, title: "Sem estampas");
            } else {
              //print("${Values.PRINT_DEVBOX_PREFIX} _worker.stamps = NOT null | length = ${_worker.stamps.length}");

              return FutureBuilder(
                //Future here = the icon link of this stamp
                future:
                    _workerDetailsBloc.getIconLink(_worker.stamps[index].id),
                builder: (BuildContext context, AsyncSnapshot urlSnapshot) {
                  switch (urlSnapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                      return Center(
                        child: Container(
                            padding: const EdgeInsets.all(16),
                            width: CardSmallSquare.defaultSize,
                            height: CardSmallSquare.defaultSize,
                            child: const CircularProgressIndicator()),
                      );
                    default:
                      return FutureBuilder(
                        //Future here = the name of this stamp
                        future: _workerDetailsBloc
                            .findStampNameById(_worker.stamps[index].id),
                        builder: (BuildContext context,
                            AsyncSnapshot stampTitleSnapshot) {
                          switch (stampTitleSnapshot.connectionState) {
                            case ConnectionState.none:
                            case ConnectionState.waiting:
                              return Center(
                                child: Container(
                                    padding: const EdgeInsets.all(16),
                                    width: CardSmallSquare.defaultSize,
                                    height: CardSmallSquare.defaultSize,
                                    child: const CircularProgressIndicator()),
                              );
                            default:
                              return CardSmallSquare(
                                iconUrl: urlSnapshot.data,
                                //TODO: se precaver contra null
                                title: stampTitleSnapshot.data ??
                                    "Selo não localizado",
                              );
                          }
                        },
                      );
                  }
                },
              );
            }
          },
        ),
      ),
    );
  }

  Widget _buildActivities(BuildContext context) {
    //Height of the list tile
    double tileHeight = 20;

    //If there are activities registered int the worker's account
    if (_worker.activities != null) {
      return SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
        //print("${Values.PRINT_DEVBOX_PREFIX} Index = ${index ?? "index nulo"}");
        String key = _worker.activities.keys.elementAt(index);
        //print("${Values.PRINT_DEVBOX_PREFIX} Key = ${key ?? "key nula"}");

        //print("${Values.PRINT_DEVBOX_PREFIX} Key($key) = ${_worker.activities[key]}");
        List activities = _worker.activities[key];

        //if the current activity has a list of sub-activities
        if (activities != null) {
          if (activities.length > 0) {
            //print("${Values.PRINT_DEVBOX_PREFIX} activities list has items");

            ///The parent activity with a list of sub-activities
            return Container(
              //margin: EdgeInsets.symmetric(horizontal: 16),
              height: (activities.length + _worker.activities.keys.length) *
                  tileHeight, //Sums of all lists * the size of each tile
              child: ListView.builder(
                itemCount: activities.length + 1,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int position) {

                  ///HEADER with the parent activity, before each list of sub-activities
                  if (position == 0) {
                    return Container(
                      //color: Theme.of(context).accentColor.withAlpha(50),
                      padding: EdgeInsets.only(left: 16),
                      height: tileHeight,
                      child: ListTile(
                        leading: Icon(
                          Icons.check,
                          size: tileHeight,
                          color: Theme.of(context).primaryColor,
                        ),
                        title: Align(
                          alignment: Alignment(-1.2, -1.2),
                          child: Text(
                            key,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Theme.of(context).primaryColor),
                          ),
                        ),
                      ),
                    );
                  }
                  position -= 1;

                  ///SUB-ACTIVITY item
                  return Container(
                    alignment: Alignment.centerLeft,
                    //padding: EdgeInsets.only(left: 32),
                    height: tileHeight,
                    //color: Colors.blue,
                    child: ListTile(
                      //contentPadding: EdgeInsets.symmetric(horizontal: 50),
                      leading: Icon(
                        Icons.check,
                        size: tileHeight,
                        color: Colors.transparent,
                      ),
                      title: FutureBuilder(
                        future:
                            _workerDetailsBloc.findSubActivityByIdOnFirestore(
                                key, activities[position]),
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.hasData)
                            return Text(
                              "- ${snapshot.data}",
                              textAlign: TextAlign.start,
                            );
                          else
                            return Text("Atividade não encontrada...");
                        },
                      ),
                    ),
                  );
                },
              ),
            );
          } else {
            //print("${Values.PRINT_DEVBOX_PREFIX} length <= 0");
          }
        } else {
          //print("${Values.PRINT_DEVBOX_PREFIX} activities = null");
        }

        ///The parent activities with no list of sub-activities
        return Container(
          padding: EdgeInsets.only(left: 16),
          //margin: EdgeInsets.symmetric(horizontal: 16),
          child: ListTile(
            leading: Icon(
              Icons.check,
              size: tileHeight,
              color: Theme.of(context).primaryColor,
            ),
            title: Container(
              alignment: Alignment(-1.2, 0),
              child: Text(
                key,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).primaryColor),
              ),
            ),
          ),
        );
      }, childCount: _worker.activities.keys.length));
    } else {
      ///When there is no info about the worker's activities
      return SliverToBoxAdapter(
          child: Container(
        padding: EdgeInsets.symmetric(horizontal: 32),
        child: Text("Sem informações"),
      ));
    }
  }
}
