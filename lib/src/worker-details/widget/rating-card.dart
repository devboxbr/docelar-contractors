import 'package:docelar/src/shared/model/rating.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:flutter/material.dart';

class RatingCard extends StatelessWidget {
  final Rating rating;
  final int index;
  static const double CARD_HEIGHT = 100;
  static const double EXTERNAL_MARGIN = 16;
  static const int COMMENT_MAX_LINES = 3;

  RatingCard({@required this.rating, this.index});

  @override
  Widget build(BuildContext context) {
    //The screen width - Content Paddings and Margins - the icon's container width - an error margin
    double commentsMaxWidth = MediaQuery.of(context).size.width -
        (EXTERNAL_MARGIN * 3) -
        CARD_HEIGHT -
        16;

    return Container(
        height: CARD_HEIGHT,
        //Spacing outside the card
        margin: const EdgeInsets.symmetric(
            horizontal: EXTERNAL_MARGIN, vertical: 4),

        //decoration with bg color and SHADOW
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.grey[400], blurRadius: 2, offset: Offset(0, 2))
          ],
          borderRadius:
              BorderRadius.all(Radius.circular(Values.CARD_BORDER_RADIUS)),
          //border: Border.all(color: Values.mediumGreyText, width: _borderSize)
        ),
        child: Row(
          children: <Widget>[
            ///LEADING
            Stack(
              children: <Widget>[
                ///Leading Left strip
                Container(
                  width: 6,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.horizontal(
                          left: Radius.circular(Values.CARD_BORDER_RADIUS)),
                      color: Colors.transparent/*Colors.grey[200]*/ /*Theme.of(context).accentColor*/),
                ),
                ///LEADING CONTENT
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.horizontal(
                          left: Radius.circular(Values.CARD_BORDER_RADIUS)),

                      ///LEADING BG
                      color: Colors
                          .transparent /*Colors.grey[200]*/ /*Theme.of(context).primaryColor*/),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Icon(
                          Icons.star,
                          size: CARD_HEIGHT - 50,
                          color: Theme.of(context).accentColor,
                        ),
                        width: CARD_HEIGHT,
                      ),

                      ///RATE
                      Text(
                        rating.rate.toString(),
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Theme.of(context).accentColor,
                            fontSize: 18),
                      )
                    ],
                  ),
                )
              ],
            ),

            Container(
              width: 1,
              height: CARD_HEIGHT - 16,
              color: Colors.grey[200],
            ),

            ///CONTENT
            Container(
              padding: EdgeInsets.symmetric(vertical: 8),
              margin: EdgeInsets.only(left: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ///TITLE
                  Text(
                    rating.title,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),

                  SizedBox(height: 4),

                  ///COMMENTS
                  Container(
                    width: commentsMaxWidth,
                    child: Text(
                      rating.comment,
                      maxLines: COMMENT_MAX_LINES,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.grey),
                    ),
                  )
                  /*rating.comment != null
                      ? rating.comment.isNotEmpty
                          ? Container(
                              width: commentsMaxWidth,
                              child: Text(
                                rating.comment,
                                maxLines: COMMENT_MAX_LINES,
                                overflow: TextOverflow.ellipsis,
                              ))
                          : Container()
                      : Container()*/
                ],
              ),
            ),
          ],
        ));
  }
}
