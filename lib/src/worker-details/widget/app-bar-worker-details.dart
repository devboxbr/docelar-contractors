import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/home/home-bloc.dart';
import 'package:docelar/src/home/search/home-search.dart';
import 'package:docelar/src/home/widget/address-quick-pick.dart';
import 'package:docelar/src/shared/widget/button-back-app-bar.dart';
import 'package:docelar/src/shared/widget/button-navigation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AppBarWorkerDetails extends StatelessWidget {
  final String _title = "DETALHES";

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      floating: false,
      pinned: false,
      snap: false,
      centerTitle: true,
      elevation: 0,
      backgroundColor: Colors.grey[50],
      leading: BackButtonAppBar(callback: () {
        Navigator.of(context).pop();
      }),
      title: Text(_title, style: TextStyle(color: Colors.black)),
      bottom: TabBar(
        //Color when the tab is selected/active
        labelColor: Theme.of(context).accentColor,
        //Colors when the tab is not selected/inactive
        unselectedLabelColor: Colors.grey,
        tabs: [
          Tab(icon: const Icon(FontAwesomeIcons.user), text: "SOBRE"),
          //Tab(icon: const Icon(FontAwesomeIcons.calendarAlt), text: "AGENDA"),
          Tab(icon: const Icon(Icons.stars), text: "AVALIAÇÕES")
        ],
      ),
    );
  }
}
