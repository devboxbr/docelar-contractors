import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/shared/model/worker.dart';
import 'package:docelar/src/worker-details/widget/app-bar-worker-details.dart';
import 'package:docelar/src/shared/widget/drawer/main-drawer-view.dart';
import 'package:docelar/src/worker-details/tabs/worker-about-tab.dart';
import 'package:docelar/src/worker-details/tabs/worker-ratings-tab.dart';
import 'package:docelar/src/worker-details/tabs/worker-schedule-tab.dart';
import 'package:docelar/src/worker-details/worker-details-bloc.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class WorkerDetailsView extends StatelessWidget {
  final String _workerId;
  final Worker _worker;
  final WorkerDetailsBloc _workerDetailsBloc =
      BlocProvider.getBloc<WorkerDetailsBloc>();

  WorkerDetailsView(this._workerId, this._worker);

  ///Tabs to be shown in the worker details screen
  List<Widget> _tabs() {
    return [
      //Tab with the worker's info
      WorkerAboutTab(_worker, _workerDetailsBloc),
      //TAb with the worker's schedule of appointments
      //WorkerScheduleTab(_worker, _workerDetailsBloc),
      //Tab with the worker's ratings
      WorkerRatingsTab(_worker, _workerDetailsBloc),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
          drawer: MainDrawer(),
          floatingActionButton: FloatingActionButton(
              child: Center(
                  child: Icon(FontAwesomeIcons.calendarPlus,
                      color: Colors.grey[800])),
              onPressed: () {

              }),
          body: DefaultTabController(
            length: _tabs().length,
            child: NestedScrollView(
              headerSliverBuilder: (context, innerBoxIsScrolled) {
                return <Widget>[
                  SliverOverlapAbsorber(
                    handle: NestedScrollView.sliverOverlapAbsorberHandleFor(
                        context),
                    child: AppBarWorkerDetails(),
                  ),
                ];
              },
              body: TabBarView(
                  physics: NeverScrollableScrollPhysics(), children: _tabs()),
            ),
          )),
    );
  }
}
