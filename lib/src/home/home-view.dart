import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/home/search/home-search.dart';
import 'package:docelar/src/home/widget/address-quick-pick.dart';
import 'package:docelar/src/home/home-bloc.dart';
import 'package:docelar/src/home/widget/workers-list-card-tile.dart';
import 'package:docelar/src/shared/helpers/custom-icons.dart';
import 'package:docelar/src/shared/helpers/custom-navigation.dart';
import 'package:docelar/src/shared/services/firebase-service.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/app-bar-main.dart';
import 'package:docelar/src/shared/widget/card-list-categories.dart';
import 'package:docelar/src/shared/widget/card-small-square.dart';
import 'package:docelar/src/shared/widget/drawer/main-drawer-view.dart';
import 'package:docelar/src/shared/widget/button-navigation.dart';
import 'package:docelar/src/shared/widget/section-text.dart';
import 'package:docelar/src/signin/signin-view.dart';
import 'package:flutter/material.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final HomeBloc homeBloc = BlocProvider.getBloc<HomeBloc>();
  final AppBloc appBloc = BlocProvider.getBloc<AppBloc>();
  String uid;

  @override
  void initState() {
    super.initState();
    _checkLoggedIn(context);

    //print("${Values.PRINT_DEVBOX_PREFIX}Iniciou HomeView()");
    homeBloc.findUserAddress();
  }

  void _checkLoggedIn(BuildContext context) async {
    await FirebaseService.isUserLoggedIn().then((bool isUserLoggedIn) async {
      if (!isUserLoggedIn) {
        CustomNavigation.loadSignInScreen(context);
      } else {
        await FirebaseService.getContractorInFirestore().then((contractor) {
          appBloc.inContractorIn.add(contractor);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    //SystemChrome.restoreSystemUIOverlays();

    return SafeArea(
      top: true,
      child: Scaffold(
        drawer: MainDrawer(),
        body: NestedScrollView(
          headerSliverBuilder: (context, innerBoxIsScrolled) {
            return <Widget>[
              MainAppBar(
                appBloc: appBloc,
                isShowSearch: true,
                child: AddressQuickPick(homeBloc),
              ),
              SliverPadding(
                padding: const EdgeInsets.all(0),
                sliver: SliverList(
                  delegate: SliverChildListDelegate([
                    SizedBox(height: 16),

                    ///1st row - Section title
                    SectionText("Acesso rápido - Categorias"),

                    SizedBox(height: 8),

                    ///2nd row - Categories Card List
                    CardListCategories(),

                    SizedBox(height: 16),

                    ///3rd row - Worker Highlights
                    SectionText("Destaques próximos"),

                    SizedBox(height: 8),
                  ]),
                ),
              )
            ];
          },
          body: _buildWorkersCards(),
        ),
      ),
    );
  }

  Widget _buildWorkersCards() {
    return StreamBuilder(
      stream: FirebaseService.firestore
          .collection(FirebaseService.WORKERS_COLLECTION)
          .orderBy("rating")
          .startAt([null]).endAt([5]).snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            //Shows a circular progress indicator when there is no data loaded
            return Center(
              child: Container(
                  height: 48, width: 48, child: CircularProgressIndicator()),
            );
          default:
            return ListView(
              children: snapshot.data.documents.reversed.map((doc) {
                return GestureDetector(
                  child: WorkerListCardTile(
                    workerId: doc['id'],
                    workerPictureUrl: doc['pictureUrl'],
                    workerName: doc['name'],
                    workerRating: doc['rating'] == null
                        ? null
                        //Converting to string because if the number at Firebase is '5', it's read here as an int,
                        //if it's '5.0' there, it's read here as a double. Converting to string we get rid of the exceptions due to number types
                        : double.parse(doc['rating'].toString()),
                  ),
                  onTap: () {
                    homeBloc.loadWorkerDetailsView(context, doc['id']);
                  },
                  //TODO: Colocar para detectar clique longo e abrir uma dialog com opção de denunciar (possível irregularidade no nome etc.)
                );
              }).toList(),
            );
        }
      },
    );
  }
}
