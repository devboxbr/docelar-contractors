import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/shared/helpers/custom-navigation.dart';
import 'package:docelar/src/shared/model/contractor.dart';
import 'package:docelar/src/shared/model/worker.dart';
import 'package:docelar/src/shared/services/firebase-service.dart';
import 'package:docelar/src/shared/widget/dialogs.dart';
import 'package:docelar/src/signin/signin-view.dart';
import 'package:docelar/src/worker-details/worker_details-view.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc extends BlocBase {
  var _contractor = BehaviorSubject<Contractor>.seeded(null);

  Stream<Contractor> get outContractor => _contractor.stream;

  Sink<Contractor> get inContractor => _contractor.sink;

  void loadWorkerDetailsView(BuildContext context, String workerId) async {
    //Showing a loading dialog
    Dialogs.showLoadingDialog(context);
    //Retrieving the worker's object
    Worker worker = await FirebaseService.getWorkerInFirestore(workerId);
    //Checks if the worker was successfully retrieved from firebase
    if(worker != null) {
      //Loading the details page
      Navigator.pushReplacement(
          context, MaterialPageRoute(
          builder: (context) => WorkerDetailsView(workerId, worker)));
    } else {
      Dialogs.showDialogWithOkButton(context, "Ops!", "Desculpe, não conseguimos encontrar este perfil!");
    }
  }

  @override
  void dispose() {
    super.dispose();
    _contractor.close();
  }

  void findUserAddress() async {
    _contractor.add(await FirebaseService.getContractorInFirestore());
  }

  void loadSignInScreen(BuildContext context) {
    CustomNavigation.loadSignInScreen(context);
  }


}
