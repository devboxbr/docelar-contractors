import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/resume-worker.dart';
import 'package:flutter/material.dart';

class WorkerListCardTile extends StatelessWidget {

  //Icon properties
  final String workerPictureUrl;

  //Title properties
  final String workerName;

  //Worker rating in the system
  final double workerRating;

  final String workerId;

  //Spacing between the stars
  double starsSpacing = 4.0;

  //Constructor
  WorkerListCardTile(
      {@required this.workerPictureUrl,
      @required this.workerName,
      @required this.workerRating,
      @required this.workerId,
      this.starsSpacing});

  @override
  Widget build(BuildContext context) {
    return Container(
      //Spacing outside the card
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),

      //decoration with bg color and SHADOW
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Colors.grey[400], blurRadius: 2, offset: Offset(0, 2))
        ],
        borderRadius:
            BorderRadius.all(Radius.circular(Values.CARD_BORDER_RADIUS)),
        //border: Border.all(color: Values.mediumGreyText, width: _borderSize)
      ),
      child: WorkerResume(workerPictureUrl, workerName, workerRating)
    );
  }
}
