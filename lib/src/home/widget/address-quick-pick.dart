import 'package:docelar/src/home/home-bloc.dart';
import 'package:docelar/src/shared/helpers/custom-navigation.dart';
import 'package:docelar/src/shared/model/address.dart';
import 'package:docelar/src/shared/model/contractor.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:flutter/material.dart';

class AddressQuickPick extends StatelessWidget {
  final HomeBloc homeBloc;

  AddressQuickPick(this.homeBloc);

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "SERVIÇO PARA:",
            style: TextStyle(color: Values.mediumGreyText, fontSize: 14),
            textAlign: TextAlign.center,
          ),
          StreamBuilder(
              stream: homeBloc.outContractor,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  //String streetName = contractor.mainAddress.street;

                  Contractor contractor = snapshot.data;
                  //print("${Values.PRINT_DEVBOX_PREFIX} AddressQuickPick(): contractor $contractor");

                  //return Container(width: 10, height: 10, color: Colors.red);

                  return Center(
                    child: DropdownButton<String>(
                      iconEnabledColor: Theme.of(context).primaryColor,
                      icon: Icon(Icons.keyboard_arrow_down),
                      isDense: true,
                      value: contractor.mainAddress != null ? contractor.mainAddress.label : "Cadastrar endereço",
                      items: contractor.allAddresses != null ? contractor.allAddresses.map((Address address) {
                        //print(contractor.allAddresses);
                        return DropdownMenuItem(
                          value: address.label,
                          child: Text(address.label, textAlign: TextAlign.center),
                        );
                      }).toList() : null,
                      onChanged: (String address) {},
                    ),
                  );
                } else {
                  print("Problem getting the contractor from Firebase in AddressQuickPick() StreamBuilder.");
                  return _buildDefaultAddressDropdown(context);
                }
              }),
        ]);
  }

  Widget _buildDefaultAddressDropdown(BuildContext context) {
    const String defaultValue = "Cadastre um endereço";
    return DropdownButton<String>(
      value: defaultValue,
      items: [defaultValue].map((string) {
        return DropdownMenuItem(value: string, child: Text(string));
      }).toList(),
      onChanged: (String value) {
        CustomNavigation.loadProfileScreen(context);
      },
    );
  }
}
