import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/home/widget/workers-list-card-tile.dart';
import 'package:docelar/src/shared/model/contractor.dart';
import 'package:docelar/src/shared/services/firebase-service.dart';
import 'package:flutter/material.dart';

class HomeSearch extends SearchDelegate<String> {
  //AppBloc appBloc = BlocProvider.getBloc<AppBloc>();
  final AppBloc appBloc;

  HomeSearch({this.appBloc});

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    //If we want to return the query to the home
    //Future.delayed(Duration.zero).then((_) => close(context, query));

    return StreamBuilder(
      //LOCAL CONTRACTOR STREAM
      builder: (BuildContext context, AsyncSnapshot contractorSnapshot) {
        if (contractorSnapshot.hasData) {
          return StreamBuilder(
            //FIRESTORE STREAM
            stream: FirebaseService.firestore
                .collection(FirebaseService.WORKERS_COLLECTION)
                .orderBy("rating")
                .startAt([null])
                .endAt([5])
                .where("name == $query")
                .snapshots(),
            builder: (BuildContext context, AsyncSnapshot firestoreSnapshot) {
              if (!contractorSnapshot.hasData || !firestoreSnapshot.hasData) {
                return Center(child: CircularProgressIndicator());
              } else {
                return ListView.builder(itemBuilder: (BuildContext context, int index) {
                  print(firestoreSnapshot.data);
                  return Container(
                    color: Colors.blue,
                  );
                  //return WorkerListCardTile();
                });
              }
            },
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    return Container(
      padding: const EdgeInsets.all(16),
      child: StreamBuilder(
        stream: appBloc.outContractorLoggedIn,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data.recentSearches == null
                  ? 1
                  : (snapshot.data as Contractor).recentSearches.length,
              itemBuilder: (BuildContext context, int index) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  Contractor contractor = snapshot.data;
                  if (contractor != null) {
                    if (contractor.recentSearches != null) {
                      return Text(contractor.recentSearches[index]);
                    }
                  }
                  return Text("Sem pesquisas recentes...");
                }
              },
            );
          } else {
            return Text("Sem pesquisas recentes...");
          }
        },
      ),
    );
  }
}
