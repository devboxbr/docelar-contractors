import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/profile/profile-bloc.dart';
import 'package:docelar/src/shared/helpers/text-input-formatter.dart';
import 'package:docelar/src/shared/model/address.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/button-back-app-bar.dart';
import 'package:docelar/src/shared/widget/dialogs.dart';
import 'package:docelar/src/signup/widget/text-field-signup.dart';
import 'package:docelar/src/signup/signup-bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class EditOrCreateAddressView extends StatefulWidget {
  final Address address;
  final int indexAddress;

  EditOrCreateAddressView(this.address, this.indexAddress);

  @override
  _EditOrCreateAddressViewState createState() =>
      _EditOrCreateAddressViewState(address, indexAddress);
}

class _EditOrCreateAddressViewState extends State<EditOrCreateAddressView> {
  //If there is an editingAddress set, the form will load its address
  final Address editingAddress;
  final int indexAddress;

  bool hasEdited = false;

  _EditOrCreateAddressViewState(this.editingAddress, this.indexAddress);

  var _formAddressKey;

  final SignUpBloc signUpBloc = BlocProvider.getBloc<SignUpBloc>();
  final ProfileBloc profileBloc = BlocProvider.getBloc<ProfileBloc>();
  final AppBloc appBloc = BlocProvider.getBloc<AppBloc>();

  static final TextEditingController _labelController = TextEditingController();
  static final TextEditingController _postalCodeController =
      TextEditingController();
  static final TextEditingController _streetController =
      TextEditingController();
  static final TextEditingController _numberController =
      TextEditingController();
  static final TextEditingController _neighborhoodController =
      TextEditingController();
  static final TextEditingController _complementController =
      TextEditingController();
  static final TextEditingController _referencePointController =
      TextEditingController();
  static final TextEditingController _cityController = TextEditingController();
  static final TextEditingController _stateController = TextEditingController();

  final FocusNode focus1 = FocusNode();
  final FocusNode focus2 = FocusNode();
  final FocusNode focus3 = FocusNode();
  final FocusNode focus4 = FocusNode();
  final FocusNode focus5 = FocusNode();
  final FocusNode focus6 = FocusNode();
  final FocusNode focus7 = FocusNode();
  final FocusNode focus8 = FocusNode();
  final FocusNode focus9 = FocusNode();

  @override
  void initState() {
    super.initState();
    _formAddressKey = GlobalKey<FormState>(
        debugLabel:
            editingAddress == null ? "add-address-key" : "edit-address-key");

    _labelController.addListener(hasEditedListener);
    _postalCodeController.addListener(hasEditedListener);
    _streetController.addListener(hasEditedListener);
    _numberController.addListener(hasEditedListener);
    _neighborhoodController.addListener(hasEditedListener);
    _complementController.addListener(hasEditedListener);
    _referencePointController.addListener(hasEditedListener);
    _cityController.addListener(hasEditedListener);
    _stateController.addListener(hasEditedListener);
  }

  void hasEditedListener() {
    if (editingAddress != null) {
      if (_labelController.text.length > 0 ||
          _postalCodeController.text.length > 0 ||
          _streetController.text.length > 0 ||
          _numberController.text.length > 0 ||
          _neighborhoodController.text.length > 0 ||
          _complementController.text.length > 0 ||
          _referencePointController.text.length > 0 ||
          _cityController.text.length > 0 ||
          _stateController.text.length > 0) {
        hasEdited = true;
      } else {
        hasEdited = false;
      }
    }
  }

  void focusNextField(
      BuildContext context, FocusNode currentNode, FocusNode nextNode) {
    currentNode.unfocus();
    FocusScope.of(context).requestFocus(nextNode);
  }

  void submitAddAddressForm(BuildContext context) {
    // Validate returns true if the form is valid, otherwise false.
    if (_formAddressKey.currentState.validate()) {
      profileBloc.saveNewUserAddress(
          context,
          appBloc,
          null,
          _labelController.text,
          _postalCodeController.text.replaceAll(new RegExp("[-,.\s]"), ""),
          _streetController.text,
          _numberController.text,
          _neighborhoodController.text,
          _complementController.text,
          _referencePointController.text,
          _cityController.text,
          _stateController.text);

      clearFieldsData();
    }
  }

  void submitEditAddressForm(BuildContext context, int indexAddress) {
    // Validate returns true if the form is valid, otherwise false.
    if (_formAddressKey.currentState.validate()) {
      profileBloc.updateUserAddress(
          context,
          appBloc,
          indexAddress,
          _labelController.text.length > 0
              ? _labelController.text
              : editingAddress.label,
          _postalCodeController.text.length > 0
              ? _postalCodeController.text.replaceAll(new RegExp(r"[-,.\s]"), "")
              : editingAddress.postalCode,
          _streetController.text.length > 0
              ? _streetController.text
              : editingAddress.street,
          _numberController.text.length > 0
              ? _numberController.text
              : editingAddress.number,
          _neighborhoodController.text.length > 0
              ? _neighborhoodController.text
              : editingAddress.neighborhood,
          _complementController.text.length > 0
              ? _complementController.text
              : editingAddress.complementInfo,
          _referencePointController.text.length > 0
              ? _referencePointController.text
              : editingAddress.referencePoint,
          _cityController.text.length > 0
              ? _cityController.text
              : editingAddress.city,
          _stateController.text.length > 0
              ? _stateController.text
              : editingAddress.state);

      clearFieldsData();
    }
  }

  void clearFieldsData(){
    _labelController.clear();
    _postalCodeController.clear();
    _streetController.clear();
    _numberController.clear();
    _neighborhoodController.clear();
    _complementController.clear();
    _referencePointController.clear();
    _cityController.clear();
    _stateController.clear();
  }

  @override
  void dispose() {
    super.dispose();
    focus1.dispose();
    focus2.dispose();
    focus3.dispose();
    focus4.dispose();
    focus5.dispose();
    focus6.dispose();
    focus7.dispose();
    focus8.dispose();
    focus9.dispose();
  }

  @override
  Widget build(BuildContext context) {
//    SystemChrome.setSystemUIOverlayStyle(
//        SystemUiOverlayStyle(statusBarColor: Colors.transparent));

    return SafeArea(
      top: true,
      child: Stack(
        children: <Widget>[
          Image.asset(
            "resources/images/bg_recife_antigo.png",
            fit: BoxFit.cover,
            width: double.infinity,
            height: double.infinity,
            alignment: Alignment.center,
          ),
          Scaffold(
            key: profileBloc.scaffoldKey,
            //backgroundColor: Colors.grey,
            resizeToAvoidBottomPadding: true,
            backgroundColor: Colors.transparent,
            body: Builder(
              builder: (BuildContext context) {
                return NestedScrollView(
                  headerSliverBuilder: (context, bool innetBoxScrolled) {
                    return <Widget>[
                      SliverAppBar(
                        elevation: 0,
                        centerTitle: true,
                        backgroundColor: Colors.transparent,
                        leading: BackButtonAppBar(
                          callback: () {
                            //Asks if the user has made some edition
                            if(hasEdited) {
                              //Asks for user confirmation before closing the screen
                              Dialogs.showConfirmationYesNoDialog(
                                  context: context,
                                  title: "Alterações",
                                  contentText: "Deseja realmente descartar a edição?",
                                  yesCallback: () {
                                    clearFieldsData();
                                    Navigator.of(context).pop();
                                  },
                                  noCallback: () {});
                            } else {
                              clearFieldsData();
                              Navigator.of(context).pop();
                            }
                          },
                          color: Colors.white,
                        ),
                      )
                    ];
                  },
                  body: Stack(
                    children: <Widget>[
                      //BACKGROUND

                      Form(
                        key: _formAddressKey,
                        child: ListView(
                            padding: EdgeInsets.symmetric(horizontal: 16),
                            shrinkWrap: true,
                            //crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              const SizedBox(height: 16),

                              Container(
                                //alignment: Alignment.center,
                                width: 100,
                                child: Text(editingAddress != null ? "EDITAR ENDEREÇO" : "CADASTRAR ENDEREÇO",
                                    //softWrap: true,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: TextStyle(
                                        color: Theme.of(context).accentColor,
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center),
                              ),

                              const SizedBox(height: 48),

                              //LABEL
                              TextFieldSignUp(
                                hintText: editingAddress != null
                                    ? "Novo nome deste lugar"
                                    : "Casa, trabalho, academia...",
                                labelText: editingAddress?.label,
                                validator: editingAddress == null
                                    ? (value) {
                                  return signUpBloc.validateLabelField(value);
                                }
                                    : null,
                                fieldController: _labelController,
                                obscureText: false,
                                textCapitalization: TextCapitalization.words,
                                textInputType: TextInputType.text,
                                signUpBloc: signUpBloc,
                                focusNode: focus1,
                                textInputAction: TextInputAction.next,
                                onSumitted: (value) {
                                  focusNextField(context, focus1, focus2);
                                },
                              ),

                              SizedBox(height: 16),

                              //POSTAL CODE
                              TextFieldSignUp(
                                hintText:
                                editingAddress != null ? "Novo CEP" : "CEP",
                                labelText: editingAddress?.getFormattedPostalCode(),
                                validator: editingAddress == null
                                    ? (value) {
                                  return signUpBloc
                                      .validatePostalCodeField(value.replaceAll(new RegExp("[-,.\s]"), ""));
                                }
                                    : null,
                                fieldController: _postalCodeController,
                                obscureText: false,
                                textCapitalization: TextCapitalization.none,
                                textInputType: TextInputType.number,
                                signUpBloc: signUpBloc,
                                focusNode: focus2,
                                textInputAction: TextInputAction.next,
                                onSumitted: (value) {
                                  focusNextField(context, focus2, focus3);
                                },
                                inputFormatters: [
                                  CustomTextInputFormatters.cepFormatter
                                ],
                              ),

                              SizedBox(height: 16),

                              //STREET
                              TextFieldSignUp(
                                  hintText:
                                  editingAddress != null ? "Nova rua" : "Rua",
                                  labelText: editingAddress?.street,
                                  validator: editingAddress == null
                                      ? (value) {
                                    return signUpBloc
                                        .validateStreetField(value);
                                  }
                                      : null,
                                  fieldController: _streetController,
                                  obscureText: false,
                                  textCapitalization: TextCapitalization.sentences,
                                  textInputType: TextInputType.text,
                                  signUpBloc: signUpBloc,
                                  focusNode: focus3,
                                  textInputAction: TextInputAction.next,
                                  onSumitted: (value) {
                                    focusNextField(context, focus3, focus4);
                                  }),

                              SizedBox(height: 16),

                              //NUMBER
                              TextFieldSignUp(
                                  hintText: editingAddress != null
                                      ? "Novo número"
                                      : "Número",
                                  labelText: editingAddress?.number,
                                  validator: editingAddress == null
                                      ? (value) {
                                    return signUpBloc
                                        .validateAddressNumberField(value);
                                  }
                                      : null,
                                  fieldController: _numberController,
                                  obscureText: false,
                                  textCapitalization: TextCapitalization.none,
                                  textInputType: TextInputType.text,
                                  signUpBloc: signUpBloc,
                                  focusNode: focus4,
                                  textInputAction: TextInputAction.next,
                                  onSumitted: (value) {
                                    focusNextField(context, focus4, focus5);
                                  }),

                              SizedBox(height: 16),

                              //NEIGHBORHOOD
                              TextFieldSignUp(
                                  hintText: editingAddress != null
                                      ? "Novo bairro"
                                      : "Bairro",
                                  labelText: editingAddress?.neighborhood,
                                  validator: editingAddress == null
                                      ? (value) {
                                    return signUpBloc
                                        .validateNeighborhoodField(value);
                                  }
                                      : null,
                                  fieldController: _neighborhoodController,
                                  obscureText: false,
                                  textCapitalization: TextCapitalization.words,
                                  textInputType: TextInputType.text,
                                  signUpBloc: signUpBloc,
                                  focusNode: focus5,
                                  textInputAction: TextInputAction.next,
                                  onSumitted: (value) {
                                    focusNextField(context, focus5, focus6);
                                  }),

                              SizedBox(height: 16),

                              //COMPLEMENT
                              TextFieldSignUp(
                                  hintText: editingAddress != null
                                      ? "Novo complemento"
                                      : "Complemento (Edf., apto etc.)",
                                  labelText: editingAddress?.complementInfo,
                                  validator: editingAddress == null
                                      ? (value) {
                                    return signUpBloc
                                        .validateComplementField(value);
                                  }
                                      : null,
                                  fieldController: _complementController,
                                  obscureText: false,
                                  textCapitalization: TextCapitalization.sentences,
                                  textInputType: TextInputType.text,
                                  signUpBloc: signUpBloc,
                                  focusNode: focus6,
                                  textInputAction: TextInputAction.next,
                                  onSumitted: (value) {
                                    focusNextField(context, focus6, focus7);
                                  }),

                              SizedBox(height: 16),

                              //REFERENCE POINT
                              TextFieldSignUp(
                                  hintText: editingAddress != null
                                      ? "Novo ponto de referência"
                                      : "Ponto de referência",
                                  labelText: editingAddress?.referencePoint,
                                  validator: editingAddress == null
                                      ? (value) {
                                    return signUpBloc
                                        .validateReferencePointField(value);
                                  }
                                      : null,
                                  fieldController: _referencePointController,
                                  obscureText: false,
                                  textCapitalization: TextCapitalization.sentences,
                                  textInputType: TextInputType.text,
                                  signUpBloc: signUpBloc,
                                  focusNode: focus7,
                                  textInputAction: TextInputAction.next,
                                  onSumitted: (value) {
                                    focusNextField(context, focus7, focus8);
                                  }),

                              SizedBox(height: 16),

                              //STATE

                              TextFieldSignUp(
                                hintText: editingAddress != null
                                    ? "Novo Estado"
                                    : "Estado",
                                labelText: editingAddress?.state,
                                validator: editingAddress == null
                                    ? (value) {
                                  return signUpBloc.validateStateField(value);
                                }
                                    : null,
                                fieldController: _stateController,
                                obscureText: false,
                                textCapitalization: TextCapitalization.words,
                                textInputType: TextInputType.text,
                                signUpBloc: signUpBloc,
                                focusNode: focus8,
                                textInputAction: TextInputAction.next,
                                onSumitted: (value) {
                                  focusNextField(context, focus8, focus9);
                                },
                              ),

                              SizedBox(height: 16),

                              //CITY
                              TextFieldSignUp(
                                hintText: editingAddress != null
                                    ? "Nova cidade"
                                    : "Cidade",
                                labelText: editingAddress?.city,
                                validator: editingAddress == null
                                    ? (value) {
                                  return signUpBloc.validateCityField(value);
                                }
                                    : null,
                                fieldController: _cityController,
                                obscureText: false,
                                textCapitalization: TextCapitalization.words,
                                textInputType: TextInputType.text,
                                signUpBloc: signUpBloc,
                                focusNode: focus9,
                                textInputAction: TextInputAction.done,
                                onSumitted: (value) {
                                  submitAddAddressForm(context);
                                },
                              ),

                              SizedBox(height: 48),

                              //BUTTON SIGN-UP
                              Container(
                                height: Values.BUTTON_HEIGHT,
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(35)),
                                  onPressed: () {
                                    if (editingAddress != null) {
                                      submitEditAddressForm(context, indexAddress);
                                    } else {
                                      submitAddAddressForm(context);
                                    }
                                  },
                                  color: Theme.of(context).accentColor,
                                  child: Text(
                                    "SALVAR",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),

                              SizedBox(height: 48),
                            ]),
                      )
                    ],
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
