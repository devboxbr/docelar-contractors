import 'package:docelar/src/profile/tabs/profile-about-tab.dart';
import 'package:docelar/src/profile/tabs/profile-address-tab.dart';
import 'package:docelar/src/profile/widget/app-bar-profile.dart';
import 'package:flutter/material.dart';

class ProfileView extends StatefulWidget {
  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {

  ///Tabs to be shown in the worker details screen
  List<Widget> _tabs() {
    return [
      //Tab with the user info
      ProfileAboutTab(),
      //Tab with the user addresses
      ProfileAddressTab(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
          body: DefaultTabController(
            length: _tabs().length,
            child: NestedScrollView(
              headerSliverBuilder: (context, innerBoxIsScrolled) {
                return <Widget>[
                  SliverOverlapAbsorber(
                    handle: NestedScrollView.sliverOverlapAbsorberHandleFor(
                        context),
                    child: AppBarProfile(),
                  ),
                ];
              },
              body: TabBarView(
                  physics: NeverScrollableScrollPhysics(), children: _tabs()),
            ),
          )),
    );
  }
}
