import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/home/home-view.dart';
import 'package:docelar/src/profile/edit-address-view.dart';
import 'package:docelar/src/shared/helpers/custom-navigation.dart';
import 'package:docelar/src/shared/model/address.dart';
import 'package:docelar/src/shared/model/contractor.dart';
import 'package:docelar/src/shared/model/user.dart';
import 'package:docelar/src/shared/services/firebase-service.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/dialogs.dart';
import 'package:docelar/src/shared/widget/snackbars.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter/material.dart';

class ProfileBloc extends BlocBase {
  var _email = BehaviorSubject<String>.seeded(null);
  var _phone = BehaviorSubject<String>.seeded(null);
  var _hasMadeChanges = BehaviorSubject<bool>.seeded(false);

  var scaffoldKey = GlobalKey<ScaffoldState>();

  Stream<String> get outEmail => _email.stream;

  Sink<String> get inEmail => _email.sink;

  Stream<String> get outPhone => _phone.stream;

  Sink<String> get inPhone => _phone.sink;

  Stream<bool> get outHasMadeChanges => _hasMadeChanges.stream;

  Sink<bool> get inHasMadeChanges => _hasMadeChanges.sink;

  @override
  void dispose() {
    super.dispose();
    _email.close();
    _phone.close();
    _hasMadeChanges.close();
  }
  ///ERROR_INVALID_EMAIL
  ///ERROR_EMAIL_ALREADY_IN_USE
  ///ERROR_REQUIRES_RECENT_LOGIN
  ///Saving user personal data locally and on the cloud database
  Future<Null> updateContractorPersonalData(BuildContext context, Contractor contractor, AppBloc appBloc,
      String email, String phone) async {
    //print("${Values.PRINT_DEVBOX_PREFIX} Email antes: ${contractor.email}");
    //print("${Values.PRINT_DEVBOX_PREFIX} Email enviado: $email");
    Dialogs.showLoadingDialog(context);
    //Getting the data the user my have informed and setting it to his local object
    //Making a copy of the contractor passed as parameter
    Contractor contractorUpdated = Contractor.fromJson(contractor.toJson());
    bool hasChangedEmail = false;
    if (email != null) {
      if (email.length > 0) {
        //print("${Values.PRINT_DEVBOX_PREFIX} 1");
        await FirebaseService.updateUserEmailOnAuth(email).catchError((error) {
          //Popping the loading dialog
          Navigator.of(context).pop();
          //print("${Values.PRINT_DEVBOX_PREFIX} 2");
          //print("${Values.PRINT_DEVBOX_PREFIX} ${error.toString()}");
          //Handling the possible errors
          if (error.toString().contains("ERROR_INVALID_EMAIL")) {
            //print("${Values.PRINT_DEVBOX_PREFIX} 3");
            SnackBars.showErrorSnackBar("E-mail inválido!", context);
          } else if (error
              .toString()
              .contains("ERROR_EMAIL_ALREADY_IN_USE")) {
            //print("${Values.PRINT_DEVBOX_PREFIX} 4");
            SnackBars.showErrorSnackBar(
                "E-mail já cadastrado, tente outro.", context);
          } else if (error
              .toString()
              .contains("ERROR_REQUIRES_RECENT_LOGIN")) {
            //print("${Values.PRINT_DEVBOX_PREFIX} 41");
            SnackBars.showMessageSnackBarWithAction("Você precisa entrar novamente.", context, "ENTRAR", (){
              FirebaseService.signOut();
              CustomNavigation.loadSignInScreen(context);
            });
          } else {
            //print("${Values.PRINT_DEVBOX_PREFIX} 5");
            SnackBars.showErrorSnackBar("Falha ao alterar e-mail!", context);
          }
          //If catches an error, stops the execution of the method
          return;
        }). then((result) {
          if(result != null) {
            if (result) {
              //print("${Values.PRINT_DEVBOX_PREFIX} 6");
              contractorUpdated.email = email;
              hasChangedEmail = true;
              return;
            }
          }
        });
      }
    }
    if (phone != null) {
      if (phone.length > 0) {
        print("${Values.PRINT_DEVBOX_PREFIX} 61");
        contractorUpdated.phone = phone;
      }
    }
    /*print("${Values.PRINT_DEVBOX_PREFIX} contractor.email depois: ${contractor.email}");
    print("${Values.PRINT_DEVBOX_PREFIX} contractorUpdated.email depois: ${contractorUpdated.email}");
    print("${Values.PRINT_DEVBOX_PREFIX} 666");*/
    //print("${Values.PRINT_DEVBOX_PREFIX} ProfileBloc(): email: $email");
    //print("${Values.PRINT_DEVBOX_PREFIX} ProfileBloc(): phone: $phone");

    //Only saves the updatedUser to cloud database if the user has changed since
    if(contractorUpdated.email != contractor.email || contractorUpdated.phone != contractor.phone) {
      //print("${Values.PRINT_DEVBOX_PREFIX} 7");
      //Saving his local object to the cloud database
      await FirebaseService.updateUserOnFirestore(contractorUpdated)
          .then((bool result) async {
        if (result) {
          //print("${Values.PRINT_DEVBOX_PREFIX} 8");
          //Clearing data
          inEmail.add(null);
          inPhone.add(null);
          _hasMadeChanges.value = false;
          //Updates the local user object
          appBloc.inContractorIn.add(contractorUpdated);
          /*print("${Values.PRINT_DEVBOX_PREFIX} 888: contractor.email = ${contractor.email}");
          print("${Values.PRINT_DEVBOX_PREFIX} 888: contractor.pictureUrl = ${contractor.pictureUrl}");
          print("${Values.PRINT_DEVBOX_PREFIX} 888: contractorUpdated.email = ${contractorUpdated.email}");
          print("${Values.PRINT_DEVBOX_PREFIX} 888: contractorUpdated.pictureUrl = ${contractorUpdated.pictureUrl}");*/
          //Popping the loading dialog
          Navigator.of(context).pop();
          //User feedback
          SnackBars.showMessageSnackBar("Alterações salvas.", context);
        } else {
          if(hasChangedEmail) {
            await FirebaseService.updateUserEmailOnAuth(contractor.email);
          }
          //print("${Values.PRINT_DEVBOX_PREFIX} 9");
          //print("${Values.PRINT_DEVBOX_PREFIX} Erro no ProfileBloc.saveUserData(): result = false ");
          //User feedback
          SnackBars.showErrorSnackBar(
              "Não foi possível salvar suas alterações.", context);
        }
      }).catchError((error) async {
        //print("${Values.PRINT_DEVBOX_PREFIX} 10");
        //If it catches an error, undo the change of email and set it to be the old one
        if(hasChangedEmail) {
          await FirebaseService.updateUserEmailOnAuth(contractor.email);
        }

        //Popping the loading dialog
        Navigator.of(context).pop();
        //print("${Values.PRINT_DEVBOX_PREFIX} Erro no ProfileBloc.saveUserData(): ${error.toString()}");
        //User feedback
        SnackBars.showErrorSnackBar(
            "Não foi possível salvar suas alterações.", context);
      });
    }
  }

  ///Updates user address locally and on the cloud database
  void deleteUserAddress(
      BuildContext context, User user, AppBloc appBloc, int indexAddress) {
    Dialogs.showLoadingDialog(context);
    //Showing confirmation dialog
    Dialogs.showConfirmationYesNoDialog(
      context: context,
      title: "Alterações",
      contentText: "Realmente deseja apagar este endereço do seu cadastro?",
      yesCallback: () async {
        //Creating a copy of the user that will be passed to Firebase
        User userUpdated = user;
        //Saving a reference to the address the user wants to erase
        Address erasedAddress = userUpdated.allAddresses[indexAddress];
        //Removing the erased address from the user list of address
        userUpdated.allAddresses.removeAt(indexAddress);
        //If the erased address is set to be the main address
        if (userUpdated.mainAddress == erasedAddress)
          //Sets the main address automatically as the first address found in his list of addresses
          userUpdated.mainAddress = userUpdated.allAddresses.first;

        //userUpdated.mainAddress = userUpdated.allAddresses.first;

        //Saving user's local object to the cloud database
        await FirebaseService.updateUserOnFirestore(userUpdated).then((result) {
          //Popping the loading dialog
          Navigator.of(context).pop();
          if (result) {
            //Updates the local user object
            appBloc.inContractorIn.add(userUpdated);
            //User feedback
            SnackBars.showMessageSnackBar("Alterações salvas.", context);
            //CustomNavigation.loadHomeScreen(context);
            //Navigator.of(context).pop();
          } else {
            //User feedback
            SnackBars.showErrorSnackBar(
                "Não foi possível salvar suas alterações.", context);
          }
        }).catchError((error) {
          //Popping the loading dialog
          Navigator.of(context).pop();
          print(
              "${Values.PRINT_DEVBOX_PREFIX} Erro no ProfileBloc.updateUserAddress(): ${error.toString()}");
          //User feedback
          SnackBars.showErrorSnackBar(
              "Não foi possível salvar suas alterações.", context);
        });
      },
      noCallback: () {},
    );
  }

  ///Saver the address to the user's data in firestore
  void saveNewUserAddress(
      BuildContext context,
      AppBloc appBloc,
      int indexAddress,
      String label,
      String postalCode,
      String street,
      String number,
      String neighborhood,
      String complementInfo,
      String referencePoint,
      String city,
      String state) async {
    //Show the loading-dialog
    Dialogs.showLoadingDialog(context);

    //Creating the address object
    Address editedAddress = Address(label, street, number, neighborhood,
        postalCode, city, state, complementInfo, referencePoint);

    //TODO: localizar location point
    editedAddress.locationCoordinates = null;

    //Calling Firebase service to save the address
    await FirebaseService.saveContractorAddressToFirestore(
            appBloc.getUserLoggedIn().id, editedAddress)
        .then((User user) async {
      //print("${Values.PRINT_DEVBOX_PREFIX}saved address? $user");
      //Closing the loading-dialog
      Navigator.of(context).pop();
      if (user != null) {
        appBloc.inContractorIn.add(user);
        //CustomNavigation.loadHomeScreen(context);
        //First pop to close the loading dialog
        SnackBars.showMessageSnackBar("Endereço cadastrado!", context, scaffoldKey: scaffoldKey);
        //Second pop to close the edit address screen
        Navigator.of(context).pop();
      } else {
        //Navigator.of(context).pop();
        //Feedback to the user
        SnackBars.showErrorSnackBar("Ops! Algo deu errado!", context, scaffoldKey: scaffoldKey);
      }
    });
  }

  ///Saver the address to the user's data in firestore
  void updateUserAddress(
      BuildContext context,
      AppBloc appBloc,
      int indexAddress,
      String label,
      String postalCode,
      String street,
      String number,
      String neighborhood,
      String complementInfo,
      String referencePoint,
      String city,
      String state) async {
    //Show the loading-dialog
    Dialogs.showLoadingDialog(context);

    //Creating the address object
    Address editedAddress = Address(label, street, number, neighborhood,
        postalCode, city, state, complementInfo, referencePoint);

    print(
        "${Values.PRINT_DEVBOX_PREFIX} ProfileBloc(): postal code: ${postalCode}");

    User user = appBloc.getUserLoggedIn();

    if (user.mainAddress == user.allAddresses[indexAddress]) {
      user.mainAddress = editedAddress;
    }

    user.allAddresses[indexAddress] = editedAddress;

    //TODO: localizar location point
    editedAddress.locationCoordinates = null;

    await FirebaseService.updateUserOnFirestore(user).then((result) async {
      if (result) {
        //Updating local user's object
        appBloc.inContractorIn.add(user);
        //Closing the loading-dialog
        Navigator.of(context).pop();
        //Feedback to the user
        //SnackBars.showMessageSnackBar("Alterações salvas!", context);
        await Future.delayed(Duration(seconds: 1));
        //Closing the edit screen
        Navigator.of(context).pop();
      } else {
        //Closing the loading-dialog
        Navigator.of(context).pop();
        //Feedback to the user
        //SnackBars.showErrorSnackBar("Ops! Algo deu errado!", context);
        await Future.delayed(Duration(seconds: 1));
        //Closing the edit screen
        Navigator.of(context).pop();
      }
    });
  }

  bool hasChangesToSave() {
    return _hasMadeChanges.value;
  }

  ///Sets the [address] passed as the [user] main address and updates the cloud database
  void setMainAddress(User user, Address address, AppBloc appBloc) {
    user.mainAddress = address;
    appBloc.inContractorIn.add(user);
    FirebaseService.updateUserOnFirestore(user);
  }

  void loadHomePage(BuildContext context) {
    CustomNavigation.loadHomeScreen(context);
  }

  void loadEditAddressView(BuildContext context, Address address, int index) {
    CustomNavigation.loadEditAddressScreen(context, address, index);
  }
}
