import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/signup/signup-bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFieldEditProfile extends StatelessWidget {
  final String hintText;
  final bool enabled;
  final Icon icon;
  final String labelText;
  final TextEditingController fieldController;
  final TextCapitalization textCapitalization;
  final TextInputType textInputType;
  final FocusNode focusNode;
  final TextInputAction textInputAction;
  final Function onSubmitted;
  final Function onChanged;
  final List<TextInputFormatter> inputFormatters;
  final hideTrailIcon;

  TextFieldEditProfile(
      {@required this.hintText,
      this.textInputType,
      this.onSubmitted,
      this.textCapitalization,
      this.fieldController,
      this.textInputAction,
      this.focusNode,
      this.inputFormatters,
      this.enabled,
      this.icon,
      this.labelText,
      this.onChanged,
      this.hideTrailIcon});

  @override
  Widget build(BuildContext context) {
    //print("######### botao $labelText hideTrailIcon = $hideTrailIcon");
    return TextField(
      enabled: enabled,
      inputFormatters: inputFormatters,
      onChanged: onChanged,
      onSubmitted: onSubmitted,
      textInputAction: textInputAction,
      style: const TextStyle(color: Colors.black),
      keyboardType: textInputType,
      focusNode: focusNode,
      decoration: InputDecoration(
          labelText: labelText,
          suffixIcon: hideTrailIcon
              ? null
              : (enabled
                  ? IconButton(
                      icon: Icon(Icons.edit),
                      onPressed: () {
                        if (focusNode != null)
                          FocusScope.of(context).requestFocus(focusNode);
                      })
                  : null),
          icon: icon,
          labelStyle: TextStyle(color: Colors.grey),
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
          filled: true,
          fillColor: Color.fromARGB(80, 255, 255, 255),
          disabledBorder: const OutlineInputBorder(
            //TODO: Transformar isso num método
              borderSide: const BorderSide(width: 1, color: Colors.transparent),
              borderRadius: const BorderRadius.all(
                  const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS))),
          border: const OutlineInputBorder(
              borderSide: const BorderSide(width: 1, color: Colors.transparent),
              borderRadius: const BorderRadius.all(
                  const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS))),
          enabledBorder: const OutlineInputBorder(
            borderSide: const BorderSide(width: 1, color: Colors.transparent),
            borderRadius: const BorderRadius.all(
                const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS)),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Theme.of(context).accentColor, width: 1),
            borderRadius: const BorderRadius.all(
                const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS)),
          ),
          errorBorder: const OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red, width: 1),
            borderRadius: const BorderRadius.all(
                const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS)),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red, width: 1),
            borderRadius: const BorderRadius.all(
                const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS)),
          ),
          errorStyle: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
              color: Colors.red[200]),
          hintText: hintText,
          hintStyle: const TextStyle(color: Colors.grey)),
      textCapitalization: textCapitalization,
      //onSaved: ,
      controller: fieldController,
    );
  }
}
