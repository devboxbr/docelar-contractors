import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/profile/profile-bloc.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/button-back-app-bar.dart';
import 'package:docelar/src/shared/widget/dialogs.dart';
import 'package:docelar/src/shared/widget/user-picture-avatar.dart';
import 'package:flutter/material.dart';

class AppBarProfile extends StatelessWidget {
  final String _title = "PERFIL";

  ProfileBloc profileBloc = BlocProvider.getBloc<ProfileBloc>();
  AppBloc appBloc = BlocProvider.getBloc<AppBloc>();

  static const double expandedHeight = 250;
  static const double pictureSize = 1.5;
  static const double _avatarHeight = expandedHeight / pictureSize;

  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;
    double horizontalPadding = (_screenWidth - _avatarHeight) / 2;
    double verticalPadding = (expandedHeight - _avatarHeight) / 2;

    return SliverAppBar(
      //floating: false,
      pinned: true,
      //snap: false,
      centerTitle: true,
      elevation: 0,
      backgroundColor: Colors.grey[50],
      leading: BackButtonAppBar(callback: () {
        //If there are no changes pending to be saved
        if (!profileBloc.hasChangesToSave()) {
          //Pops the screen
          Navigator.of(context).pop();
        } else {
          //If there are changes to be saved, asks the user if he wants to discard it
          Dialogs.showConfirmationYesNoDialog(
            context: context,
            title: "Alterações",
            contentText: "Deseja descartar as alterações?",
            yesCallback: () {
              print("${Values.PRINT_DEVBOX_PREFIX} YES CLICKED ON DIALOG.");
              //Pops the dialog
              Navigator.of(context).pop();
            },
            noCallback: () {
              print("${Values.PRINT_DEVBOX_PREFIX} NO CLICKED ON DIALOG.");
            },
          );
        }
      }),
      expandedHeight: expandedHeight,

      flexibleSpace: FlexibleSpaceBar(
        collapseMode: CollapseMode.pin,
        centerTitle: true,
        background:

            ///LEADING - AVATAR
            StreamBuilder(
                stream: appBloc.outContractorLoggedIn,
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                    case ConnectionState.none:

                      ///LOADING INDICATOR
                      return Center(
                        child: Container(
                          width: 48,
                          height: 48,
                          child: CircularProgressIndicator(),
                        ),
                      );
                    default:
                      return Padding(
                        child: UserPictureAvatar(snapshot.data.pictureUrl),

                        /*Stack(
                          fit: StackFit.expand,
                          overflow: Overflow.visible,
                          children: <Widget>[
                            UserPictureAvatar(snapshot.data.pictureUrl),
                            Positioned(
                              top: 0,
                              right: 0,

                              child: Container(
                                child: SizedBox(
                                    child: Icon(Icons.edit,
                                        size: 24, color: Colors.white)),
                                width: 36,
                                height: 36,
                                //Gradient
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Theme.of(context).accentColor),
                              ),
                            )
                          ],
                        )*/
                        padding: EdgeInsets.symmetric(
                            vertical: verticalPadding,
                            horizontal: horizontalPadding),
                      );
                  }
                }),
      ),

      //title: Text(_title, style: TextStyle(color: Colors.black)),
      bottom: TabBar(
        //Color when the tab is selected/active
        labelColor: Theme.of(context).accentColor,
        indicatorColor: Theme.of(context).accentColor,
        //Colors when the tab is not selected/inactive
        unselectedLabelColor: Colors.grey,
        tabs: [
          Tab(
            text: "MEUS DADOS",
            /*icon: const Icon(FontAwesomeIcons.user), */
          ),
          Tab(text: "ENDEREÇOS"
              /*icon: const Icon(FontAwesomeIcons.addressCard), */)
        ],
      ),
    );
  }
}
