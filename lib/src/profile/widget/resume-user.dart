import 'package:docelar/src/shared/model/user.dart';
import 'package:flutter/material.dart';

class UserResume extends StatelessWidget {
  //Card height
  static const double HEIGHT = 132;

  //Amount of stars to be shown
  static const int STARS_MAX_COUNT = 5;

  //Stars size
  static const double STAR_SIZE = 20;

  //Text to be shown when there is no rating
  static const String NO_RATING_TEXT = "Sem avaliação";

  //Icon properties
  final User user;

  UserResume(this.user);

  @override
  Widget build(BuildContext context) {
    //user.phone = "81981186557";
    return ListView(
      children: <Widget>[
        const SizedBox(width: 16),

        ///USER NAME
        Text("${user.getFullName().toUpperCase()}",
            style: const TextStyle(
                fontWeight: FontWeight.bold, fontSize: 18),
            maxLines: 2,
            softWrap: true),

        //SizedBox(height: 16),

        Row(
          children: <Widget>[
            Text("CPF: ",
                style: const TextStyle(
                    fontSize: 18, fontWeight: FontWeight.bold)),
            Text("${user.getFormattedCpf()}",
                maxLines: 2,
                softWrap: true,
                style: const TextStyle(fontSize: 18)),
          ],
        ),

        Row(
          children: <Widget>[
            Text("E-mail: ",
                style: const TextStyle(
                    fontSize: 18, fontWeight: FontWeight.bold)),
            Text("${user.email}",
                maxLines: 2,
                softWrap: true,
                style: const TextStyle(fontSize: 18)),
          ],
        ),

        Row(
          children: <Widget>[
            Text("Telefone: ",
                style: const TextStyle(
                    fontSize: 18, fontWeight: FontWeight.bold)),
            Text("${user.getFormattedPhone()}",
                maxLines: 2,
                softWrap: true,
                style: const TextStyle(fontSize: 18)),
          ],
        ),


      ]
    );
  }
}
