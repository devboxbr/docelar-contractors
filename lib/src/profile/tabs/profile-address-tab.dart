import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/profile/profile-bloc.dart';
import 'package:docelar/src/shared/helpers/custom-navigation.dart';
import 'package:docelar/src/shared/model/user.dart';
import 'package:docelar/src/shared/widget/card-address.dart';
import 'package:docelar/src/shared/widget/dialogs.dart';
import 'package:flutter/material.dart';

class ProfileAddressTab extends StatelessWidget {
  final String _tabName = "my-addresses";

  AppBloc appBloc = BlocProvider.getBloc<AppBloc>();
  ProfileBloc profileBloc = BlocProvider.getBloc<ProfileBloc>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: StreamBuilder(
        stream: appBloc.outContractorLoggedIn,
        builder: (context, snapshot) {
          return Scaffold(
            floatingActionButton: FloatingActionButton(
              child: Center(child: Icon(Icons.add, color: Colors.grey[800])),
              onPressed: (){
                CustomNavigation.loadEditAddressScreen(context, null, null);
              },
            ),
            body: Builder(
              builder: (BuildContext context) {
                User user = snapshot.data;

                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                  case ConnectionState.none:

                    ///LOADING INDICATOR
                    return Center(
                      child: Container(
                        width: 48,
                        height: 48,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  default:

                    ///LOADED PAGE
                    return CustomScrollView(
                      key: PageStorageKey<String>(_tabName),
                      slivers: <Widget>[
                        SliverOverlapInjector(
                          handle:
                              NestedScrollView.sliverOverlapAbsorberHandleFor(
                                  context),
                        ),
                        SliverToBoxAdapter(
                          child: SizedBox(height: 16),
                        ),
                        SliverList(
                          delegate: SliverChildBuilderDelegate(
                              (context, index) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.waiting:
                              case ConnectionState.none:

                                ///LOADING INDICATOR
                                return Center(
                                  child: Container(
                                    width: 48,
                                    height: 48,
                                    child: CircularProgressIndicator(),
                                  ),
                                );
                              default:
                                if (user.allAddresses != null) {
                                  if (user.allAddresses.length > 0) {
                                    //print("${Values.PRINT_DEVBOX_PREFIX} LISTA DE ENDEREÇOS OK!");
                                    return InkWell(
                                      onLongPress: () {
                                        Dialogs.showDialogWithItemsList(
                                            context: context,
                                            title: Text(
                                              "OPÇÕES",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 22,
                                                  color: Theme.of(context)
                                                      .primaryColor),
                                            ),
                                            contentWidgets: [

                                              ///DIVIDER
                                              Container(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 8),
                                                height: 1,
                                                color: Theme.of(context)
                                                    .accentColor,
                                              ),

                                              ///EDIT BUTTON
                                              InkWell(
                                                child: ListTile(
                                                  title: Text(
                                                    "Tornar principal",
                                                    textAlign: TextAlign.center,
                                                    style:
                                                    TextStyle(fontSize: 18),
                                                  ),
                                                ),
                                                onTap: () {
                                                  //Popping the first dialog
                                                  Navigator.of(context).pop();
                                                  profileBloc.setMainAddress(user, user.allAddresses[index], appBloc);

                                                },
                                              ),

                                              ///DIVIDER
                                              Container(
                                                padding:
                                                const EdgeInsets.symmetric(
                                                    vertical: 8),
                                                height: 1,
                                                color: Theme.of(context)
                                                    .accentColor,
                                              ),

                                              ///EDIT BUTTON
                                              InkWell(
                                                child: ListTile(
                                                  title: Text(
                                                    "Editar",
                                                    textAlign: TextAlign.center,
                                                    style:
                                                    TextStyle(fontSize: 18),
                                                  ),
                                                ),
                                                onTap: () {
                                                  //Popping the first dialog
                                                  Navigator.of(context).pop();
                                                  profileBloc
                                                      .loadEditAddressView(
                                                      context, user.allAddresses[index], index);
                                                },
                                              ),

                                              ///DIVIDER
                                              Container(
                                                padding:
                                                const EdgeInsets.symmetric(
                                                    vertical: 8),
                                                height: 1,
                                                color: Theme.of(context)
                                                    .accentColor,
                                              ),

                                              ///DELETE BUTTON
                                              InkWell(
                                                child: ListTile(
                                                  title: Text("Apagar",
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                          fontSize: 18)),
                                                ),
                                                onTap: () {
                                                  //Popping the first dialog
                                                  Navigator.of(context).pop();
                                                  profileBloc.deleteUserAddress(
                                                      context,
                                                      user,
                                                      appBloc,
                                                      index);
                                                },
                                              )
                                            ]);
                                      },
                                      child: CardAddress(
                                        address: user.allAddresses[index],
                                        cardIndex: index,
                                        isMainAddress: user.allAddresses[index] == user.mainAddress ? true : false,
                                      ),
                                    );
                                  }
                                }
                                return Container(
                                  padding: const EdgeInsets.all(16),
                                  child: Text("Sem endereços cadastrados."),
                                );
                            }
                          },
                              //If the user hasn't a list of addresses
                              childCount: user == null
                                  ? 1
                                  : user.allAddresses == null
                                      //ChildCount  = 1
                                      ? 1
                                      //If the user has a list of address
                                      //And if the length of the list of address is smaller or equals zero
                                      : (user.allAddresses.length <= 0
                                          //ChildCount = 1
                                          ? 1
                                          //If if the length of the list of address is greater than zero
                                          //ChildCount = length of the list of addresses
                                          : user.allAddresses.length)),
                        ),
                      ],
                    );
                }
              },
            ),
          );
        },
      ),
    );
  }
}
