import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/profile/profile-bloc.dart';
import 'package:docelar/src/profile/widget/text-field-edit-profile.dart';
import 'package:docelar/src/shared/helpers/text-input-formatter.dart';
import 'package:docelar/src/shared/model/contractor.dart';
import 'package:docelar/src/shared/model/user.dart';
import 'package:docelar/src/shared/services/firebase-service.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/snackbars.dart';
import 'package:docelar/src/signup/signup-bloc.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ProfileAboutTab extends StatefulWidget {
  @override
  _ProfileAboutTabState createState() => _ProfileAboutTabState();
}

class _ProfileAboutTabState extends State<ProfileAboutTab> {
  final String _tabName = "my-data";
  bool _hasChangedData = false;
  static final _formEditProfileKey =
      GlobalKey<FormState>(debugLabel: "edit-profile-key");

  /*final TextEditingController _nameController = TextEditingController();
  final TextEditingController _cpfController = TextEditingController();*/
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();

  SignUpBloc signUpBloc = BlocProvider.getBloc<SignUpBloc>();
  ProfileBloc profileBloc = BlocProvider.getBloc<ProfileBloc>();
  AppBloc appBloc = BlocProvider.getBloc<AppBloc>();

  @override
  void initState() {
    super.initState();
    _phoneController.addListener(onTextFieldsChange);
    _emailController.addListener(onTextFieldsChange);
  }

  void onTextFieldsChange() {
    if (_phoneController.text != null || _emailController.text != null) {
      if (_phoneController.text.length > 0 ||
          _emailController.text.length > 0) {
        //print("${Values.PRINT_DEVBOX_PREFIX} text fields changed!");
        //print("${Values.PRINT_DEVBOX_PREFIX} phone typed: ${_phoneController.text ?? "null"}");
        //print("${Values.PRINT_DEVBOX_PREFIX} email typed: ${_emailController.text ?? "null"}");
        profileBloc.inHasMadeChanges.add(true);
      } else {
        profileBloc.inHasMadeChanges.add(false);
      }
    } else {
      profileBloc.inHasMadeChanges.add(false);
    }
  }

  @override
  void dispose() {
    super.dispose();
    _phoneController.dispose();
    _emailController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: false,
      child: Scaffold(
        floatingActionButton: StreamBuilder(
            stream: appBloc.outContractorLoggedIn,
            builder: (context, snapshot) {
              return FloatingActionButton(
                  child:
                      Center(child: Icon(Icons.save, color: Colors.grey[800])),
                  onPressed: () async {
                    String resultValidationEmail;
                    String resultValidationPhone;
                    //if there is a contractor saved in the appBloc
                    if (snapshot.hasData) {
                      if (_emailController.text.length > 0 ||
                          _phoneController.text.length > 0) {
                        //If the email field has some text
                        if (_emailController.text != null) {
                          if (_emailController.text.length > 0) {
                            //Validates the email typed
                            resultValidationEmail = signUpBloc
                                .validateEmailField(_emailController.text);
                          }
                        }
                        //print("${Values.PRINT_DEVBOX_PREFIX} 2 text phone: ${_phoneController.text}");
                        if (_phoneController.text != null) {
                          if (_phoneController.text.length > 0) {
                            //Validates the email typed
                            resultValidationPhone = signUpBloc
                                .validatePhoneField(_phoneController.text
                                    .replaceAll(new RegExp(r"[()-\s]"), ""));
                          }
                        }
                        /*//print("${Values.PRINT_DEVBOX_PREFIX} 3 text phone: ${_phoneController.text.replaceAll(new RegExp(r"[()-\s]"), "")}");
                      print(
                          "${Values.PRINT_DEVBOX_PREFIX} ProfileAboutTab(): resultValidationEmail: $resultValidationEmail");
                      print(
                          "${Values.PRINT_DEVBOX_PREFIX} ProfileAboutTab(): resultValidationPhone: $resultValidationPhone");*/
                        //Checks the validations
                        if (resultValidationEmail == null &&
                            resultValidationPhone == null) {
                          //If no result has been returned
                          await profileBloc.updateContractorPersonalData(
                              context, snapshot.data as Contractor, appBloc, _emailController.text, _phoneController.text
                              .replaceAll(new RegExp(r"[()-\s]"), ""));
                          _emailController.clear();
                          _phoneController.clear();
                        } else {
                          if (resultValidationEmail != null &&
                              resultValidationPhone == null) {
                            //Shows the result error to the user
                            SnackBars.showErrorSnackBar(
                                "Verifique seu e-mail!", context);
                          } else if (resultValidationEmail == null &&
                              resultValidationPhone != null) {
                            //Shows the result error to the user
                            SnackBars.showErrorSnackBar(
                                "Verifique seu telefone!", context);
                          } else {
                            //Shows the result error to the user
                            SnackBars.showErrorSnackBar(
                                "Verifique seus dados!", context);
                          }
                        }
                      } else {
                        SnackBars.showErrorSnackBar(
                            "Nenhuma alteração a ser salva!", context);
                      }
                    }
                  });
            }),
        body: Builder(
          builder: (BuildContext context) {
            return StreamBuilder(
              stream: appBloc.outContractorLoggedIn,
              builder: (context, snapshot) {
                //print("${Values.PRINT_DEVBOX_PREFIX} snapshot.data: ${snapshot.data}");
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                  case ConnectionState.none:

                    ///LOADING INDICATOR
                    return Center(
                      child: Container(
                        width: 48,
                        height: 48,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  default:

                    ///LOADED PAGE
                    return CustomScrollView(
                      key: PageStorageKey<String>(_tabName),
                      slivers: <Widget>[
                        SliverOverlapInjector(
                          handle:
                              NestedScrollView.sliverOverlapAbsorberHandleFor(
                                  context),
                        ),
                        SliverToBoxAdapter(
                          child: Padding(
                            padding: const EdgeInsets.all(16),
                            child: Column(
                              children: <Widget>[
                                ///USER NAME
                                TextFieldEditProfile(
                                  hintText:
                                      snapshot.data.getFullName().toUpperCase(),
                                  icon: Icon(
                                    FontAwesomeIcons.userAlt,
                                    color: Colors.grey[400],
                                  ),
                                  enabled: false,
                                  hideTrailIcon: true,
                                  textInputType: TextInputType.text,
                                  textCapitalization: TextCapitalization.words,
                                ),

                                const SizedBox(height: 8),

                                ///CPF
                                TextFieldEditProfile(
                                  hintText: snapshot.data.getFormattedCpf(),
                                  icon: Icon(
                                    FontAwesomeIcons.solidIdCard,
                                    color: Colors.grey[400],
                                  ),
                                  enabled: false,
                                  hideTrailIcon: true,
                                  textInputType: TextInputType.number,
                                  textCapitalization: TextCapitalization.words,
                                ),

                                const SizedBox(height: 8),

                                ///EMAIL
                                TextFieldEditProfile(
                                  focusNode: FocusNode(),
                                  labelText: snapshot.data.email,
                                  fieldController: _emailController,
                                  hintText: "Novo e-mail",
                                  icon: Icon(
                                    FontAwesomeIcons.solidEnvelope,
                                    color: Colors.grey[400],
                                  ),
                                  enabled: true,
                                  hideTrailIcon: false,
                                  textInputType: TextInputType.emailAddress,
                                  textCapitalization: TextCapitalization.none,
                                  onSubmitted: (String text) {
                                    _emailController.clear();
                                  },
                                ),

                                const SizedBox(height: 8),

                                ///PHONE
                                TextFieldEditProfile(
                                  focusNode: FocusNode(),
                                  labelText: snapshot.data.getFormattedPhone(),
                                  fieldController: _phoneController,
                                  hintText: "Novo telefone",
                                  icon: Icon(
                                    FontAwesomeIcons.phone,
                                    color: Colors.grey[400],
                                  ),
                                  enabled: true,
                                  hideTrailIcon: false,
                                  textCapitalization: TextCapitalization.none,
                                  textInputType: TextInputType.number,
                                  onSubmitted: (String text) {
                                    //print("${Values.PRINT_DEVBOX_PREFIX} 1 text phone: $text");
                                    _phoneController.clear();
                                  },
                                  inputFormatters: [
                                    CustomTextInputFormatters.phoneFormatter
                                  ],
                                ),
                              ],
                              //shrinkWrap: true,
                            ),
                          ),
                        ),
                      ],
                    );
                }
              },
            );
          },
        ),
      ),
    );
  }
}
