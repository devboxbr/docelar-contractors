import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/shared/helpers/text-input-formatter.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/signup/widget/text-field-signup.dart';
import 'package:docelar/src/signup/signup-bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignUpAddressView extends StatefulWidget {
  @override
  _SignUpAddressViewState createState() => _SignUpAddressViewState();
}

class _SignUpAddressViewState extends State<SignUpAddressView> {
  static final _formAddressKey =
      GlobalKey<FormState>(debugLabel: "signup-address-key");

  final SignUpBloc signUpBloc = BlocProvider.getBloc<SignUpBloc>();
  final AppBloc appBloc = BlocProvider.getBloc<AppBloc>();

  static final TextEditingController _labelController = TextEditingController();
  static final TextEditingController _postalCodeController =
      TextEditingController();
  static final TextEditingController _streetController =
      TextEditingController();
  static final TextEditingController _numberController =
      TextEditingController();
  static final TextEditingController _neighborhoodController =
      TextEditingController();
  static final TextEditingController _complementController =
      TextEditingController();
  static final TextEditingController _referencePointController =
      TextEditingController();
  static final TextEditingController _cityController = TextEditingController();
  static final TextEditingController _stateController = TextEditingController();

  final FocusNode focus1 = FocusNode();
  final FocusNode focus2 = FocusNode();
  final FocusNode focus3 = FocusNode();
  final FocusNode focus4 = FocusNode();
  final FocusNode focus5 = FocusNode();
  final FocusNode focus6 = FocusNode();
  final FocusNode focus7 = FocusNode();
  final FocusNode focus8 = FocusNode();
  final FocusNode focus9 = FocusNode();

  void focusNextField(
      BuildContext context, FocusNode currentNode, FocusNode nextNode) {
    currentNode.unfocus();
    FocusScope.of(context).requestFocus(nextNode);
  }

  void submitForm(BuildContext context) {
    // Validate returns true if the form is valid, otherwise false.
    if (_formAddressKey.currentState.validate()) {
      signUpBloc.saveUserAddress(
          context,
          appBloc,
          _labelController.text,
          _postalCodeController.text.replaceAll(new RegExp("[-,.\s]"), ""),
          _streetController.text,
          _numberController.text,
          _neighborhoodController.text,
          _complementController.text,
          _referencePointController.text,
          _cityController.text,
          _stateController.text);
    }
  }


  @override
  void dispose() {
    super.dispose();
    focus1.dispose();
    focus2.dispose();
    focus3.dispose();
    focus4.dispose();
    focus5.dispose();
    focus6.dispose();
    focus7.dispose();
    focus8.dispose();
    focus9.dispose();
    _labelController.dispose();
    _postalCodeController.dispose();
    _streetController.dispose();
    _numberController.dispose();
    _neighborhoodController.dispose();
    _complementController.dispose();
    _referencePointController.dispose();
    _cityController.dispose();
    _stateController.dispose();
  }

  @override
  Widget build(BuildContext context) {
//    SystemChrome.setSystemUIOverlayStyle(
//        SystemUiOverlayStyle(statusBarColor: Colors.transparent));

    return Scaffold(
        resizeToAvoidBottomPadding: true,
        body: Builder(builder: (BuildContext scaffoldContext) {
          return Stack(children: <Widget>[
            //BACKGROUND
            Image.asset(
              "resources/images/bg_recife_antigo.png",
              fit: BoxFit.cover,
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.center,
            ),

            //FORM
            Form(
              key: _formAddressKey,
              child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  shrinkWrap: true,
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(height: 48),

                    //1st ROW
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        //PAGE TITLE
                        Text("CRIAR CONTA",
                            style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontSize: 30,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center),
                        //PAGE DETAILS
                        Text("Endereço",
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Colors.white, fontSize: 18)),

                        SizedBox(height: 16),

                        GestureDetector(
                          onTap: () {
                            signUpBloc.loadHomeScreen(context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text("Concluir depois",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.grey[400]),
                                  textAlign: TextAlign.right),
                              SizedBox(width: 8),
                              Icon(FontAwesomeIcons.arrowRight,
                                  color: Colors.grey[400])
                            ],
                          ),
                        )
                      ],
                    ),

                    SizedBox(height: 32),
                    //TextFieldSignUp(this.hintText, this.validator, this.fieldController, this.obscureText, this.textCapitalization, this.textInputType, this.signUpBloc);

                    //LABEL
                    TextFieldSignUp(
                      hintText: "Casa, trabalho, academia...",
                      validator: (value) {
                        return signUpBloc.validateLabelField(value);
                      },
                      fieldController: _labelController,
                      obscureText: false,
                      textCapitalization: TextCapitalization.words,
                      textInputType: TextInputType.text,
                      signUpBloc: signUpBloc,
                      focusNode: focus1,
                      textInputAction: TextInputAction.next,
                      onSumitted: (value) {
                        focusNextField(scaffoldContext, focus1, focus2);
                      },
                    ),

                    SizedBox(height: 16),

                    //POSTAL CODE
                    TextFieldSignUp(
                      hintText: "CEP",
                      validator: (value) {
                        return signUpBloc.validatePostalCodeField(value.replaceAll(new RegExp("[-,.\s]"), ""));
                      },
                      fieldController: _postalCodeController,
                      obscureText: false,
                      textCapitalization: TextCapitalization.none,
                      textInputType: TextInputType.number,
                      signUpBloc: signUpBloc,
                      focusNode: focus2,
                      textInputAction: TextInputAction.next,
                      onSumitted: (value) {
                        focusNextField(scaffoldContext, focus2, focus3);
                      },
                      inputFormatters: [CustomTextInputFormatters.cepFormatter],
                    ),

                    SizedBox(height: 16),

                    //STREET
                    TextFieldSignUp(
                        hintText: "Rua",
                        validator: (value) {
                          return signUpBloc.validateStreetField(value);
                        },
                        fieldController: _streetController,
                        obscureText: false,
                        textCapitalization: TextCapitalization.sentences,
                        textInputType: TextInputType.text,
                        signUpBloc: signUpBloc,
                        focusNode: focus3,
                        textInputAction: TextInputAction.next,
                        onSumitted: (value) {
                          focusNextField(scaffoldContext, focus3, focus4);
                        }),

                    SizedBox(height: 16),

                    //NUMBER
                    TextFieldSignUp(
                        hintText: "Número",
                        validator: (value) {
                          return signUpBloc.validateAddressNumberField(value);
                        },
                        fieldController: _numberController,
                        obscureText: false,
                        textCapitalization: TextCapitalization.none,
                        textInputType: TextInputType.text,
                        signUpBloc: signUpBloc,
                        focusNode: focus4,
                        textInputAction: TextInputAction.next,
                        onSumitted: (value) {
                          focusNextField(scaffoldContext, focus4, focus5);
                        }),

                    SizedBox(height: 16),

                    //NEIGHBORHOOD
                    TextFieldSignUp(
                        hintText: "Bairro",
                        validator: (value) {
                          return signUpBloc.validateNeighborhoodField(value);
                        },
                        fieldController: _neighborhoodController,
                        obscureText: false,
                        textCapitalization: TextCapitalization.words,
                        textInputType: TextInputType.text,
                        signUpBloc: signUpBloc,
                        focusNode: focus5,
                        textInputAction: TextInputAction.next,
                        onSumitted: (value) {
                          focusNextField(scaffoldContext, focus5, focus6);
                        }),

                    SizedBox(height: 16),

                    //COMPLEMENT
                    TextFieldSignUp(
                        hintText: "Complemento (Edf., apto etc.)",
                        validator: (value) {
                          return signUpBloc.validateComplementField(value);
                        },
                        fieldController: _complementController,
                        obscureText: false,
                        textCapitalization: TextCapitalization.sentences,
                        textInputType: TextInputType.text,
                        signUpBloc: signUpBloc,
                        focusNode: focus6,
                        textInputAction: TextInputAction.next,
                        onSumitted: (value) {
                          focusNextField(scaffoldContext, focus6, focus7);
                        }),

                    SizedBox(height: 16),

                    //REFERENCE POINT
                    TextFieldSignUp(
                        hintText: "Ponto de referência",
                        validator: (value) {
                          return signUpBloc.validateReferencePointField(value);
                        },
                        fieldController: _referencePointController,
                        obscureText: false,
                        textCapitalization: TextCapitalization.sentences,
                        textInputType: TextInputType.text,
                        signUpBloc: signUpBloc,
                        focusNode: focus7,
                        textInputAction: TextInputAction.next,
                        onSumitted: (value) {
                          focusNextField(scaffoldContext, focus7, focus8);
                        }),

                    SizedBox(height: 16),

                    //STATE

                    TextFieldSignUp(
                      hintText: "Estado",
                      validator:     (value) {
                        return signUpBloc.validateStateField(value);
                      },
                      fieldController: _stateController,
                      obscureText: false,
                      textCapitalization: TextCapitalization.words,
                      textInputType: TextInputType.text,
                      signUpBloc: signUpBloc,
                      focusNode: focus8,
                      textInputAction: TextInputAction.next,
                      onSumitted: (value) {
                        focusNextField(scaffoldContext, focus8, focus9);
                      },
                    ),

                    SizedBox(height: 16),

                    //CITY
                    TextFieldSignUp(
                      hintText: "Cidade",
                      validator: (value) {
                        return signUpBloc.validateCityField(value);
                      },
                      fieldController: _cityController,
                      obscureText: false,
                      textCapitalization: TextCapitalization.words,
                      textInputType: TextInputType.text,
                      signUpBloc: signUpBloc,
                      focusNode: focus9,
                      textInputAction: TextInputAction.done,
                      onSumitted: (value) {
                        submitForm(scaffoldContext);
                      },
                    ),

                    SizedBox(height: 48),

                    //BUTTON SIGN-UP
                    Container(
                      height: Values.BUTTON_HEIGHT,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(35)),
                        onPressed: () {
                          submitForm(scaffoldContext);
                        },
                        color: Theme.of(context).accentColor,
                        child: Text(
                          "FINALIZAR",
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),

                    SizedBox(height: 48),
                  ]),
            )
          ]);
        }));
  }
}
