import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/signup/signup-bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TextFieldSignUp extends StatelessWidget {
  final String hintText;
  final String labelText;
  final Function(String) validator;
  final TextEditingController fieldController;
  final TextCapitalization textCapitalization;
  final bool obscureText;
  final TextInputType textInputType;
  final SignUpBloc signUpBloc;
  final FocusNode focusNode;
  final TextInputAction textInputAction;
  final Function onSumitted;
  final List<TextInputFormatter> inputFormatters;
  final Color textDefaultColor;
  final Color fillColor;


  TextFieldSignUp(
      {@required this.hintText,
      @required this.validator,
      @required this.fieldController,
      @required this.obscureText,
      @required this.textCapitalization,
      @required this.textInputType,
      @required this.signUpBloc,
      @required this.focusNode,
      @required this.textInputAction,
      @required this.onSumitted,
      this.inputFormatters,
      this.textDefaultColor,
      this.fillColor,
      this.labelText});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: signUpBloc.outObscurePasswordText,
        builder: (context, snapshot) {
          return TextFormField(
            inputFormatters: inputFormatters,
            onFieldSubmitted: onSumitted,
            textInputAction: textInputAction,
            obscureText: this.obscureText ? (snapshot.data ?? false) : false,
            style: TextStyle(color: textDefaultColor ?? Colors.white),
            keyboardType: textInputType,
            focusNode: focusNode,
            decoration: InputDecoration(
                labelText: labelText,
                labelStyle: TextStyle(color: textDefaultColor ?? Colors.white),
                suffixIcon: this.obscureText
                    ? IconButton(
                        icon: Icon(
                            snapshot.data != null
                                ? (snapshot.data
                                    ? FontAwesomeIcons.eyeSlash
                                    : FontAwesomeIcons.eye)
                                : null,
                            color: textDefaultColor ?? Colors.white),
                        onPressed: () {
                          signUpBloc.inObscurePasswordText.add(!snapshot.data);
                        })
                    : null,
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                filled: true,
                fillColor: fillColor ?? Color.fromARGB(80, 255, 255, 255),
                border: const OutlineInputBorder(
                  //TODO: Transformar isso num método
                  borderSide:
                      const BorderSide(width: 1, color: Colors.transparent),
                  borderRadius: const BorderRadius.all(const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide:
                      const BorderSide(width: 1, color: Colors.transparent),
                  borderRadius: const BorderRadius.all(const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context).accentColor, width: 1),
                  borderRadius: const BorderRadius.all(const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS)),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.red, width: 1),
                  borderRadius: const BorderRadius.all(const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS)),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.red, width: 1),
                  borderRadius: const BorderRadius.all(const Radius.circular(Values.TEXT_FIELD_BORDER_RADIUS)),
                ),
                errorStyle: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: Colors.red[200]),
                hintText: hintText,
                hintStyle: TextStyle(color: textDefaultColor ?? Colors.white)),
            textCapitalization: textCapitalization,
            validator: validator,
            controller: fieldController,
          );
        });
  }
}
