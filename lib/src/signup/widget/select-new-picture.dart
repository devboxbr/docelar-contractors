import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SelectNewPicture extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: SizedBox(
              child: Icon(FontAwesomeIcons.camera,
                  size: 50, color: Colors.grey[300])),
          width: 100,
          height: 100,
          //Gradient
          decoration: BoxDecoration(
              border: Border.all(
                  width: 3, color: Colors.grey[300]),
              shape: BoxShape.circle,
              color: Colors.white),
        ),
        Positioned(
          bottom: 4,
          right: 4,
          child: Container(
            child: SizedBox(
                child: Icon(Icons.add,
                    size: 24, color: Colors.white)),
            width: 36,
            height: 36,
            //Gradient
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Theme.of(context).accentColor),
          ),
        )
      ],
    );
  }
}
