import 'dart:io';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/home/home-view.dart';
import 'package:docelar/src/shared/helpers/custom-navigation.dart';
import 'package:docelar/src/shared/model/address.dart';
import 'package:docelar/src/shared/model/contractor.dart';
import 'package:docelar/src/shared/model/user.dart';
import 'package:docelar/src/shared/services/firebase-service.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/dialogs.dart';
import 'package:docelar/src/shared/widget/snackbars.dart';
import 'package:docelar/src/signin/signin-view.dart';
import 'package:docelar/src/signup/sign-up-address-view.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:cpf_cnpj_validator/cpf_validator.dart';

class SignUpBloc extends BlocBase {
  var _name = BehaviorSubject<String>.seeded("");

  Stream<String> get outName => _name.stream;

  Sink<String> get inName => _name.sink;

  var _lastName = BehaviorSubject<String>.seeded("");

  Stream<String> get outLastName => _lastName.stream;

  Sink<String> get inLastName => _lastName.sink;

  var _cpf = BehaviorSubject<String>.seeded("");

  Stream<String> get outCpf => _cpf.stream;

  Sink<String> get inCpf => _cpf.sink;

  var _phoneNumber = BehaviorSubject<String>.seeded("");

  Stream<String> get outPhoneNumber => _phoneNumber.stream;

  Sink<String> get inPhoneNumber => _phoneNumber.sink;

  var _email = BehaviorSubject<String>.seeded("");

  Stream<String> get outEmail => _email.stream;

  Sink<String> get inEmail => _email.sink;

  var _password = BehaviorSubject<String>.seeded("");

  Stream<String> get outPassword => _password.stream;

  Sink<String> get inPassword => _password.sink;

  var _passwordConfirmation = BehaviorSubject<String>.seeded("");

  Stream<String> get outPasswordConfirmation => _passwordConfirmation.stream;

  Sink<String> get inPasswordConfirmation => _passwordConfirmation.sink;

  var _obscurePasswordText = BehaviorSubject<bool>.seeded(true);

  Stream<bool> get outObscurePasswordText => _obscurePasswordText.stream;

  Sink<bool> get inObscurePasswordText => _obscurePasswordText.sink;

  var _loading = BehaviorSubject<bool>.seeded(false);

  Stream<bool> get outLoading => _loading.stream;

  Sink<bool> get inLoading => _loading.sink;

  var _pictureUrl = BehaviorSubject<String>.seeded(null);

  Stream<String> get outPictureUrl => _pictureUrl.stream;

  Sink<String> get inPictureUrl => _pictureUrl.sink;

  var _userPicture = BehaviorSubject<File>.seeded(null);

  Stream<File> get outUserPicture => _userPicture.stream;

  Sink<File> get inUserPicture => _userPicture.sink;

  Contractor newContractor;

  String validateNameField(String value) {
    if (value.isEmpty) {
      //Validates empty strings
      return "Informe seu nome";
    } else if (!value.contains(new RegExp("[^a-záàãâéèẽêíóòõôúùũ ]"))) {
      //Validates all the acceptable characters
      return "Digite apenas letras";
    } else {
      return null;
    }
  }

  String validateLastNameField(String value) {
    if (value.isEmpty) {
      //Validates empty strings
      return "Informe seu sobrenome";
    } else if (!value.contains(new RegExp("[^a-záàãâéèẽêíóòõôúùũ ]"))) {
      //Validates all the acceptable characters
      return "Digite apenas letras";
    } else {
      return null;
    }
  }

  ///Validates a CPF number passed as the value
  String validateCpfField(String value) {
    if (!CPFValidator.isValid(value)) {
      return "CPF inválido";
    } else {
      return null;
    }
  }

  ///Validates the phone informed
  String validatePhoneField(String value) {
    if (value.isEmpty) {
      //If the password text is empty
      return "Informe sua senha";
    } else if (value.contains(new RegExp("[^0-9]"))) {
      return "Digite apenas números";
    } else if (value.length < 11) {
      return "Informe seu número no formato (99)99999-9999";
    } else {
      return null;
    }
  }

  ///Method to validate the email's text field of the login form
  String validateEmailField(String value) {
    if (value.isEmpty) {
      //If the field is empty
      return "Informe seu e-mail";
    } else if (!value.contains("@")) {
      //If the text hasn't an @ symbol, consider as an invalid email
      return "Informe um e-mail";
    } else if (value.contains(" ")) {
      //If the text has som blank space
      return "E-mails não contêm espaços em branco";
    } else if (!value.contains(new RegExp(".+@.+\..+"))) {
      //Checks if the email has an email formatting
      return "Informe um e-mail válido";
    } else {
      return null;
    }
  }

  ///Validates the password's text field of the login form
  String validatePasswordField(String value) {
    if (value.isEmpty) {
      //If the password text is empty
      return "Informe sua senha";
    } else if (value.length < 6) {
      //If the password has less than 6 characters
      return "Obrigatório ter mais de 6 caracteres";
    } else {
      return null;
    }
  }

  ///Validates if the passwords match
  String validatePasswordConfirmationField(
      String password, String passwordConfirmation) {
    if (password.isEmpty) {
      //If the password text is empty
      return "Digite sua senha novamente para confirmar";
    } else if (password != passwordConfirmation) {
      return "Senhas não coincidem";
    } else {
      return null;
    }
  }

  String validateLabelField(String value) {
    if (value.isEmpty) {
      //If the password text is empty
      return "Diga o que é esse lugar. Pode ser sua casa, casa da sua mãe, seu trabalho etc.";
    } else {
      return null;
    }
  }

  String validatePostalCodeField(String value) {
    if (value.isEmpty) {
      //If the password text is empty
      return "Informe seu CEP";
    } else if (value.length != 8) {
      return "Informe os 8 dígitos do seu CEP (99999-999)";
    } else if (value.contains(new RegExp("[^0-9]"))) {
      return "Digite apenas números";
    } else {
      return null;
    }
  }

  String validateStreetField(String value) {
    if (value.isEmpty) {
      //Validates empty strings
      return "Informe o nome da sua rua";
    } else {
      return null;
    }
  }

  String validateAddressNumberField(String value) {
    if (value.isEmpty) {
      //Validates empty strings
      return "Informe o número da sua residência na rua";
    } else {
      return null;
    }
  }

  String validateNeighborhoodField(String value) {
    if (value.isEmpty) {
      //Validates empty strings
      return "Informe o nome do seu bairro";
    } else {
      return null;
    }
  }

  String validateComplementField(String value) {
    if (value.isEmpty) {
      //Validates empty strings
      return "Informe o complemento. Ex: casa, apt. 101, bloco C etc.";
    } else {
      return null;
    }
  }

  String validateReferencePointField(String value) {
    if (value.isEmpty) {
      //Validates empty strings
      return "Informe um ponto de referência. Ex: próximo à padaria do João";
    } else {
      return null;
    }
  }

  String validateCityField(String value) {
    if (value.isEmpty) {
      //Validates empty strings
      return "Informe o nome da sua cidade";
    } else if (!value.contains(new RegExp("[^a-záàãâéèẽêíóòõôúùũ, ]"))) {
      //Validates all the acceptable characters
      return "Digite apenas letras";
    } else {
      return null;
    }
  }

  String validateStateField(String value) {
    if (value.isEmpty) {
      //Validates empty strings
      return "Informe o nome da sua cidade";
    } else if (!value.contains(new RegExp("[^a-záàãâéèẽêíóòõôúùũ, ]"))) {
      //Validates all the acceptable characters
      return "Digite apenas letras";
    } else {
      return null;
    }
  }

  //FIREBASE
  /// Errors:
  ///   ERROR_WEAK_PASSWORD - If the password is not strong enough.
  ///   ERROR_INVALID_EMAIL - If the email address is malformed.
  ///   ERROR_EMAIL_ALREADY_IN_USE - If the email is already in use by a different account.
  ///Sign up a new user
  Future<bool> signUserUp(BuildContext context, AppBloc appBloc) async {
    //Shows the loading dialog with the circular progress indicator
    Dialogs.showLoadingDialog(context);
    //TODO: check if the login was successful at the end of the method. If it wasn't, delete the user created at start.
    FirebaseUser firebaseUser =
        await FirebaseService.signUserUp(_email.value, _password.value)
            .catchError((onError) {
      //Popping the dialog called
      Navigator.of(context).pop();

      //Handling the possible errors
      if (onError.toString().contains("ERROR_WEAK_PASSWORD")) {
        SnackBars.showErrorSnackBar("Senha fraca demais.", context);
      } else if (onError.toString().contains("ERROR_INVALID_EMAIL")) {
        SnackBars.showErrorSnackBar("E-mail inválido!", context);
      } else if (onError.toString().contains("ERROR_EMAIL_ALREADY_IN_USE")) {
        SnackBars.showErrorSnackBar("E-mail já cadastrado, tente outro.", context);
      }
    })/*.then((user) async {

      //Updating user's name and picture at Firebase Authentication
      String result = await FirebaseService.updateAuthUser(_name.value, _pictureUrl.value);

      //If there was an error
      if (result != null) {
        //Popping the dialog called
        Navigator.of(context).pop();
        showErrorSnackBar(result, context);
        //Deletes the user created at the start of the method
        await user.delete();
      }
      return;
    })*/;

    if (firebaseUser != null) {
      //Saves the user's picture to firebase
      await savePictureToFirebaseStorage(_userPicture.value).then((String url) {
        //Saves the picture's url to the bloc
        inPictureUrl.add(url);
      });

      //User's model creation + Firestore
      newContractor = Contractor(_name.value, _lastName.value, _cpf.value,
          _phoneNumber.value, _email.value);

      //Saving in the user's object his picture url, if we have a valid link
      if (_pictureUrl.value.isNotEmpty || _pictureUrl.value != null)
        newContractor.pictureUrl = _pictureUrl.value;
      //Saving the user's id to his object
      newContractor.id = firebaseUser.uid;
      newContractor.registerDateTime = DateTime.now();

      //Saving the user to Firestore
      await FirebaseService.saveUserToFirestore(newContractor).then((success) {
        //Popping the dialog called
        //Navigator.of(context).pop();
        print("${Values.PRINT_DEVBOX_PREFIX} SignUpBloc.signUserUp() - Receiving success = $success");

        //If the firestore process was successful
        if (success) {
          //appBloc.inContractorIn.add(newContractor);
          //Loads the address sign-up screen
          loadAddressScreen(context);
          //Deleting the picture here so when the user finishes a signup,
          // logs out and tries to signup again, he won't see his last taken
          // picture anymore
          _userPicture.value.delete();
          _pictureUrl.value = null;
          //TODO: Save this user to shared preferences aiming an automatic sign in next time
          return true;
        } else {
          //Deletes the user created at the start of the method
          firebaseUser.delete();

          print("${Values.PRINT_DEVBOX_PREFIX}Error loggin in with firebase!");
          SnackBars.showErrorSnackBar(
              "Não foi possível fazer o cadastro. Já possui conta?", context);
          return false;
        }
      });
    } else {
      print("${Values.PRINT_DEVBOX_PREFIX} Error to sign in with firebase!");
      //Popping the CircularProgressIndicator() dialog
      //Navigator.of(context).pop();
      Scaffold.of(context).showSnackBar(
          SnackBar(content: Text("Não foi possível entrar. Verifique sua conexão com a internet!")));
    }
    return false;
  }

  ///Saver the address to the user's data in firestore
  void saveUserAddress(
      BuildContext context,
      AppBloc appBloc,
      String label,
      String postalCode,
      String street,
      String number,
      String neighborhood,
      String complementInfo,
      String referencePoint,
      String city,
      String state) async {
    Address address = Address(label, street, number, neighborhood, postalCode,
        city, state, complementInfo, referencePoint);

    //Show the loading-dialog
    Dialogs.showLoadingDialog(context);

    //TODO: localizar location point
    address.locationCoordinates = null;

    //Calling Firebase service to save the address
    await FirebaseService.saveContractorAddressToFirestore(newContractor.id, address).then((User user) async {
      print("${Values.PRINT_DEVBOX_PREFIX}saved address? $user");
      if (user != null) {
        appBloc.inContractorIn.add(user);
        loadHomeScreen(context);
      } else {
        //Closing the loading-dialog
        Navigator.of(context).pop();
        //Feedback to the user
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text("Ops! Algo deu errado!")));
        /*
        //Waits 3 seconds - So the user can read the snackbar
        await Future.delayed(Duration(seconds: 3));
        //And loads the home screen
        loadHomeScreen(context);
        */
      }
    });
  }

  ///Whether the user has taken a picture
  bool hasUserTakenPicture() {
    return _userPicture.value != null;
  }

  ///Saves the passed file to Firestore
  Future<String> savePictureToFirebaseStorage(File pictureFile) async {
    return await FirebaseService.savePictureToFirebase(pictureFile);
  }

  ///Load Home screen
  void loadAddressScreen(BuildContext context) {
    CustomNavigation.loadAddressSignUpScreen(context);
  }

  ///Load Home screen
  void loadHomeScreen(BuildContext context) {
    CustomNavigation.loadHomeScreen(context);
  }

  void loadLoginScreen(BuildContext context) {
    CustomNavigation.loadSignInScreen(context);
    inUserPicture.add(null);
  }

  @override
  void dispose() {
    super.dispose();
    _name.close();
    _lastName.close();
    _cpf.close();
    _phoneNumber.close();
    _email.close();
    _password.close();
    _passwordConfirmation.close();
    _obscurePasswordText.close();
    _loading.close();
    _pictureUrl.close();
    _userPicture.close();
  }
}
