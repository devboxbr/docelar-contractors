import 'dart:io';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:brasil_fields/formatter/cpf_input_formatter.dart';
import 'package:brasil_fields/formatter/telefone_input_formatter.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/shared/helpers/text-input-formatter.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/dialogs.dart';
import 'package:docelar/src/shared/widget/snackbars.dart';
import 'package:docelar/src/signup/widget/text-field-signup.dart';
import 'package:docelar/src/signup/signup-bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';

class SignUpView extends StatefulWidget {
  @override
  _SignUpViewState createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  static final _formPersonalKey =
      GlobalKey<FormState>(debugLabel: "signup-personal-key");

  final SignUpBloc signUpBloc = BlocProvider.getBloc<SignUpBloc>();
  final AppBloc appBloc = BlocProvider.getBloc<AppBloc>();

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _cpfController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordConfirmationController =
      TextEditingController();

  final FocusNode focus1 = FocusNode();
  final FocusNode focus2 = FocusNode();
  final FocusNode focus3 = FocusNode();
  final FocusNode focus4 = FocusNode();
  final FocusNode focus5 = FocusNode();
  final FocusNode focus6 = FocusNode();
  final FocusNode focus7 = FocusNode();

  File _pictureTaken;

  Future<void> takePicture(BuildContext context) async {
    _pictureTaken = await ImagePicker.pickImage(
            source: ImageSource.camera, imageQuality: 50)
        .then((File pictureFile) {
      signUpBloc.inUserPicture.add(pictureFile);
      return pictureFile;
    });
  }

  ///Android system -- although very rarely -- sometimes kills the MainActivity after
  ///the image_picker finishes. When this happens, we lost the data selected from the
  ///image_picker. You can use retrieveLostData to retrieve the lost data in this situation.
  Future<void> retrieveLostData(BuildContext context) async {
    final LostDataResponse response = await ImagePicker.retrieveLostData();
    if (response == null) {
      return;
    }
    if (response.file != null) {
      setState(() {
        if (response.type == RetrieveType.image) {
          //_userPicture = response.file;
        }
      });
    } else {
      SnackBars.showErrorSnackBar("Ops! Algo deu errado!", context);
    }
  }

  void focusNextField(
      BuildContext context, FocusNode currentNode, FocusNode nextNode) {
    currentNode.unfocus();
    FocusScope.of(context).requestFocus(nextNode);
  }

  Future<void> submitForm(BuildContext context) async {
    if (!signUpBloc.hasUserTakenPicture()) {
      print("${Values.PRINT_DEVBOX_PREFIX} Faltou a foto");
      //Show the user a dialog informing the picture is missing
      Dialogs.showDialogWithOkButton(context, "Ops!",
          "Você precisa tirar uma foto para finalizar o cadastro.");
    } else {
      // Validate returns true if the form is valid, otherwise false.
      if (_formPersonalKey.currentState.validate()) {
        //Saves all his/her data
        signUpBloc.inName.add(_nameController.text);
        signUpBloc.inLastName.add(_lastNameController.text);
        signUpBloc.inCpf.add(_cpfController.text.replaceAll(new RegExp("[-,.\s]"), ""));
        signUpBloc.inPhoneNumber.add(_phoneController.text.replaceAll(new RegExp("[()-,.\s]"), ""));
        signUpBloc.inEmail.add(_emailController.text);
        signUpBloc.inPassword.add(_passwordController.text);

        await signUpBloc.signUserUp(context, appBloc);
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
    focus1.dispose();
    focus2.dispose();
    focus3.dispose();
    focus4.dispose();
    focus5.dispose();
    focus6.dispose();
    focus7.dispose();
    _nameController.dispose();
    _lastNameController.dispose();
    _cpfController.dispose();
    _phoneController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _passwordConfirmationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
//    SystemChrome.setSystemUIOverlayStyle(
//        SystemUiOverlayStyle(statusBarColor: Colors.transparent));

    return Scaffold(
        resizeToAvoidBottomPadding: true,
        body: Builder(builder: (BuildContext scaffoldContext) {
          return Stack(children: <Widget>[
            //BACKGROUND
            Image.asset(
              "resources/images/bg_recife_antigo.png",
              fit: BoxFit.cover,
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.center,
            ),

            //FORM
            Form(
              key: _formPersonalKey,
              child: ListView(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  shrinkWrap: true,
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    const SizedBox(height: 48),

                    //1st ROW
                    Row(
                      children: <Widget>[
                        //1st COLUMN
                        //CIRCLE SELECT NEW PICTURE
                        GestureDetector(
                            child: Stack(
                              children: <Widget>[
                                StreamBuilder<File>(
                                  stream: signUpBloc.outUserPicture,
                                  builder: (context, snapshot) {
                                    //NO PICTURE BEING USED
                                    if (snapshot.data == null) {
                                      return Container(
                                        child: SizedBox(
                                            child: Icon(FontAwesomeIcons.camera,
                                                size: 50,
                                                color: Colors.grey[300])),
                                        width: 100,
                                        height: 100,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                width: 3,
                                                color: Colors.grey[300]),
                                            shape: BoxShape.circle,
                                            color: Colors.white),
                                      );
                                    } else {
                                      //AFTER THE USER HAS TAKEN A PICTURE
                                      return Container(
                                        height: 100,
                                        width: 100,
                                        child: ClipOval(
                                          child: Image.file(
                                            snapshot.data,
                                            fit: BoxFit.cover,
                                            width: double.infinity,
                                            height: double.infinity,
                                            alignment: Alignment.center,
                                          ),
                                        ),
                                      );
                                    }
                                  },
                                ),
                                StreamBuilder<File>(
                                    stream: signUpBloc.outUserPicture,
                                    builder: (context, snapshot) {
                                      //TAKE PICTURE ICON BUTTON
                                      return Positioned(
                                        bottom: 4,
                                        right: 4,
                                        child: Container(
                                          child: SizedBox(
                                              child: Icon(
                                                  snapshot.data == null
                                                      ? Icons.add
                                                      : Icons.edit,
                                                  size: 24,
                                                  color: Colors.white)),
                                          width: 36,
                                          height: 36,
                                          //Gradient
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Theme.of(context)
                                                  .accentColor),
                                        ),
                                      );
                                    })
                              ],
                            ),
                            onTap: () async {
                              setState(() {
                                //Takes the user's picture using his camera
                                takePicture(scaffoldContext);
                              });
                            }),

                        const SizedBox(width: 32),

                        //2nd COLUMN
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            //PAGE TITLE
                            Text("CRIAR CONTA",
                                style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold)),
                            //PAGE DETAILS
                            const Text("Dados Pessoais",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18)),
                          ],
                        ),
                      ],
                    ),

                    const SizedBox(height: 32),
                    //TextFieldSignUp(this.hintText, this.validator, this.fieldController, this.obscureText, this.textCapitalization, this.textInputType, this.signUpBloc);
                    //FIRST NAME
                    TextFieldSignUp(
                      hintText: "Nome",
                      validator: (value) {
                        return signUpBloc.validateNameField(value);
                      },
                      fieldController: _nameController,
                      obscureText: false,
                      textCapitalization: TextCapitalization.words,
                      textInputType: TextInputType.text,
                      signUpBloc: signUpBloc,
                      focusNode: focus1,
                      textInputAction: TextInputAction.next,
                      onSumitted: (value) {
                        focusNextField(scaffoldContext, focus1, focus2);
                      },
                    ),

                    const SizedBox(height: 16),

                    //LAST NAME
                    TextFieldSignUp(
                      hintText: "Sobrenome",
                      validator: (value) {
                        return signUpBloc.validateLastNameField(value);
                      },
                      fieldController: _lastNameController,
                      obscureText: false,
                      textCapitalization: TextCapitalization.words,
                      textInputType: TextInputType.text,
                      signUpBloc: signUpBloc,
                      focusNode: focus2,
                      textInputAction: TextInputAction.next,
                      onSumitted: (value) {
                        focusNextField(scaffoldContext, focus2, focus3);
                      },
                    ),

                    const SizedBox(height: 16),

                    //CPF
                    TextFieldSignUp(
                      hintText: "CPF",
                      validator: (value) {
                        return signUpBloc.validateCpfField(value.replaceAll(new RegExp("[-,.\s]"), ""));
                      },
                      fieldController: _cpfController,
                      obscureText: false,
                      textCapitalization: TextCapitalization.none,
                      textInputType: TextInputType.number,
                      signUpBloc: signUpBloc,
                      focusNode: focus3,
                      textInputAction: TextInputAction.next,
                      onSumitted: (value) {
                        focusNextField(scaffoldContext, focus3, focus4);
                      },
                      inputFormatters: [CustomTextInputFormatters.cpfFormatter],
                    ),

                    const SizedBox(height: 16),

                    //PHONE NUMBER
                    TextFieldSignUp(
                      hintText: "Número de celular",
                      validator: (value) {
                        return signUpBloc.validatePhoneField(value.replaceAll(new RegExp("[()-,.\s]"), ""));
                      },
                      fieldController: _phoneController,
                      obscureText: false,
                      textCapitalization: TextCapitalization.none,
                      textInputType: TextInputType.number,
                      signUpBloc: signUpBloc,
                      focusNode: focus4,
                      textInputAction: TextInputAction.next,
                      onSumitted: (value) {
                        focusNextField(scaffoldContext, focus4, focus5);
                      },
                      inputFormatters: [
                        CustomTextInputFormatters.phoneFormatter
                      ],
                    ),

                    const SizedBox(height: 16),

                    //EMAIL
                    TextFieldSignUp(
                      hintText: "E-mail",
                      validator: (value) {
                        return signUpBloc.validateEmailField(value);
                      },
                      fieldController: _emailController,
                      obscureText: false,
                      textCapitalization: TextCapitalization.none,
                      textInputType: TextInputType.emailAddress,
                      signUpBloc: signUpBloc,
                      focusNode: focus5,
                      textInputAction: TextInputAction.next,
                      onSumitted: (value) {
                        focusNextField(scaffoldContext, focus5, focus6);
                      },
                    ),

                    const SizedBox(height: 16),

                    //PASSWORD
                    TextFieldSignUp(
                      hintText: "Senha",
                      validator: (value) {
                        return signUpBloc.validatePasswordField(value);
                      },
                      fieldController: _passwordController,
                      obscureText: true,
                      textCapitalization: TextCapitalization.none,
                      textInputType: TextInputType.text,
                      signUpBloc: signUpBloc,
                      focusNode: focus6,
                      textInputAction: TextInputAction.next,
                      onSumitted: (value) {
                        focusNextField(scaffoldContext, focus6, focus7);
                      },
                    ),

                    const SizedBox(height: 16),

                    //PASSWORD CONFIRMATION
                    TextFieldSignUp(
                      hintText: "Confirma sua senha",
                      validator: (value) {
                        return signUpBloc.validatePasswordConfirmationField(
                            value, _passwordController.text);
                      },
                      fieldController: _passwordConfirmationController,
                      obscureText: true,
                      textCapitalization: TextCapitalization.none,
                      textInputType: TextInputType.text,
                      signUpBloc: signUpBloc,
                      focusNode: focus7,
                      textInputAction: TextInputAction.done,
                      onSumitted: (value) {
                        submitForm(scaffoldContext);
                      },
                    ),

                    const SizedBox(height: 48),

                    //BUTTON SIGN-UP
                    Container(
                      height: Values.BUTTON_HEIGHT,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(35)),
                        onPressed: () async {
                          submitForm(scaffoldContext);
                        },
                        color: Theme.of(context).accentColor,
                        child: const Text(
                          "AVANÇAR",
                          style: const TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),

                    const SizedBox(height: 16),

                    SizedBox(
                      height: 32,
                      child: GestureDetector(
                        onTap: () {
                          signUpBloc.loadLoginScreen(context);
                        },
                        child: const Text(
                          "Cancelar",
                          textAlign: TextAlign.center,
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                    ),

                    const SizedBox(height: 48),
                  ]),
            )
          ]);
        }));
  }
}
