import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/shared/services/firebase-service.dart';
import 'package:rxdart/rxdart.dart';

class WalletBloc extends BlocBase {
  var _wallet = BehaviorSubject<double>.seeded(null);

  Stream<double> get outWallet => _wallet.stream;

  Sink<double> get inWallet => _wallet.sink;

  Future<String> getWalletInfo() async {
    double value = await FirebaseService.getWalletInfo();
    return value == null
        ? "R\$0,00"
        : "R\$${value.toStringAsFixed(2).replaceAll('.', ',')}";
  }

  @override
  void dispose() {
    super.dispose();
    _wallet.close();
  }
}
