import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/shared/helpers/custom-icons.dart';
import 'package:docelar/src/shared/values.dart';
import 'package:docelar/src/shared/widget/button-back-app-bar.dart';
import 'package:docelar/src/shared/widget/card-small-square.dart';
import 'package:docelar/src/wallet/wallet-bloc.dart';
import 'package:flutter/material.dart';

class WalletView extends StatelessWidget {
  final AppBloc appBloc = BlocProvider.getBloc<AppBloc>();
  final WalletBloc walletBloc = BlocProvider.getBloc<WalletBloc>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.grey[50],
          leading: BackButtonAppBar(callback: (){
            Navigator.of(context).pop();
          }),
        ),
        //drawer: MainDrawer(),
        body: ListView(
          children: <Widget>[
            ///Spacer
            //SizedBox(height: 16),

            ///WALLET YELLOW CARD
            _buildWalletCard(context),

            ///SPACER
            SizedBox(height: 16),

            GridView.count(
              padding: EdgeInsets.symmetric(horizontal: 16),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              crossAxisCount: 3,
              childAspectRatio: 1.05,
              mainAxisSpacing: 16,
              crossAxisSpacing: 8,
              children: <Widget>[
                CardSmallSquare(
                  height: CardSmallSquare.defaultSize,
                  customIcon: CustomIcon(
                      iconCode: CustomIcon.transfer_money,
                      size: 40,
                      color: Theme.of(context).primaryColor),
                  title: "Transferir saldo",
                ),
                CardSmallSquare(
                  height: CardSmallSquare.defaultSize,
                  customIcon: CustomIcon(
                      iconCode: CustomIcon.charge_to_friend,
                      size: 40,
                      color: Theme.of(context).primaryColor),
                  size: null,
                  title: "Carga para amigo",
                ),
                CardSmallSquare(
                  height: CustomIcon.defaultSize,
                  customIcon: CustomIcon(
                      iconCode: CustomIcon.money_back,
                      size: 40,
                      color: Theme.of(context).primaryColor),
                  title: "Reembolso",
                ),
                CardSmallSquare(
                  height: CustomIcon.defaultSize,
                  customIcon: CustomIcon(
                      iconCode: CustomIcon.finance_history,
                      size: 40,
                      color: Theme.of(context).primaryColor),
                  title: "Histórico financeiro",
                ),
                CardSmallSquare(
                  height: CustomIcon.defaultSize,
                  customIcon: CustomIcon(
                      iconCode: CustomIcon.help,
                      size: 40,
                      color: Theme.of(context).primaryColor),
                  title: "Ajuda",
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _buildWalletCard(BuildContext context) {
    return

        ///YELLOW CARD
        Container(
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      width: MediaQuery.of(context).size.width,
      height: 200,
      //decoration with bg color and SHADOW
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        boxShadow: [
          BoxShadow(
              color: Colors.grey[400], blurRadius: 2, offset: Offset(0, 2))
        ],
        borderRadius:
            BorderRadius.all(Radius.circular(Values.CARD_BORDER_RADIUS)),
        //border: Border.all(color: Values.mediumGreyText, width: _borderSize)
      ),
      child:

          ///INNER YELLOW CARD container
          Stack(
        children: <Widget>[
          ///BG
          Container(
            padding: EdgeInsets.only(top: 16, left: 16, right: 16),
            child: Opacity(
              child: Image.asset(
                "resources/images/bg_wallet_container.png",
                fit: BoxFit.fitHeight,
                width: double.infinity,
                height: double.infinity,
                alignment: Alignment.centerLeft,
              ),
              opacity: 0.3,
            ),
          ),

          ///CONTENT
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(right: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text("Minha carteira", style: TextStyle(fontSize: 16)),
                FutureBuilder(
                  future: walletBloc.getWalletInfo(),
                  builder: (context, snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                      case ConnectionState.waiting:
                        //Shows a circular progress indicator when there is no data loaded
                        return Container(
                            alignment: Alignment.centerRight,
                            height: 30,
                            width: 30,
                            child: CircularProgressIndicator());
                      default:
                        return Text(snapshot.data,
                            style: TextStyle(fontSize: 54));
                    }
                  },
                ),
                FlatButton(
                  padding: EdgeInsets.all(0),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.all(
                            Radius.circular(Values.CARD_BORDER_RADIUS + 10))),
                    padding: EdgeInsets.all(8),
                    child: Text(
                      "Adicionar",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
