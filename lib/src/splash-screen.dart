import 'package:docelar/src/home/home-view.dart';
import 'package:docelar/src/shared/services/firebase-service.dart';
import 'package:docelar/src/signin/signin-view.dart';
import 'package:docelar/src/signup/sign-up-address-view.dart';
import 'package:docelar/src/signup/signup-personal-view.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    _checkUserAndChangePage(context);
  }

  //Loads the home screen
  Future _checkUserAndChangePage(BuildContext context) async {
    bool isLoogedIn = await FirebaseService.isUserLoggedIn();

    //DEBUG ONLY
    //_loadSignInView(context);

    //print("isUserLoggedIn? $isLoogedIn");

    if (isLoogedIn) {
      _loadHomeView(context);
    } else {
      _loadSignInView(context);
    }

  }

  void _loadHomeView(BuildContext context) async {
    await Future.delayed(Duration(seconds: 3)).then((v) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeView()));
    });
  }

  //Loads the home screen
  void _loadSignInView(BuildContext context) async {
    await Future.delayed(Duration(seconds: 3)).then((v) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => SignInView()));
    });
  }

  //Loads the home screen
  void _loadSignUpView(BuildContext context) async {
    await Future.delayed(Duration(seconds: 3)).then((v) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => SignUpView()));
    });
  }

  @override
  Widget build(BuildContext context) {
    //Making the app fullscreen
    //SystemChrome.setEnabledSystemUIOverlays([]);
    //SystemChrome.restoreSystemUIOverlays();

    return Stack(
      children: <Widget>[
        Image.asset(
          "resources/images/splash-screen.png",
          fit: BoxFit.cover,
          width: double.infinity,
          height: double.infinity,
          alignment: Alignment.center,
        )
      ],
    );
  }
}
