import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/app-bloc.dart';
import 'package:docelar/src/home/home-bloc.dart';
import 'package:docelar/src/profile/profile-bloc.dart';
import 'package:docelar/src/shared/widget/drawer/main-drawer-bloc.dart';
import 'package:docelar/src/signin/signin-bloc.dart';
import 'package:docelar/src/signup/signup-bloc.dart';
import 'package:docelar/src/splash-screen.dart';
import 'package:docelar/src/shared/theme/style.dart';
import 'package:docelar/src/wallet/wallet-bloc.dart';
import 'package:docelar/src/worker-details/worker-details-bloc.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocs: [
        Bloc((i) => AppBloc()),
        Bloc((i) => SignInBloc()),
        Bloc((i) => SignUpBloc()),
        Bloc((i) => MainDrawerBloc()),
        Bloc((i) => HomeBloc()),
        Bloc((i) => WorkerDetailsBloc()),
        Bloc((i) => WalletBloc()),
        Bloc((i) => ProfileBloc()),
      ],
      child: MaterialApp(
        title: 'Doce Lar',
        theme: appTheme(),
        home: SplashScreen(),
        debugShowCheckedModeBanner: false,
      )
    );
  }
}
