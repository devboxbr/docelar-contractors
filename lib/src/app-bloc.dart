import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:docelar/src/shared/model/contractor.dart';
import 'package:docelar/src/shared/model/user.dart';
import 'package:rxdart/rxdart.dart';

class AppBloc extends BlocBase {

  var _contractorIn = BehaviorSubject<Contractor>.seeded(null);

  Stream<Contractor> get outContractorLoggedIn => _contractorIn.stream;
  Sink<Contractor> get inContractorIn => _contractorIn.sink;

  User getUserLoggedIn() {
    return _contractorIn.value;
  }

  @override
  void dispose() {
    super.dispose();
    _contractorIn.close();
  }


}
